#!/bin/env python

# submission script for MonoJetAnalysis jobs
# Valerio Ippolito - INFN Sezione di Roma

import re

def configureSamples(sh):
  from ROOT import SH
  # configure SampleHandler metadata
  sh.setMetaString('nc_tree', 'CollectionTree')
  sh.setMetaString('isAFII', 'NO')
  sh.setMetaString('isData', 'NO')

  # now, make sensible exceptions (letting us know about whether
  # SampleHandler believes these are 8/13 TeV AFII/FULLSIM samples)
  regexps = { 'isAFII' : re.compile('a\d\d\d'),
              'isData': re.compile('data'),
  }

  for isample in xrange(sh.size()):
    sample = sh.at(isample)

    for opt,this_re in regexps.items():
      state = 'NO'
      if this_re.search(sample.name()):
        state = 'YES'

      sample.setMetaString(opt, state)
      print 'Sample {name} has {opt} set to {val}'.format(name=sample.name(), opt=opt, val=state)


if __name__ == '__main__':
  allowed_drivers = ['local', 'prun', 'proof', 'slurm', 'condor']
  allowed_algos = ['MonoJet', 'MonoJetTruth', 'MonoJetVjetsRW']

  from argparse import ArgumentParser
  import os
  parser = ArgumentParser(description='launch MonoJetAnalysis jobs', add_help=True)

  # submission
  parser.add_argument('-w', '--where', type=str, dest='where', choices=allowed_drivers, help='driver to be used (local, prun, proof, slurm)', metavar='where', default='local')
  parser.add_argument('-t', '--inputList', type=str, dest='txt', help='comma-separated list of txt files, one per sample, containing file paths', metavar='list', default='')
  parser.add_argument('-g', '--inputDatasets', type=str, dest='dq2', help='comma-separated list of input DQ2 datasets', metavar='list', default='')
  parser.add_argument('-l', '--inputDatasetLists', type=str, dest='dq2list', help='comma-separated list of txt files containing one DQ2 sample per line (empty lines or lines starting with # are ignored)', metavar='list', default='')
  parser.add_argument('-d', '--inputDirs', type=str, dest='dir', help='comma-separated list of input directories (they will be scanned for samples, one per subdirectory)', metavar='list', default='')
  parser.add_argument('-n', '--name', type=str, dest='submitDir', help='name of the local directory to use for output (and, if applicable, prefix of the output DQ2 datasets)', metavar='submitDir', default='submitDir')
  parser.add_argument('-a', '--algo', type=str, dest='algoName', choices=allowed_algos, help='algorithm name (e.g. MonoJet)', metavar='algoName', default='MonoJet')
  parser.add_argument('-m', '--nEvtsMax', type=int, dest='nEvtsMax', help='maximum number of events to process', metavar='nEvtsMax', default=-1)
  parser.add_argument('-s', '--nSkipEvts', type=int, dest='nSkipEvts', help='skip number of events', metavar='nSkipEvts', default=-1)
  parser.add_argument('-u', '--user', type=str, dest='userName', help='username for grid jobs', metavar='userName', default=os.environ['USER'])
  parser.add_argument('-rs', '--replicationSite', type=str, dest='replicationSite', help='name of disk where to replicate output of grid jobs', metavar='replicationSite', default=None)
  #parser.add_argument('-f', '--additional-flags', type=str, dest='additionalFlags', help='additional commands for prun / batch submission', metavar='cmd', default='--bexec="unset ROOT_TTREECACHE_SIZE"')

  # algo
  parser.add_argument('--doSyst', dest='doSystematics', help='also do systematic variations', action='store_true', default=False)
  parser.add_argument('--doTheoSyst', dest='doTheoSystematics', help='also do theoretical systematic variations', action='store_true', default=False) #theo syst
  parser.add_argument('--doIncludeTaus', dest='doIncludeTaus', help='ignore the photon definitions', action='store_false', default=True)
  #parser.add_argument('--doSystTrees', dest='doSystTrees', help='write the systematic TTrees', action='store_true', default=False)
  parser.add_argument('--dirtyJets', dest='dirtyJets', help='no Loose cleaning', action='store_true', default=False)
  parser.add_argument('--doPreOR', dest='doPreOR', help='save also objects before OR (big ntuples!)', action='store_true', default=False)
  parser.add_argument('--configFile', type=str, dest='configFile', help='name of the config file to use', metavar='configFile', default='MonoJetAnalysis/monophoton.conf')
  #parser.add_argument('--ptSkim', type=int, dest='ptSkim', help='leading jet pt skim (nominal tree), in MeV', metavar='cut', default=150000)
  #parser.add_argument('--ptSkimForSyst', type=int, dest='ptSkimForSyst', help='leading jet pt skim (systematics), in MeV', metavar='cut', default=150000)
  #parser.add_argument('--metSkim', type=int, dest='metSkim', help='MET skim (nominal tree), in MeV', metavar='cut', default=150000)
  #parser.add_argument('--metSkimForSyst', type=int, dest='metSkimForSyst', help='MET skim (systematics), in MeV', metavar='cut', default=150000)
  #parser.add_argument('--phptSkim', type=int, dest='phptSkim', help='leading photon pt skim (nominal tree), in MeV', metavar='cut', default=150000)
  #parser.add_argument('--phptSkimForSyst', type=int, dest='phptSkimForSyst', help='leading photon pt skim (systematics), in MeV', metavar='cut', default=150000)
  parser.add_argument('--doMonoph', dest='doMonophoton', help='do monophoton analysis', action='store_false', default=True) #FEDE
  parser.add_argument('--cutSyst', dest='cutSystematics', help='remove systematics trees when systematic affects only SFs', action='store_false', default=True)
  parser.add_argument('--doSkim', dest='doSkim', help='Apply analysis common selections', action='store_true', default=True)
  parser.add_argument('--doTrim', dest='doTrim', help='Trim branches', action='store_true', default=True)
  parser.add_argument('--MCsample', type=str, dest='MCsample', choices= ['nominal', 'data', 'Zgamma', 'Znunugamma', 'Wgamma', 'gammajets'] , help='name of the MC sample', metavar='MCsample', default='nominal')

  options = parser.parse_args()

  import ROOT
  ROOT.gROOT.SetBatch(True)
  ROOT.gROOT.ProcessLine(ROOT.gSystem.ExpandPathName('.x $ROOTCOREDIR/scripts/load_packages.C'))
  from ROOT import xAOD
  from ROOT import SH
  from ROOT import EL

  ### checks
  if (options.where == None):
    parser.error('Empty driver')
  if (options.userName == ''):
    parser.error('Empty user name')
  if (options.txt == '' and options.dq2 == '' and options.dq2list == '' and options.dir == ''): 
    parser.error('Empty input list')

  ### set up the job for xAOD access
  xAOD.Init().ignore()

  ### crash on unchecked status codes
  #ROOT.xAOD.TReturnCode.enableFailure()

  ###
  # determine input samples
  sh = SH.SampleHandler()

  getlist = lambda x: filter(lambda y: y != '', x.replace(' ', '').split(','))

  txts = getlist(options.txt)
  for txt in txts:
    sampleName = ''.join(txt.split('/')[-1].split('.')[:-1]) # /a/b/c/d.txt -> d
    print 'adding txt ',sampleName
    SH.readFileList(sh, sampleName, txt)

  dq2s = getlist(options.dq2)
  for dq2 in dq2s:
    print 'adding dq2 ',dq2
    SH.scanRucio(sh, dq2)
    #SIL: add  this to force the job to look for data on LOCALGROUPDISK
    #SH.makeGridDirect(sh, 'INFN-MILANO-ATLASC_LOCALGROUPDISK', 'srm://t2cmcondor.mi.infn.it/atlas/atlaslocalgroupdisk', '/atlaslocalgroupdisk', True)

  dq2lists = getlist(options.dq2list)
  for dq2list in dq2lists:
    print 'adding dq2list ',dq2list
    for dq2raw in open(dq2list).readlines():
      dq2 = dq2raw.rstrip('\n').replace(' ', '') # remove spaces from line
      if dq2.startswith('#') == False and dq2 != '': # ignore comments / separators
      	SH.scanRucio(sh, dq2)
    		#SIL: add  this to force the job to look for data on LOCALGROUPDISK
      	#SH.makeGridDirect(sh, 'INFN-MILANO-ATLASC_LOCALGROUPDISK', 'srm://t2cmcondor.mi.infn.it/atlas/atlaslocalgroupdisk', '/atlaslocalgroupdisk', True)

  dirs = getlist(options.dir)
  for dir in dirs:
    print 'adding dir ',dir
    SH.scanDir(sh, dir)

  configureSamples(sh)
  sh.setMetaString("nc_grid_filter", "*AOD*");
  ##SIL: workaround to solve rucio problem: data found on storage_3 instead of storage_1
  sh.updateLocation("root://gridftp-b1-1.mi.infn.it:1094//gpfs/storage_1/atlas/atlaslocalgroupdisk/rucio", "/gpfs/storage_1/atlas/atlaslocalgroupdisk/rucio")
  # print SampleHandler object (need the getattr trick to access the method 'print')
  getattr(sh, 'print')()


  ###
  # create an EventLoop job
  job = EL.Job()
  job.sampleHandler(sh)

  # add the algorithm we asked to the job
  # (we use getattr to make it possible to use any
  # algorithm, e.g. MonoJetTruth, MonoJetR20...)
  alg = getattr(ROOT, options.algoName)()
  alg.SetName(options.algoName)

  # set algorithm options
  alg.config_file = options.configFile
  alg.m_doSystematics = options.doSystematics
  alg.m_doTheoSystematics = options.doTheoSystematics #theo syst
  #alg.doSystTrees = options.doSystTrees
  alg.m_doIncludeTaus = options.doIncludeTaus
  alg.dirtyJets = options.dirtyJets
  alg.doPreOR = options.doPreOR
  #alg.ptSkim = options.ptSkim
  #alg.ptSkimForSyst = options.ptSkimForSyst
  #alg.metSkim = options.metSkim
  #alg.metSkimForSyst = options.metSkimForSyst
  #alg.phptSkim = options.phptSkim
  #alg.phptSkimForSyst = options.phptSkimForSyst
  alg.doMonophoton = options.doMonophoton
  alg.cutSystematics = options.cutSystematics  #Systematics reorganization
  alg.m_doSkim = options.doSkim
  alg.m_doTrim = options.doTrim
  alg.MC_sample = options.MCsample 
  job.algsAdd(alg)

  # make sure we can read trigger decision
  job.options().setString(EL.Job.optXaodAccessMode, EL.Job.optXaodAccessMode_class)

  # limit number of events
  if options.nEvtsMax > 0:
    job.options().setDouble(EL.Job.optMaxEvents, options.nEvtsMax);
    print 'Jobs will process maximum {n} events'.format(n=options.nEvtsMax)

  # limit number of events
  if options.nSkipEvts > 0:
    job.options().setDouble(EL.Job.optSkipEvents, options.nSkipEvts);
    print 'Jobs will skip {n} events'.format(n=options.nSkipEvts)

  ###
  # run
  driver = None
  submitDir = options.submitDir
  if (options.where == 'local'):
#   if options.additionalFlags != None:
#     raise RuntimeError('Additional flags not supported for DirectDriver')
    driver = EL.DirectDriver()
    driver.submit(job, submitDir)
  elif (options.where == 'prun'):
    out = EL.OutputStream("outputLabel", "xAOD")
    out.options().setString(EL.OutputStream.optMergeCmd, "XAOD_ACCESSTRACER_FRACTION=0 xAODMerge -b") 
    dset_name_mask = 'user.{user}.{tag}.%in:name[1]%.%in:name[2]%.%in:name[3]%'.format(user=options.userName, tag=submitDir)
    print dset_name_mask, len(dset_name_mask)
    #if len(dset_name_mask > )
    driver = EL.PrunDriver()
#   driver.options().setDouble('nc_skipScout', 1)  # not a prun option anymore :(
    if options.doSystematics: # or options.doSystTrees: 
      driver.options().setString(EL.Job.optGridNGBPerJob, '1')
      driver.options().setString('nc_optGridNfilesPerJob', '1')    
    else:
      driver.options().setString(EL.Job.optGridNGBPerJob, '5')
      driver.options().setString('nc_optGridNfilesPerJob', '5')
    driver.options().setString('nc_outputSampleName', dset_name_mask)
    if options.replicationSite !=None: 
      driver.options().setString('nc_destSE', options.replicationSite)
    if (options.userName == 'areiss' and options.replicationSite ==None):
      driver.options().setString('nc_destSE', "MAINZGRID_LOCALGROUPDISK")
#   if options.additionalFlags != None:
#     driver.options().setString(EL.Job.optSubmitFlags, options.additionalFlags)
    driver.submitOnly(job, submitDir)
  elif (options.where == 'proof'):
    driver = EL.ProofDriver()
    driver.numWorkers = 8
#   if options.additionalFlags != None:
#     driver.options().setString(EL.Job.optSubmitFlags, options.additionalFlags)
    driver.submit(job, submitDir)
  elif (options.where == 'slurm'):
    driver = EL.SlurmDriver()
    driver.SetJobName('mj')
    driver.SetAccount('hepl')
    driver.SetPartition('pleiades')
    driver.SetRunTime('0-10:00') # 10h
    driver.SetMemory('2000')
    driver.SetConstrain('amd|intel')
    driver.submit(job, submitDir)
  elif (options.where == 'condor'):
    #EL.StatusCode.FAILURE
    driver = ROOT.EL.CondorDriver()
    driver.shellInit =  "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase; source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh || exit $?"
    job.options().setString(ROOT.EL.Job.optCondorConf,'Requirements = ((TARGET.ClusterName == "proof-pool") &&((TARGET.Machine == "proof-04.mi.infn.it") ||(TARGET.Machine == "proof-06.mi.infn.it")  ||(TARGET.Machine == "proof-07.mi.infn.it") ||(TARGET.Machine == "proof-08.mi.infn.it")||(TARGET.Machine == "proof-09.mi.infn.it") ) )')
    driver.submitOnly(job, options.submitDir)
    job.options().setString(ROOT.EL.Job.optCondorConf,'Requirements = HasCVMFS')
    job.options().setDouble (ROOT.EL.Job.optFilesPerWorker, 5);
  else:
    raise RuntimeError('Unrecognized driver option {opt}'.format(opt=options.where))

