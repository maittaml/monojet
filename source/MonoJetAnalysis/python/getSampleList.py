import sys, os, argparse, subprocess, re, pickle
from argparse import ArgumentParser
from collections import OrderedDict

def get_args():
  '''
  This function gets the command line options.
  '''

  parser = argparse.ArgumentParser(prog='getSampleList.py')


  parser.add_argument('-p', '--projectTag',type=str, nargs='?', help='Project tag, defaults to "mc15_13TeV"', default='mc16_13TeV')
  parser.add_argument('-d', '--derivation',type=str, nargs='?', help='Derivation, defaults to "EXOT5"', default='DAOD_EXOT5', required = True)
  parser.add_argument('-t', '--tag',type=str, nargs='?', help='tag name for output file',required = True)
  
  return parser.parse_args()

def runCommand(cmd, verbose = False):
    if verbose:
        print "  Will run the following command: %s" % cmd
    cmdResult = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return cmdResult.stdout


def searchDS(queryPattern):
    cmd = "rucio ls --short --filter type=CONTAINER %s | sort -r " % (queryPattern)
    queryResult = runCommand(cmd, False)
    search = [line.rstrip().replace(projectTag+":", "") for line in queryResult.readlines()]
    if not search: search.append("# %s not found" % (queryPattern))
    #print search
    return search

if __name__ == '__main__':
    
    
    options = get_args()

    
    MC_samples = OrderedDict([
#        ('top' , [410470,410471,410472,410501, 410503,410011,410012,410013,410014,410025,410026]),
#        ('Zee' , range(364114, 364127 + 1)),
#        ('Zmumu' , range(364100, 364113 +1)),
#        ('Ztautau' , range(364128,364141 + 1)),
#        ('Znunu' , range(364142,364155 + 1)),
#        ('Wenu' , range(364170,364183 + 1)),
#        ('Wmunu' , range(364156,364169 + 1)),
#        ('Wtaunu' , range(364184,364197 + 1)),
        ('Zmumu_PTV' , [364216, 364217]),
        ('Zee_PTV' , [364218, 364219]),
        ('Ztautau_PTV' , [364220, 364221]),
        ('Znunu_PTV' , [364222, 364223]),
        ('Wmunu_PTV' , [364224, 364225]),
        ('Wenu_PTV' , [364226, 364227]),
        ('Wtaunu_PTV' , [364228, 364229]),
        ##new drell yan
#        ('Zmumu_DY', range(364198, 364203+1)),
#        ('Zee_DY', range(364204, 364209+1)),
#        ('Ztautau_DY', range(364210, 364215+1)),
#        ('multijet' , range(361020, 361032 + 1) + range(426001, 426009+1)),
#        ('VV' , range(363355, 363360+1) + range(364242, 364255+1) + [363489,363494] + range(308092, 308098+1)),
#        ('photon', range(361039, 361062 + 1)),
#        ('photon_NLO' , range(364543, 364547 +1))
        ])

    #    print MC_samples


    daod = options.derivation
    projectTag = options.projectTag
    nameTag = options.tag

    print daod
    if not 'data' in projectTag: isData = False
    else: isData = True

    tags = ['r10201*p3480','r10201*p3371']#,'r10201*p3495']

    if not isData: outName = '{tag}_bkg_{daod}.txt'.format(tag = nameTag, daod = daod);
    else: outName = '{tag}_{daod}.txt'.format(tag = nameTag, daod = daod);

    print '---------------------------------------'
    print '-- Your script confugration --'
    print '---------------------------------------'
    print ''
    print '\t Project tag: ', projectTag
    for tag in tags: print '\t Tags: ', tag 
    print '\t Output file ', outName
    print '---------------------------------------'


    outFile = open(outName, "w")

    for groupname, runs in MC_samples.items():
        print 'searching ',groupname, ' datasets'
        outFile.write("### %s ######\n" % groupname)
        for mc_channel_number in runs:
          #print mc_channel_number
          for tag in tags:
            dataset = '{projectTag}.{dsid}.*{daod}*{tag}/'.format(projectTag = projectTag, dsid = mc_channel_number, daod = daod, tag = tag)
            #print dataset
            result =  searchDS(dataset)
            for i in result: outFile.write("%s\n" %i)

    outFile.close()

