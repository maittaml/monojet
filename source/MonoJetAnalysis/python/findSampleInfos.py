import re
import subprocess

def getSampleInfo(sample):
    #    print 'ami show dataset info ' + sample
    try:
      info = subprocess.check_output('ami show dataset info ' + sample, shell=True).split('\n')
    except subprocess.CalledProcessError:
      print 'unable to find dataset', sample.split('.')[1], sample.split('.')[2]
      return

    mcid = 0
    name = ''
    xsec = 0
    eff = 1
    total_events = 0

    for lineraw in info:
        line = lineraw.rstrip('\n')
        if 'totalEvents' in line:
            x = re.compile('totalEvents\s+:\s+([.\-0-9e]+)\s*')
            m = x.match(line)
            if m:
                total_events = int(m.group(1))
        if 'physicsShort' in line:
            x = re.compile('physicsShort\s+:\s+([a-zA-Z\_0-9]+)\s*')
            m = x.match(line)
            if m:
                name = m.group(1)
        if 'crossSection' in line:
            x = re.compile('crossSection\s+:\s+([.\-0-9e]+)\s*')
            m = x.match(line)
            if m:
                xsec = float(m.group(1))*1e3
        if 'genFiltEff' in line:
            x = re.compile('genFiltEff\s+:\s+([.\-0-9e]+)\s*')
            m = x.match(line)
            if m:
                eff = float(m.group(1))
        if 'datasetNumber' in line:
            mcid = line
            x = re.compile('datasetNumber\s+:\s+([0-9]+)\s*')
            m = x.match(line)
            if m:
                mcid = int(m.group(1))

    print 'mcid:',mcid,'name:',sample,'xsec:',xsec,'eff:',eff,'Nevents:',total_events

def eventize(sample):
  # split with .
  tokens = sample.split('.')
  new_tokens = []
  for i, token in enumerate(tokens):
    if 'mc16_13TeV' in token:
      use = 'mc15_13TeV'
    elif 'deriv' in token:
      use = 'evgen'
    elif 'AOD' in token:
      use = 'EVNT'
    elif i == len(tokens) - 1 or (i == len(tokens) - 2 and tokens[-1] == '/'):
      # last term is to be split again by _, to isolate the 'e' tag
      tagtokens = token.split('_')
      new_tagtokens = []
      for tagtoken in tagtokens:
        if 'e' in tagtoken:
          new_tagtokens.append(tagtoken)
      if len(new_tagtokens) == 0:
        raise RuntimeError('Unable to guess e-tag')
      else:
        use = '_'.join(new_tagtokens)
    else:
      use = token
    new_tokens.append(use)

  return '.'.join(new_tokens)


if __name__ == '__main__':
  from argparse import ArgumentParser
  parser = ArgumentParser(description='launch MonoJetAnalysis jobs', add_help=True)
  parser.add_argument('-l', '--lists', type=str, dest='lists', help='comma-separated list of text files, containing one EVNT dataset per line', metavar='lists', required=True)
  parser.add_argument('-e', '--eventize', default=False, dest='eventize', help='try to guess event dataset from (D)AOD dataset', action='store_true')

  options = parser.parse_args()

  files = options.lists.split(',')

  for filename in files:
    with open(filename) as f:
      samples = map(lambda x: x.rstrip('\n'), f.readlines())
      if options.eventize:
        samples = map(eventize, samples)

      non_evnt = filter(lambda x: 'EVNT' not in x, samples)
      if len(non_evnt):
        print non_evnt
        raise RuntimeError('EVNT datasets must be provided')
      else:
        for sample in samples:
          getSampleInfo(sample) 
