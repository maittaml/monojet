#!/bin/bash

curl http://adc-mon.cern.ch/DAODProduction//DAILYCHECK_MC/derivs_obsolete_DAILYCHECK_MC.txt > MCDeletions.txt
curl http://adc-mon.cern.ch/DAODProduction//DAILYCHECK_DATA/derivs_obsolete_DAILYCHECK_DATA.txt > DataDeletions.txt
g++ -std=c++11 outputDaysToDeletion.cpp -O3 -o outputDaysToDeletionexe.out
./outputDaysToDeletionexe.out
rm outputDaysToDeletionexe.out
rm MCDeletions.txt
rm DataDeletions.txt
