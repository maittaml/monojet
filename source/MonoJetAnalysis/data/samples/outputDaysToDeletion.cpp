#include <vector>
#include <tuple>
#include <fstream>
#include <sstream>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <iomanip>
#include <regex>

std::vector<std::pair<std::string,int>> readListOfDeletionsIntoVectorWithDatasetNameAndDeletionTime(std::string deletionFileAddress);
std::vector<std::string> readListOfDatasets(std::string datasetAddress);

std::vector<std::pair<std::string,int>> removeUnusedDatasets(std::vector<std::pair<std::string,int>> deletionList,std::vector<std::string> usedDatasets);

bool sortbysecondElement(const std::pair<std::string,int> &a,
			 const std::pair<std::string,int> &b)
{
  return (a.second > b.second);
}


int main(){


  std::ofstream outputFileForDatasetsRequiringExtension;
  outputFileForDatasetsRequiringExtension.open("monojetExtensionRequest.txt", std::ios::trunc);
  
  std::vector<std::pair<std::string,int>> MCDeletionList = readListOfDeletionsIntoVectorWithDatasetNameAndDeletionTime("MCDeletions.txt");
  std::vector<std::pair<std::string,int>> dataDeletionList = readListOfDeletionsIntoVectorWithDatasetNameAndDeletionTime("DataDeletions.txt");


  std::vector<std::string> datasetList_MC16a_Bkg = readListOfDatasets("EXOT5/bkg_mc16a_EXOT5.txt");
  std::vector<std::string> datasetList_MC16c_Bkg = readListOfDatasets("EXOT5/bkg_mc16c_EXOT5.txt");
  std::vector<std::string> datasetList_MC16d_Bkg = readListOfDatasets("EXOT5/bkg_mc16d_EXOT5.txt");
  std::vector<std::string> datasetList_MC16e_Bkg = readListOfDatasets("EXOT5/bkg_mc16e_EXOT5.txt");
  
  std::vector<std::string> datasetList_MC16a_Sig = readListOfDatasets("EXOT5/sig_mc16a_EXOT5.txt");
  std::vector<std::string> datasetList_MC16c_Sig = readListOfDatasets("EXOT5/sig_mc16c_EXOT5.txt");
  std::vector<std::string> datasetList_MC16d_Sig = readListOfDatasets("EXOT5/sig_mc16d_EXOT5.txt");
  std::vector<std::string> datasetList_MC16e_Sig = readListOfDatasets("EXOT5/sig_mc16e_EXOT5.txt");


  std::vector<std::string> datasetList_special_samples = readListOfDatasets("EXOT5/special_samples_EXOT5.txt");

  std::vector<std::string> datasetList_data15 = readListOfDatasets("EXOT5/data15_EXOT5.txt");
  std::vector<std::string> datasetList_data16 = readListOfDatasets("EXOT5/data16_EXOT5.txt");
  std::vector<std::string> datasetList_data17 = readListOfDatasets("EXOT5/data17_EXOT5.txt");
  std::vector<std::string> datasetList_data18 = readListOfDatasets("EXOT5/data18_EXOT5.txt");

  
  std::vector<std::string> datasetList;
  datasetList.insert( datasetList.end(), datasetList_MC16a_Bkg.begin(), datasetList_MC16a_Bkg.end() );
  datasetList.insert( datasetList.end(), datasetList_MC16c_Bkg.begin(), datasetList_MC16c_Bkg.end() );
  datasetList.insert( datasetList.end(), datasetList_MC16d_Bkg.begin(), datasetList_MC16d_Bkg.end() );
  datasetList.insert( datasetList.end(), datasetList_MC16e_Bkg.begin(), datasetList_MC16e_Bkg.end() );

  datasetList.insert( datasetList.end(), datasetList_MC16a_Sig.begin(), datasetList_MC16a_Sig.end() );
  datasetList.insert( datasetList.end(), datasetList_MC16c_Sig.begin(), datasetList_MC16c_Sig.end() );
  datasetList.insert( datasetList.end(), datasetList_MC16d_Sig.begin(), datasetList_MC16d_Sig.end() );
  datasetList.insert( datasetList.end(), datasetList_MC16e_Sig.begin(), datasetList_MC16e_Sig.end() );


  datasetList.insert( datasetList.end(), datasetList_special_samples.begin(), datasetList_special_samples.end() );

  datasetList.insert( datasetList.end(), datasetList_data15.begin(), datasetList_data15.end() );
  datasetList.insert( datasetList.end(), datasetList_data16.begin(), datasetList_data16.end() );
  datasetList.insert( datasetList.end(), datasetList_data17.begin(), datasetList_data17.end() );
  datasetList.insert( datasetList.end(), datasetList_data18.begin(), datasetList_data18.end() );


  std::vector<std::pair<std::string,int>> allMCDatasetsWithDeletionTimes = removeUnusedDatasets(MCDeletionList,datasetList);
  std::vector<std::pair<std::string,int>> allDataDatasetsWithDeletionTimes = removeUnusedDatasets(dataDeletionList,datasetList);
  
  
  std::vector<std::pair<std::string,int>> allRelevantDatasetsWithDeletionTimes;
  allRelevantDatasetsWithDeletionTimes.insert( allRelevantDatasetsWithDeletionTimes.end(), allMCDatasetsWithDeletionTimes.begin(), allMCDatasetsWithDeletionTimes.end() );
  allRelevantDatasetsWithDeletionTimes.insert( allRelevantDatasetsWithDeletionTimes.end(), allDataDatasetsWithDeletionTimes.begin(), allDataDatasetsWithDeletionTimes.end() );
  
  
  sort(allRelevantDatasetsWithDeletionTimes.begin(), allRelevantDatasetsWithDeletionTimes.end(), sortbysecondElement);

  std::cout << std::setw(200) << std::left;
  std::cout << "Dataset" << " : " << " Days until deletion" << std::endl << "======================================================================================================================" << std::endl << std::endl;
  
  for (auto& i:allRelevantDatasetsWithDeletionTimes){
    std::cout << std::setw(200) << std::left;
    std::cout << i.first << " : " << i.second << std::endl;
  }

  std::cout << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl << std::endl;

  std::cout << "Datasets that need extensions (less than 20 days until deletion)" << std::endl << "======================================================================================================================" << std::endl << std::endl;

  for (auto& i:allRelevantDatasetsWithDeletionTimes){
    if (i.second<=20){
      std::cout << i.first << std::endl;
      outputFileForDatasetsRequiringExtension << i.first << std::endl;
    }
  }
  outputFileForDatasetsRequiringExtension.close();
}


std::vector<std::pair<std::string,int>> readListOfDeletionsIntoVectorWithDatasetNameAndDeletionTime(std::string deletionFileAddress){

  std::ifstream deletionFile(deletionFileAddress);
  std::string currentLine;
  std::vector<std::pair<std::string,int>> vectorOfDatasetsWithDeletionTime;

  unsigned int lineNumber{0};
  while (std::getline(deletionFile, currentLine))
    {
      lineNumber++;

      if (currentLine.substr(0,2)!="mc" && currentLine.substr(0,4)!="data") {continue;} //Samples start with either mc or data, skip other lines. 
      
      std::istringstream iss_currentLine(currentLine);
      std::string dataset{"UNSET"};
      iss_currentLine >> dataset;
      
      std::string stringOfDeletionTime = currentLine.substr(225,15); //Deletion list is formatted such that the deletion time is between the 225th and 240th character.
      std::istringstream iss_stringOfDeletionTime(stringOfDeletionTime);  
      int deletionTime{-999999};
      iss_stringOfDeletionTime >> deletionTime;
      
      if (dataset=="UNSET") {throw std::runtime_error("Dataset could not be read on line: " + std::to_string(lineNumber) + " of " + deletionFileAddress);}
      if (deletionTime==-999999) {throw std::runtime_error("Deletion Time was not set for dataset: " + dataset + " on line: " + std::to_string(lineNumber));}
      
      vectorOfDatasetsWithDeletionTime.push_back(std::make_pair(dataset,deletionTime));
    }

    return vectorOfDatasetsWithDeletionTime;
}


std::vector<std::string> readListOfDatasets(std::string datasetAddress){

  std::ifstream deletionFile(datasetAddress);
  std::string currentLine;
  std::vector<std::string> vectorOfDatasets;

  unsigned int lineNumber{0};
  while (std::getline(deletionFile, currentLine))
    {
      lineNumber++;

      currentLine.erase(std::remove(currentLine.begin(), currentLine.end(), '#'), currentLine.end()); //Do not care whether or not the sample is commented out, still want to check if it needs extension      
      if (currentLine.substr(0,2)!="mc" && currentLine.substr(0,4)!="data") {continue;} //Samples start with either mc or data, skip other lines. 
      
      std::istringstream iss_currentLine(currentLine);
      std::string dataset{"UNSET"};
      iss_currentLine >> dataset;

      if (dataset=="UNSET") {throw std::runtime_error("Dataset could not be read on line: " + std::to_string(lineNumber) + " of " + datasetAddress);}      
      
      vectorOfDatasets.push_back(dataset);
    }

    return vectorOfDatasets;
}


std::vector<std::pair<std::string,int>> removeUnusedDatasets(std::vector<std::pair<std::string,int>> deletionList,std::vector<std::string> usedDatasets){

  std::vector<std::pair<std::string,int>> usedDatasetsWithDeletionTimes;
  
  std::regex r1("[_][r][0-9]{4,6}");
  std::regex r2("[_][p][0-9]{4,6}");
  std::regex r3("[.][e][0-9]{4,6}");
  std::regex r4("[_][s][0-9]{4,6}");



  std::smatch m;



  
  for (auto& i : deletionList){ //compare every entry in the list of deletion samples to the samples we are using. If they are not in both, remove them as we don't care about samples we aren't using or samples that aren't going to be deleted.

    if (i.first.substr(0,2)=="mc"){ //mc and data have different naming schemes, treat them seperately
      for (auto& j : usedDatasets){
	//      std::cout << j << std::endl;
	std::string firstRTag_Deletion{"UNSET"},firstPTag_Deletion{"UNSET"},firstETag_Deletion{"UNSET"},firstSTag_Deletion{"UNSET"}; //Due to change in naming scheme the names of our samples are not the same as the names in the deletion list, as the deletion list has extraneous tags. The first tag of each should match ours.
	std::string firstRTag_Ours{"UNSET"},firstPTag_Ours{"UNSET"},firstETag_Ours{"UNSET"},firstSTag_Ours{"UNSET"};

	if(!std::regex_search(i.first, m, r3)) continue; //there must be an etag for our samples.
      
	if(std::regex_search(i.first, m, r3)){ //check dataset names match
	  std::string deletionListName = m.prefix(); 
	  std::regex_search(j, m, r3);
	  std::string ourListName = m.prefix();
	  if (deletionListName!=ourListName) continue;
	}

      
	if(std::regex_search(i.first, m, r1)){ //check (first) r tag match
	  firstRTag_Deletion = m[0];
	  std::regex_search(j, m, r1);
	  firstRTag_Ours = m[0];
	  if (firstRTag_Deletion!=firstRTag_Ours) continue;

	}

	if(std::regex_search(i.first, m, r2)){ //check (first) p tag match
	  firstPTag_Deletion = m[0];
	  std::regex_search(j, m, r2);
	  firstPTag_Ours = m[0];
	  if (firstPTag_Deletion!=firstPTag_Ours) continue;
	}

	if(std::regex_search(i.first, m, r3)){ //check (first) e tag match
	  firstETag_Deletion = m[0];
	  std::regex_search(j, m, r3);
	  firstETag_Ours = m[0];
	  if (firstETag_Deletion!=firstETag_Ours) continue;
	}

	if(std::regex_search(i.first, m, r4)){ //check (first) s tag match
	  firstSTag_Deletion = m[0];
	  std::regex_search(j, m, r4);
	  firstSTag_Ours = m[0];
	  if (firstSTag_Deletion!=firstSTag_Ours) continue;
	}

	usedDatasetsWithDeletionTimes.push_back(i); 
      }
    }
    else{ //Currently with data, the naming scheme for our samples and the dataset deletion list is the same, so no fancy regex is needed, just check if they're the same. If the naming scheme for one changes in the future this will have to be updated.
      for (auto& j : usedDatasets){

	std::string deletionListNameWithTags = i.first;
	std::string ourListNameWithTags = j;
	if (deletionListNameWithTags!=ourListNameWithTags) continue;
	usedDatasetsWithDeletionTimes.push_back(i);
      }
    }
  }

  return usedDatasetsWithDeletionTimes;

}
