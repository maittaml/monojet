#!/bin/bash

outputfile="bkg_mc16e_AOD_TEMP.txt"
etag="e*" #event generation version. You probably don't care what specific one this is, if so, use e*.
sORatag="*" #Simulation version, it is unlikely you care about this. Leave as s* for full sim or a* for ATLAS fast.
rtag="r10724*" #mc16a r=9364, mc16c r=9781, mc16d r=10201, mc16e r=10724
ptag="p*" #Depends on specific derivation release you are looking for. If you just want any EXOT5 derivation, use p*. If you are looking for AOD's not derivations, you do not need to touch this.

ftag="" #Only for data. If you care about which specific f-tag you want, you know more about what the f-tag is than me. 
mtag="" #Only for data. If you care about which specific m-tag you want, you know more about what the m-tag is than me. 

filetype="deriv.DAOD_EXOT5" #recon.AOD for AOD, deriv.DAOD_EXOT5 for EXOT5. (DO NOT END YOUR FILETYPE WITH A . HAVE IT LIKE recon.AOD )

if [ "$filetype" == "recon.AOD" ]
then
	ptag=""
fi


    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh

    lsetup rucio
    voms-proxy-init -voms atlas
printf "\n"  | tee $outputfile
printf "\n"  | tee -a $outputfile
printf "\n"  | tee -a $outputfile

#Explaining the weird piping to all the filenames: A typical output from rucio with --filter type =container (this is done to just get containers not datasets, containers 'contain' datasets) is like this:
#+--------------------------------------------------------------------------------------------------------------+--------------+
#| SCOPE:NAME                                                                                                   | [DID TYPE]   |
#|--------------------------------------------------------------------------------------------------------------+--------------|
#| mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.recon.AOD.e3569_s3126_r10201_r10210      | CONTAINER    |
#| mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.recon.AOD.e3569_s3126_r10201             | CONTAINER    |
#+--------------------------------------------------------------------------------------------------------------+--------------+
# We are obviously only interested in the file, not the lines that say SCOPE:NAME for instance, or the lines that are just +------------------------+-------+. So piped to tail -n2 to take the second to last line to the end, and
# then sed to remove the last line.
#
# Then cut -d':' -f2 to get everything after the first colon, as before the first colon is just the scope mc16_13TeV, then  cut -d ' ' -f1  to get everything before the first space, as after the first space is just | CONTAINER    |
# So after all of this, just the filename is printed (.... that this isn't just an option in rucio is ridiculous)
#
# If no sample is found then the second to last line will be 
# |--------------+--------------|, check if this is the output and if it is then return the query so we know what samples are missing. Elsewise return the file.
#
# then tee -a just appends to file, first tee (printf above this comment block) does not append and overwrites the previous file.
# (If you do not want any output to the terminal then replace "| tee -a" with ">>" and the first "| tee" (that has no -a) with ">"


function extractFileName(){
	#$1 is the input to rucio which is passed to the function, $2 is the number of lines above the bottom (last line counts as 1)	

	OUTPUT=$(rucio ls $1$filetype".*"$ftag$mtag$etag$sORatag$rtag$ptag   --filter type=container  | tail "-n"$2 | sed -n -e 1p  | cut -d':' -f2 | cut -d ' ' -f1 )

	if [[ $OUTPUT = *"--------------|"* ]]; then #NO SAMPLE FOUND
		echo "########## rucio ls "$1$filetype".*"$ftag$mtag$etag$sORatag$rtag$ptag | tee -a $outputfile
	else #SAMPLE FOUND
		if [[ $OUTPUT = *$filetype"."*"_e"* ]]; then #OLD NAMING SCHEME (new naming scheme only ever has a .e not a _e in the tags). TRY ONE SAMPLE UP.
			let "lineNumber = $2 + 1"
			extractFileName $1 $lineNumber
		elif  [[ $OUTPUT = *$filetype"."*$rtag"_r"* ]]; then #OLD NAMING SCHEME (HAS TWO RTAGS). TRY ONE SAMPLE UP
			let "lineNumber = $2 + 1"
			extractFileName $1 $lineNumber
		elif  [[ $OUTPUT = *$filetype"."*$ptag"_p"* ]]; then #OLD NAMING SCHEME (HAS TWO PTAGS). TRY ONE SAMPLE UP
			let "lineNumber = $2 + 1"
			extractFileName $1 $lineNumber
		else
			echo $OUTPUT | tee -a $outputfile #sample found and not old naming scheme #SAMPLE FOUND OF NEW NAMING SCHEME.
		fi
	fi
}



printf "#dijets, details here https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MultijetFocusGroup in https://docs.google.com/spreadsheets/d/1dw72LZ_eiXbSdIj2ChDdNgIX8PGs5VeYgLE2Y8f-eAs/edit#gid=0 , the JZ/JZW slicing is in pt of the colliding partons. In mc16_13TeV.426005.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ5. there is a single event with a large mc weight that should be removed by hand as recommended by the multijet task force. There is a discontinuity at a pt of 60GeV betweeen (JZ0W,JZ1W) and JZ2W.\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361022.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ2W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361023.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ3W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361025.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ5W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361026.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ6W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361027.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ7W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361028.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ8W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361029.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ9W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361030.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ10W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361031.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ11W." 2
extractFileName "mc16_13TeV:mc16_13TeV.361032.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ12W." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.426001.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ1." 2
extractFileName "mc16_13TeV:mc16_13TeV.426002.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ2." 2
extractFileName "mc16_13TeV:mc16_13TeV.426003.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ3." 2
extractFileName "mc16_13TeV:mc16_13TeV.426004.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ4." 2
extractFileName "mc16_13TeV:mc16_13TeV.426005.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ5." 2
extractFileName "mc16_13TeV:mc16_13TeV.426006.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ6." 2
extractFileName "mc16_13TeV:mc16_13TeV.426007.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ7." 2
extractFileName "mc16_13TeV:mc16_13TeV.426008.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ8." 2
extractFileName "mc16_13TeV:mc16_13TeV.426009.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ9." 2
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "#dibosons details here https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MultibosonFocusGroup#MC16_sample_status\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll." 2
extractFileName "mc16_13TeV:mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv." 2
extractFileName "mc16_13TeV:mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv." 2
extractFileName "mc16_13TeV:mc16_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv." 2
extractFileName "mc16_13TeV:mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll." 2
extractFileName "mc16_13TeV:mc16_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv." 2
extractFileName "mc16_13TeV:mc16_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll." 2
extractFileName "mc16_13TeV:mc16_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv." 2
extractFileName "mc16_13TeV:mc16_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq." 2
extractFileName "mc16_13TeV:mc16_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq." 2
extractFileName "mc16_13TeV:mc16_13TeV.363494.Sherpa_221_NNPDF30NNLO_vvvv." 2
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "#tribosons, onshell details here https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MultibosonFocusGroup#MC16_sample_status\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364242.Sherpa_222_NNPDF30NNLO_WWW_3l3v_EW6." 2
extractFileName "mc16_13TeV:mc16_13TeV.364243.Sherpa_222_NNPDF30NNLO_WWZ_4l2v_EW6." 2
extractFileName "mc16_13TeV:mc16_13TeV.364244.Sherpa_222_NNPDF30NNLO_WWZ_2l4v_EW6." 2
extractFileName "mc16_13TeV:mc16_13TeV.364245.Sherpa_222_NNPDF30NNLO_WZZ_5l1v_EW6." 2
extractFileName "mc16_13TeV:mc16_13TeV.364246.Sherpa_222_NNPDF30NNLO_WZZ_3l3v_EW6." 2
extractFileName "mc16_13TeV:mc16_13TeV.364247.Sherpa_222_NNPDF30NNLO_ZZZ_6l0v_EW6." 2
extractFileName "mc16_13TeV:mc16_13TeV.364248.Sherpa_222_NNPDF30NNLO_ZZZ_4l2v_EW6." 2
extractFileName "mc16_13TeV:mc16_13TeV.364249.Sherpa_222_NNPDF30NNLO_ZZZ_2l4v_EW6." 2
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "#V+jets details here
#https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BosonJetsFocusGroup#MC16_sample_status
#IMPORTANT NOTE: if you are using the PTV500_1000 and PTV1000_E_CMS samples ( 364216-364229 ) you must follow the instructions on page 3 of this page https://indico.cern.ch/event/592226/contributions/2390290/attachments/1383938/2105135/#MC_Perf_Intro_20161206.pdf to prevent overlap with the max(HT,pTV) slices. It is unlikely that the Mll sliced samples are useful for us. The Vjj samples (308092-308098) do not overlap with the baseline sample so can be combined if #desired. These Vjj samples include VBF and V strahlung but not do include semileptonic VV.\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364129.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364130.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364132.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364134.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364135.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364136.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364138.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364139.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364140.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364141.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364142.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364143.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364144.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364145.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364146.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364147.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364148.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364149.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364150.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364151.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364152.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364153.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364154.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364155.Sherpa_221_NNPDF30NNLO_Znunu_MAXHTPTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364157.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364162.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364163.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364166.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364177.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364180.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364184.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364185.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364191.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364192.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364193.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364194.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364195.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.308092.Sherpa_221_NNPDF30NNLO_Zee2jets_Min_N_TChannel." 2
extractFileName "mc16_13TeV:mc16_13TeV.308093.Sherpa_221_NNPDF30NNLO_Zmm2jets_Min_N_TChannel." 2
extractFileName "mc16_13TeV:mc16_13TeV.308094.Sherpa_221_NNPDF30NNLO_Ztautau2jets_Min_N_TChannel." 2
extractFileName "mc16_13TeV:mc16_13TeV.308095.Sherpa_221_NNPDF30NNLO_Znunu2jets_Min_N_TChannel." 2
extractFileName "mc16_13TeV:mc16_13TeV.308096.Sherpa_221_NNPDF30NNLO_Wenu2jets_Min_N_TChannel." 2
extractFileName "mc16_13TeV:mc16_13TeV.308097.Sherpa_221_NNPDF30NNLO_Wmunu2jets_Min_N_TChannel." 2
extractFileName "mc16_13TeV:mc16_13TeV.308098.Sherpa_221_NNPDF30NNLO_Wtaunu2jets_Min_N_TChannel." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364216.Sherpa_221_NNPDF30NNLO_Zmumu_PTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364217.Sherpa_221_NNPDF30NNLO_Zmumu_PTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364218.Sherpa_221_NNPDF30NNLO_Zee_PTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364219.Sherpa_221_NNPDF30NNLO_Zee_PTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364220.Sherpa_221_NNPDF30NNLO_Ztautau_PTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364221.Sherpa_221_NNPDF30NNLO_Ztautau_PTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364222.Sherpa_221_NNPDF30NNLO_Znunu_PTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364223.Sherpa_221_NNPDF30NNLO_Znunu_PTV1000_E_CMS." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364224.Sherpa_221_NNPDF30NNLO_Wmunu_PTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364225.Sherpa_221_NNPDF30NNLO_Wmunu_PTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364226.Sherpa_221_NNPDF30NNLO_Wenu_PTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364227.Sherpa_221_NNPDF30NNLO_Wenu_PTV1000_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364228.Sherpa_221_NNPDF30NNLO_Wtaunu_PTV500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364229.Sherpa_221_NNPDF30NNLO_Wtaunu_PTV1000_E_CMS." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364199.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364200.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364201.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364202.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364203.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BFilter." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364204.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364205.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364206.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364207.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364208.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364209.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BFilter." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364210.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364211.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364212.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364213.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.364214.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_BVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.364215.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_BFilter." 2
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "#gamma + jets, details here https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BosonJetsFocusGroup#jets
#https://indico.cern.ch/event/592226/contributions/2390290/attachments/1383938/2105135/MC_Perf_Intro_20161206.pdf , (364543-364547) are NLO, while (361039-361062) are LO.\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364543.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364544.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_140_280." 2
extractFileName "mc16_13TeV:mc16_13TeV.364545.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_280_500." 2
extractFileName "mc16_13TeV:mc16_13TeV.364546.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.364547.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_1000_E_CMS." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.361039.Sherpa_CT10_SinglePhotonPt35_70_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361040.Sherpa_CT10_SinglePhotonPt35_70_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361041.Sherpa_CT10_SinglePhotonPt35_70_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.361042.Sherpa_CT10_SinglePhotonPt70_140_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361043.Sherpa_CT10_SinglePhotonPt70_140_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.361045.Sherpa_CT10_SinglePhotonPt140_280_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361046.Sherpa_CT10_SinglePhotonPt140_280_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.361048.Sherpa_CT10_SinglePhotonPt280_500_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361049.Sherpa_CT10_SinglePhotonPt280_500_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.361051.Sherpa_CT10_SinglePhotonPt500_1000_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361052.Sherpa_CT10_SinglePhotonPt500_1000_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.361054.Sherpa_CT10_SinglePhotonPt1000_2000_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361055.Sherpa_CT10_SinglePhotonPt1000_2000_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.361057.Sherpa_CT10_SinglePhotonPt2000_4000_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361058.Sherpa_CT10_SinglePhotonPt2000_4000_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361059.Sherpa_CT10_SinglePhotonPt2000_4000_BFilter." 2
extractFileName "mc16_13TeV:mc16_13TeV.361060.Sherpa_CT10_SinglePhotonPt4000_CVetoBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361061.Sherpa_CT10_SinglePhotonPt4000_CFilterBVeto." 2
extractFileName "mc16_13TeV:mc16_13TeV.361062.Sherpa_CT10_SinglePhotonPt4000_BFilter." 2
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "#V+gamma, details here https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MultibosonFocusGroup#MC16_sample_status\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.364500.Sherpa_222_NNPDF30NNLO_eegamma_pty_7_15." 2
extractFileName "mc16_13TeV:mc16_13TeV.364501.Sherpa_222_NNPDF30NNLO_eegamma_pty_15_35." 2
extractFileName "mc16_13TeV:mc16_13TeV.364502.Sherpa_222_NNPDF30NNLO_eegamma_pty_35_70." 2
extractFileName "mc16_13TeV:mc16_13TeV.364503.Sherpa_222_NNPDF30NNLO_eegamma_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364504.Sherpa_222_NNPDF30NNLO_eegamma_pty_140_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364505.Sherpa_222_NNPDF30NNLO_mumugamma_pty_7_15." 2
extractFileName "mc16_13TeV:mc16_13TeV.364506.Sherpa_222_NNPDF30NNLO_mumugamma_pty_15_35." 2
extractFileName "mc16_13TeV:mc16_13TeV.364507.Sherpa_222_NNPDF30NNLO_mumugamma_pty_35_70." 2
extractFileName "mc16_13TeV:mc16_13TeV.364508.Sherpa_222_NNPDF30NNLO_mumugamma_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364509.Sherpa_222_NNPDF30NNLO_mumugamma_pty_140_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364510.Sherpa_222_NNPDF30NNLO_tautaugamma_pty_7_15." 2
extractFileName "mc16_13TeV:mc16_13TeV.364511.Sherpa_222_NNPDF30NNLO_tautaugamma_pty_15_35." 2
extractFileName "mc16_13TeV:mc16_13TeV.364512.Sherpa_222_NNPDF30NNLO_tautaugamma_pty_35_70." 2
extractFileName "mc16_13TeV:mc16_13TeV.364513.Sherpa_222_NNPDF30NNLO_tautaugamma_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364514.Sherpa_222_NNPDF30NNLO_tautaugamma_pty_140_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364515.Sherpa_222_NNPDF30NNLO_nunugamma_pty_7_15." 2
extractFileName "mc16_13TeV:mc16_13TeV.364516.Sherpa_222_NNPDF30NNLO_nunugamma_pty_15_35." 2
extractFileName "mc16_13TeV:mc16_13TeV.364517.Sherpa_222_NNPDF30NNLO_nunugamma_pty_35_70." 2
extractFileName "mc16_13TeV:mc16_13TeV.364518.Sherpa_222_NNPDF30NNLO_nunugamma_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364519.Sherpa_222_NNPDF30NNLO_nunugamma_pty_140_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364521.Sherpa_222_NNPDF30NNLO_enugamma_pty_7_15." 2
extractFileName "mc16_13TeV:mc16_13TeV.364522.Sherpa_222_NNPDF30NNLO_enugamma_pty_15_35." 2
extractFileName "mc16_13TeV:mc16_13TeV.364523.Sherpa_222_NNPDF30NNLO_enugamma_pty_35_70." 2
extractFileName "mc16_13TeV:mc16_13TeV.364524.Sherpa_222_NNPDF30NNLO_enugamma_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364525.Sherpa_222_NNPDF30NNLO_enugamma_pty_140_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364526.Sherpa_222_NNPDF30NNLO_munugamma_pty_7_15." 2
extractFileName "mc16_13TeV:mc16_13TeV.364527.Sherpa_222_NNPDF30NNLO_munugamma_pty_15_35." 2
extractFileName "mc16_13TeV:mc16_13TeV.364528.Sherpa_222_NNPDF30NNLO_munugamma_pty_35_70." 2
extractFileName "mc16_13TeV:mc16_13TeV.364529.Sherpa_222_NNPDF30NNLO_munugamma_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364530.Sherpa_222_NNPDF30NNLO_munugamma_pty_140_E_CMS." 2
extractFileName "mc16_13TeV:mc16_13TeV.364531.Sherpa_222_NNPDF30NNLO_taunugamma_pty_7_15." 2
extractFileName "mc16_13TeV:mc16_13TeV.364532.Sherpa_222_NNPDF30NNLO_taunugamma_pty_15_35." 2
extractFileName "mc16_13TeV:mc16_13TeV.364533.Sherpa_222_NNPDF30NNLO_taunugamma_pty_35_70." 2
extractFileName "mc16_13TeV:mc16_13TeV.364534.Sherpa_222_NNPDF30NNLO_taunugamma_pty_70_140." 2
extractFileName "mc16_13TeV:mc16_13TeV.364535.Sherpa_222_NNPDF30NNLO_taunugamma_pty_140_E_CMS." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.305435.Sherpa_CT10_WqqGammaPt140_280." 2
extractFileName "mc16_13TeV:mc16_13TeV.305436.Sherpa_CT10_WqqGammaPt280_500." 2
extractFileName "mc16_13TeV:mc16_13TeV.305437.Sherpa_CT10_WqqGammaPt500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.305438.Sherpa_CT10_WqqGammaPt1000_2000." 2
extractFileName "mc16_13TeV:mc16_13TeV.305439.Sherpa_CT10_WqqGammaPt2000_inf." 2
extractFileName "mc16_13TeV:mc16_13TeV.305440.Sherpa_CT10_ZqqGammaPt140_280." 2
extractFileName "mc16_13TeV:mc16_13TeV.305441.Sherpa_CT10_ZqqGammaPt280_500." 2
extractFileName "mc16_13TeV:mc16_13TeV.305442.Sherpa_CT10_ZqqGammaPt500_1000." 2
extractFileName "mc16_13TeV:mc16_13TeV.305443.Sherpa_CT10_ZqqGammaPt1000_2000." 2
extractFileName "mc16_13TeV:mc16_13TeV.305444.Sherpa_CT10_ZqqGammaPt2000_inf." 2
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
printf "#top samples. The commented out samples (410011,410012,410013,410014,410025,410026,410501,410503) are now obsolete. Retained in case we need to compare to. Details here, https://twiki.cern.ch/twiki/bin/view//TopFocusGroup\n"  | tee -a  $outputfile
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.410464.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_SingleLep." 2
extractFileName "mc16_13TeV:mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil." 2
extractFileName "mc16_13TeV:mc16_13TeV.410466.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_AllHadronic." 2
extractFileName "mc16_13TeV:mc16_13TeV.410557.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_SingleLep." 2
extractFileName "mc16_13TeV:mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil." 2
extractFileName "mc16_13TeV:mc16_13TeV.410559.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_allhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.410480.PhPy8EG_A14_ttbar_hdamp517p5_SingleLep." 2
extractFileName "mc16_13TeV:mc16_13TeV.410481.PhPy8EG_A14_ttbar_hdamp517p5_allhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil." 2
extractFileName "mc16_13TeV:mc16_13TeV.407342.PhPy8EG_A14_ttbarHT1k5_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.407343.PhPy8EG_A14_ttbarHT1k_1k5_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.407344.PhPy8EG_A14_ttbarHT6c_1k_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.407346.PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.407347.PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.410424.Sherpa_224_NNPDF30NNLO_ttbar_dilepton_MEPS_NLO." 2
extractFileName "mc16_13TeV:mc16_13TeV.410425.Sherpa_224_NNPDF30NNLO_ttbar_SingleLeptonM_MEPS_NLO." 2
extractFileName "mc16_13TeV:mc16_13TeV.410426.Sherpa_224_NNPDF30NNLO_ttbar_SingleLeptonP_MEPS_NLO." 2
extractFileName "mc16_13TeV:mc16_13TeV.410427.Sherpa_224_NNPDF30NNLO_ttbar_AllHadronic_MEPS_NLO." 2
extractFileName "mc16_13TeV:mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top." 2
extractFileName "mc16_13TeV:mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop." 2
extractFileName "mc16_13TeV:mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top." 2
extractFileName "mc16_13TeV:mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop." 2
extractFileName "mc16_13TeV:mc16_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top." 2
extractFileName "mc16_13TeV:mc16_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop." 2
extractFileName "mc16_13TeV:mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top." 2
extractFileName "mc16_13TeV:mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop." 2
printf "\n"  | tee -a  $outputfile
extractFileName "mc16_13TeV:mc16_13TeV.410011.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_top." 2
extractFileName "mc16_13TeV:mc16_13TeV.410012.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_antitop." 2
extractFileName "mc16_13TeV:mc16_13TeV.410013.PowhegPythiaEvtGen_P2012_Wt_inclusive_top." 2
extractFileName "mc16_13TeV:mc16_13TeV.410014.PowhegPythiaEvtGen_P2012_Wt_inclusive_antitop." 2
extractFileName "mc16_13TeV:mc16_13TeV.410025.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_top." 2
extractFileName "mc16_13TeV:mc16_13TeV.410026.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_antitop." 2
extractFileName "mc16_13TeV:mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad." 2
extractFileName "mc16_13TeV:mc16_13TeV.410503.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_dil." 2
