# Set the name of the package:
atlas_subdir( MonoJetAnalysis )

# Set up which packages this package depends on:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthToolSupport/AsgTools
   PhysicsAnalysis/AnalysisCommon/PATCore
   PhysicsAnalysis/D3PDTools/EventLoop
   PhysicsAnalysis/D3PDTools/EventLoopGrid
   PhysicsAnalysis/D3PDTools/EventLoopAlgs
   PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces
   Control/AthContainers
   Control/CxxUtils
   Control/xAODRootAccess
   Event/xAOD/xAODBase
   Event/xAOD/xAODCore
   Event/xAOD/xAODEgamma
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODTracking
   Event/xAOD/xAODJet
   Event/xAOD/xAODMetaData
   Event/xAOD/xAODMuon
   Event/xAOD/xAODTau
   Event/xAOD/xAODCaloEvent
   Event/xAOD/xAODTruth
   Event/xAOD/xAODMissingET
   Event/xAOD/xAODBTaggingEfficiency
   Event/xAOD/xAODTrigger
   Event/xAOD/xAODCutFlow
   PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools
   PhysicsAnalysis/TauID/TauAnalysisTools
   PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection
   PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection #mono-photon
   Trigger/TrigAnalysis/TrigDecisionTool
   Trigger/TrigConfiguration/TrigConfxAOD
   Trigger/TrigAnalysis/TriggerMatchingTool
   Tools/PathResolver
   PhysicsAnalysis/AnalysisCommon/IsolationSelection
   PhysicsAnalysis/AnalysisCommon/PhotonVertexSelection #mono-photon
   PhysicsAnalysis/AnalysisCommon/PileupReweighting
   DataQuality/GoodRunsLists
   PhysicsAnalysis/SUSYPhys/SUSYTools
   PhysicsAnalysis/AnalysisCommon/PMGTools #mono-photon

)



# External(s) used by the package:
find_package( ROOT COMPONENTS Core Hist Physics REQUIRED )

# Generate a dictionary for the library:
atlas_add_root_dictionary( MonoJetAnalysisLib MonoJetAnalysisLibDictSource
   ROOT_HEADERS MonoJetAnalysis/*.h Root/LinkDef.h
   EXTERNAL_PACKAGES ROOT )

# Build the shared library of the package:
atlas_add_library( MonoJetAnalysisLib
   MonoJetAnalysis/*.h Root/*.h Root/*.cxx ${MonoJetAnalysisLibDictSource}
   PUBLIC_HEADERS MonoJetAnalysis
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES AsgTools PATCoreLib EventLoop AsgAnalysisInterfaces
   ${ROOT_LIBRARIES} AthContainers CxxUtils
   xAODRootAccess xAODBase xAODCore xAODEgamma xAODTau xAODJet xAODMetaData xAODMuon xAODMissingET
   xAODEventInfo xAODTracking xAODCaloEvent xAODTruth xAODCutFlow xAODTrigger
   GoodRunsListsLib PileupReweightingLib PathResolver TrigConfxAODLib TrigDecisionToolLib TriggerMatchingToolLib
   MuonMomentumCorrectionsLib FourMomUtils IsolationSelectionLib IsolationCorrectionsLib
   ElectronPhotonSelectorToolsLib TauAnalysisToolsLib ElectronEfficiencyCorrectionLib PhotonEfficiencyCorrectionLib
   SUSYToolsLib PMGToolsLib PMGAnalysisInterfacesLib
)

# make files in "share" visible to PathResolver
atlas_install_data( share/* )

