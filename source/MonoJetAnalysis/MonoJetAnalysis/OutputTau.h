#ifndef __Analysis_OutputTau__
#define __Analysis_OutputTau__

// EDM's include(s):
#include <xAODTau/TauJet.h>

// Local include(s):
#include <MonoJetAnalysis/OutputObject.h>
#include <map>

namespace Analysis {

  class OutputTau : public OutputObject {
    
    //  private:
  public:

    std::vector<double> SF;
    //    std::vector<double> SF_iso;
    std::map<TString, std::vector<double>> tau_SF_Sys;
    
    std::vector<Float_t> pt;
    std::vector<Float_t> eta;
    std::vector<Float_t> phi;
    std::vector<Float_t> m;
    
    std::vector<Int_t> idtool_pass_veryloose;
    std::vector<Int_t> idtool_pass_loose;
    std::vector<Int_t> idtool_pass_medium;
    std::vector<Int_t> idtool_pass_tight;
    
    Int_t veryloose_multiplicity;
    Int_t loose_multiplicity;
    Int_t medium_multiplicity;
    Int_t tight_multiplicity;
    
    std::vector<Float_t> truth_pt;
    std::vector<Float_t> truth_eta;
    std::vector<Float_t> truth_phi;
    std::vector<Int_t> truth_status;
    std::vector<Int_t> truth_type;
    std::vector<Int_t> truth_origin;

  public:
    
    OutputTau(TString name = "", Bool_t doTrim = false/*, Bool_t doMonophoton = kFALSE*/);
    ~OutputTau() {};
    
    void reset();
    void attachToTree(TTree *tree);
    void add(const xAOD::TauJet &input);
    void add_SFs(const xAOD::TauJet &input, std::string Sys);

  };
}

#endif // __Analysis_OutputTau__
