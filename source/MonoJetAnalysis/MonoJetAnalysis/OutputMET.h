#ifndef __Analysis_OutputMET__
#define __Analysis_OutputMET__

#include <MonoJetAnalysis/OutputObject.h>
#include <xAODMissingET/MissingET.h>

namespace Analysis {
   class OutputMET : public OutputObject {
   public:
      Float_t et;
      Float_t etx;
      Float_t ety;
      Float_t sumet;
      Float_t phi;

   public:
      OutputMET(TString name = "", Bool_t doTrim = kFALSE/*, Bool_t doMonophoton = kFALSE*/);
      ~OutputMET();
      void reset();
      void attachToTree(TTree *tree);
      void add(const xAOD::MissingET &input);
   };
}

#endif

