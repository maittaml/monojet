#ifndef MonoJetAnalysis_MonoJet_H
#define MonoJetAnalysis_MonoJet_H

#include <EventLoop/Algorithm.h>
//#include "xAODEventInfo/EventInfo.h"
#include <AsgTools/AnaToolHandle.h>

#include <PathResolver/PathResolver.h>
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <SUSYTools/ISUSYObjDef_xAODTool.h>
#include "IsolationCorrections/IsolationCorrectionTool.h"
#include <PhotonVertexSelection/IPhotonVertexSelectionTool.h> 
#include <PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h>
#include <TauAnalysisTools/TauSelectionTool.h>
#include <ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h>
 
//#include <TauAnalysisTools/ITauSelectionTool.h>

//MJ classes
#include <MonoJetAnalysis/ContentHolder.h>
#include <MonoJetAnalysis/CutFlowTool.h>
#include <MonoJetAnalysis/Enums.h>
#include <MonoJetAnalysis/Output.h>
#include <SUSYTools/SUSYCrossSection.h>  //MonoPhoton
#include <PMGTools/PMGTruthWeightTool.h> //MonoPhoton
#include "AssociationUtils/IOverlapRemovalTool.h"

class TH1F;
class TH2F;


class MonoJet : public EL::Algorithm {
   // put your configuration variables here as public variables.
   // that way they can be set directly from CINT and python.
public:
   // float cutValue;
   TString config_file;
   TString ST_config_file;
   TString PrwActualMu2017File;
   TString PrwActualMu2018File;
   TString prw_file;
   TString lumicalc_file;
   TString GRL_file;
   TString MC_campaign;
   TString forbidden_syst;
   TString trigger_list;
   Double_t metSkim;
   Double_t metSkimForSyst;
   Double_t ptSkim;
   Double_t ptSkimForSyst;
   Bool_t m_doSystematics;
   Bool_t m_doTheoSystematics;  //theo syst
   Bool_t m_doIncludeTaus; 
   //   TString m_runOnGrid;
   TString m_runOnGrid;
   Bool_t doMonophoton;   //MonoPhoton
   Bool_t cutSystematics;   //Systematics reorganization
   Bool_t m_doSkim;
   Bool_t m_doTrim;
   TString MC_sample;

   // variables that don't get filled at submission time should be
   // protected from being send from the submission node to the worker
   // node (done by the //!)
public:

   asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
   asg::AnaToolHandle<ST::ISUSYObjDef_xAODTool> m_objTool; //!
   asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> m_electronIsolationSFTool; //!
   asg::AnaToolHandle<CP::IIsolationSelectionTool> m_leptonIsolationLooseTrackOnly; //!
   asg::AnaToolHandle<CP::IPhotonPointingTool> m_photonPointingTool; //!  MonoPhoton 
   asg::AnaToolHandle<IAsgPhotonIsEMSelector> m_photonTightIsEMSelector; //!  MonoPhoton 
   asg::AnaToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonIsoSF; //!  MonoPhoton 
   asg::AnaToolHandle<IAsgPhotonEfficiencyCorrectionTool> m_photonIDSF; //!  MonoPhoton 


   // asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> m_tauLoose; //!
   //asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> m_tauMedium; //!
   //asg::AnaToolHandle<TauAnalysisTools::ITauSelectionTool> m_tauTight; //!
   std::unique_ptr<TauAnalysisTools::TauSelectionTool> m_tauVeryLoose; //!
   std::unique_ptr<TauAnalysisTools::TauSelectionTool> m_tauLoose; //!
   std::unique_ptr<TauAnalysisTools::TauSelectionTool> m_tauMedium; //!
   std::unique_ptr<TauAnalysisTools::TauSelectionTool> m_tauTight; //!

   asg::AnaToolHandle<IAsgElectronLikelihoodTool> m_elecSelLikelihoodBaseline; //!

   //Used in the analysis
   Bool_t m_determinedDerivation; //!
   Bool_t m_isEXOT5; //!
   Bool_t m_isEXOT6; //!
   TString m_inputFileName; //!
   Analysis::CutFlowTool m_cutFlow; //!
   Int_t m_eventCounter; //!
   Bool_t m_isMC; //!
   Bool_t m_isMC_Wenu_Zee; //!
   Bool_t m_isAFII; //!
   std::vector<ST::SystInfo> m_sysList; //!
   std::map<TString, TTree*> m_tree; //!
   std::map<TString, TH2F*> m_mapEleSF; //!  antiSF MonoPhoton 
   std::map<TString, TH2F*> m_mapMuSF; //!  antiSF MonoPhoton 
   std::map<TString, TH2F*> m_mapTauSF; //!  antiSF MonoPhoton 
   std::map<TString, TH1F*> m_mapCountSF; //!  antiSF MonoPhoton 
   std::map<TString, Analysis::Output> m_cand; //!
   MonoJetCuts::CutID m_last; //!
   MonoPhotonCuts::CutID m_last_monophoton; //!  
   
   Analysis::ContentHolder m_content_current; //!
   Analysis::ContentHolder m_content_nominal; //!

   TH1F *m_histoEventCount; //!
   TH1F *m_histoEventCountNonNominal; //!   theo syst
   TH1F *m_histoEventCountNonNominal_PMG; //!  theo syst
   TH2F *m_histoEleSF; //!  antiSF MonoPhoton 
   TH2F *m_histoMuSF; //!  antiSF MonoPhoton 
   TH2F *m_histoTauSF; //!  antiSF MonoPhoton 
   TH1F *m_histoCountSF; //!  antiSF MonoPhoton 

   SUSY::CrossSectionDB *m_xSec_SUSY; //! MonoPhoton 
   PMGTools::PMGTruthWeightTool *m_weightTool; //! MonoPhoton 

   // this is a standard constructor
   MonoJet();

   // these are the functions inherited from Algorithm
   virtual EL::StatusCode setupJob(EL::Job& job);
   virtual EL::StatusCode fileExecute();
   virtual EL::StatusCode histInitialize();
   virtual EL::StatusCode changeInput(bool firstFile);
   virtual EL::StatusCode initialize();
   virtual EL::StatusCode execute();
   virtual EL::StatusCode postExecute();
   virtual EL::StatusCode finalize();
   virtual EL::StatusCode histFinalize();

   // analysis
   virtual EL::StatusCode readConfig();
   virtual EL::StatusCode getMCcampaignInfo(TString lumicalc_file, const xAOD::EventInfo evtInfo,  std::string amiTag, std::vector<std::string> &prw_lumicalc, TString &mc_campaign);
   virtual EL::StatusCode analyzeEvent(Analysis::ContentHolder &content, const ST::SystInfo &systInfo, Analysis::Output &cand);
   virtual EL::StatusCode getLastCutPassed(Analysis::ContentHolder &content, MonoJetCuts::CutID &last);
   virtual EL::StatusCode getLastCutPassedMonophoton(Analysis::ContentHolder & content, MonoPhotonCuts::CutID & last); 
   virtual EL::StatusCode getMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer* jet, xAOD::ElectronContainer* el, xAOD::MuonContainer* mu, xAOD::PhotonContainer* ph, xAOD::TauJetContainer* tau, Bool_t doTST, Bool_t doJVT, xAOD::IParticleContainer *invis);
   virtual EL::StatusCode getTrackMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer * jet, xAOD::ElectronContainer * el, xAOD::MuonContainer * mu);
   virtual EL::StatusCode fillTree(Analysis::ContentHolder &content, Analysis::Output &cand, MonoJetCuts::CutID &last);  
   virtual EL::StatusCode fillTreeMonophoton(Analysis::ContentHolder &content, Analysis::Output &cand, MonoPhotonCuts::CutID &last, CP::SystematicSet sys);  
   virtual EL::StatusCode isRegion(Analysis::ContentHolder &content, Analysis::Output &cand);
   virtual EL::StatusCode antiSF(Analysis::ContentHolder &content, Analysis::Output & cand,  CP::SystematicSet sys); 

   // this is needed to distribute the algorithm to the workers
   ClassDef(MonoJet, 1);

   virtual bool isPassMETTrigger(int rn);
   virtual int getRun(Int_t run, TString inputFileName); //F.C patch for Znunu buggy samples -->to remove when good samples will be available
   virtual void GetElectronSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys);
   virtual void GetMuonSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys);
   virtual void GetPhotonSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys);
   virtual void GetTauSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys);

};

#endif
