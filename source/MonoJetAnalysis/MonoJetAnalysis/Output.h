#ifndef __Analysis_Output__
#define __Analysis_Output__

#include <MonoJetAnalysis/OutputEvent.h>
#include <MonoJetAnalysis/OutputMET.h>
#include <MonoJetAnalysis/OutputMuon.h>
#include <MonoJetAnalysis/OutputElectron.h>
#include <MonoJetAnalysis/OutputJet.h>
//#include <MonoJetAnalysis/OutputJetMonophoton.h>
#include <MonoJetAnalysis/OutputPhoton.h>
#include <MonoJetAnalysis/OutputTau.h>

#include <TString.h>
#include <map>

namespace Analysis {
   class Output {
   private:
      TTree *m_attachedTree;
      Bool_t m_doTrim;
      Bool_t m_doMonophoton;
      Bool_t m_isData;

   public:
      OutputEvent evt;
      std::map<TString, OutputMET> met;
      std::map<TString, OutputMuon> mu;
      std::map<TString, OutputElectron> el;
      std::map<TString, OutputJet> jet;
      std::map<TString, OutputPhoton> ph;
      std::map<TString, OutputTau> tau;
      
      


   public:
      Output();
      ~Output();
      void reset();    				
      void setDoTrim(Bool_t val);
      void setDoMonophoton(Bool_t val);	
      void setIsData(Bool_t val);		
      void attachToTree(TTree *tree);		
      void save();
   };
}

#endif
