// Interface class for OutputXXX classes
// Valerio Ippolito - Harvard University

#ifndef __Analysis_OutputObject__
#define __Analysis_OutputObject__

#include <TString.h>

class TTree;

namespace Analysis {
   class OutputObject {
   private:
      TString m_name;
      Bool_t m_doTrim;
      Bool_t m_write;
      Bool_t m_doMonophoton ;  
      Bool_t m_isData ; 

   public:

      inline OutputObject(TString name = "", Bool_t doTrim = kFALSE, Bool_t doMonophoton = kFALSE, Bool_t isData = kFALSE)
      {
         m_name = name;
         m_doTrim = doTrim;
	 m_doMonophoton = doMonophoton ; 
	 m_isData = isData ; 
         m_write = true;
      }

      inline TString name()
      {
         return m_name;
      }

      inline Bool_t doTrim()
      {
         return m_doTrim;
      }

      inline Bool_t doMonophoton()  
      {
         return m_doMonophoton;
      }


      inline Bool_t isData()  
      {
         return m_isData;
      }

      inline Bool_t write()
      {
         return m_write;
      }

      inline void setName(TString val)
      {
         m_name = val;
      }

      inline void setDoTrim(Bool_t val)
      {
         m_doTrim = val;
      }
	
      inline void setDoMonophoton(Bool_t val)
      {
         m_doMonophoton = val;
      }

      inline void setWrite(Bool_t val)
      {
         m_write = val;
      }

      inline void setIsData(Bool_t val)
      {
         m_isData = val;
      }


   public:
      inline virtual ~OutputObject() = 0;
      virtual void reset() = 0;
      virtual void attachToTree(TTree *tree) = 0;
   };
}

Analysis::OutputObject::~OutputObject()
{
}

#endif
