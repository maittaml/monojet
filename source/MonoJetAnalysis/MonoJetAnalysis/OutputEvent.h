// Container for all those variables which are event-related
// and are too complex to read automatically from xAOD
// (unlike e.g. jets)

#ifndef __Analysis_OutputEvent__
#define __Analysis_OutputEvent__

#include <MonoJetAnalysis/OutputObject.h>
#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <map>

namespace Analysis {
   class OutputEvent : public OutputObject {
   public:
      // event variables
      Int_t run;
      ULong64_t event;
      Int_t lbn;
      Int_t bcid;
      Int_t last; // referred to SR
      Int_t selected; // referred to SR
      Int_t year;
      Float_t averageIntPerXing;
      Float_t actualIntPerXing;
      Float_t corAverageIntPerXing;
      Float_t corActualIntPerXing;
      Float_t pu_weight;
      Float_t pu_hash;
      Float_t mconly_weight;
      std::vector<Float_t> mconly_weights;
      // Float_t btag_weight;
      Float_t jvt_all_weight;
      Float_t jvt_weight;
      Float_t pdf_x1;
      Float_t pdf_x2;
      Float_t pdf_pdf1;
      Float_t pdf_pdf2;
      Float_t pdf_scale;
      std::map<TString, Int_t> trigger;
      Int_t trigger_matched_electron;
      Int_t trigger_matched_muon;
      Int_t trigger_matched_HLT_e60_lhmedium;
      Int_t trigger_matched_HLT_e120_lhloose;
      Int_t trigger_matched_HLT_e24_lhmedium_L1EM18VH;
      Int_t trigger_matched_HLT_e24_lhmedium_L1EM20VH;
      //      Bool_t trigger_pass;
      Int_t trigger_pass;
      Int_t hfor;
      Int_t n_vx; // DEPRECATED
      Int_t n_ph;
      Int_t n_ph_tight;
      Int_t n_ph_baseline;
      Int_t n_ph_baseline_tight;
      Int_t n_jet;
      Int_t n_pflowjet;
      Int_t n_jet_preor;
      Int_t n_tau_preor;
      Int_t n_mu_preor;
      Int_t n_el_preor;
      Int_t n_ph_preor;
      Int_t n_bjet; // DEPRECATED ?
      Int_t n_el;
      Int_t n_el_baseline;
      Int_t n_tau;
      Int_t n_tau_baseline;
      Int_t n_mu;
      Int_t n_allmu_bad;
      Int_t n_mu_baseline;
      Int_t n_mu_baseline_bad;
      Int_t pdf_id1;
      Int_t pdf_id2;
      Int_t bb_decision;
      Int_t flag_bib;
      Int_t flag_bib_raw;
      Int_t flag_sct;
      Int_t flag_core;

      // composite observables (involve >1 objects)
      Float_t shatR;
      Float_t gaminvR;
      Float_t gaminvRp1;
      Float_t dphi_BETA_R;
      Float_t dphi_J1_J2_R;
      Float_t gamma_Rp1;
      Float_t costhetaR;
      Float_t dphi_R_Rp1;
      Float_t mdeltaR;
      Float_t cosptR;
      Float_t costhetaRp1;
      Float_t munu_mT;
      Float_t enu_mT;
      Float_t jj_m;
      Float_t mumu_m;
      Float_t mumu_pt;
      Float_t mumu_eta;
      Float_t mumu_phi;
      Float_t ee_m;
      Float_t ee_pt;
      Float_t ee_eta;
      Float_t ee_phi;

      Int_t n_jet_truth;
      //      Float_t truth_jet1_pt;
      //      Float_t truth_jet1_eta;
      //      Float_t truth_jet1_phi;
      //      Float_t truth_jet1_m;
      //      Float_t truth_jet2_pt;
      //      Float_t truth_jet2_eta;
      //      Float_t truth_jet2_phi;
      //      Float_t truth_jet2_m;

      //      Float_t truth_ph1_pt;
      //      Float_t truth_ph1_eta;
      //      Float_t truth_ph1_phi;
      //      Int_t truth_ph1_type;
      //      Int_t truth_ph1_origin;

      Float_t truth_V_bare_pt;
      Float_t truth_V_bare_eta;
      Float_t truth_V_bare_phi;
      Float_t truth_V_bare_m;

      Float_t truth_V_dressed_pt;
      Float_t truth_V_dressed_eta;
      Float_t truth_V_dressed_phi;
      Float_t truth_V_dressed_m;

      Float_t truth_V_simple_pt;
      Float_t truth_V_simple_eta;
      Float_t truth_V_simple_phi;
      Float_t truth_V_simple_m;      

      //      std::vector<Float_t> truth_mu_pt;
      //      std::vector<Float_t> truth_mu_eta;
      //      std::vector<Float_t> truth_mu_phi;
      //      std::vector<Float_t> truth_mu_m;
      //      std::vector<Int_t> truth_mu_origin;
      //      std::vector<Int_t> truth_mu_type;

      //      std::vector<Float_t> truth_el_pt;
      //      std::vector<Float_t> truth_el_eta;
      //      std::vector<Float_t> truth_el_phi;
      //      std::vector<Float_t> truth_el_m;
      //      std::vector<Int_t> truth_el_origin;
      //      std::vector<Int_t> truth_el_type;

      //      std::vector<Float_t> truth_tau_pt;
      //      std::vector<Float_t> truth_tau_eta;
      //      std::vector<Float_t> truth_tau_phi;
      //      std::vector<Float_t> truth_tau_m;
      //      std::vector<Int_t> truth_tau_origin;
      //      std::vector<Int_t> truth_tau_type;

      Int_t n_truthTop;
      //      std::vector <Int_t> truth_W_decay;
      //      Float_t GenFiltMet;
      Float_t allmu_tot_SF;

      std::map<TString, Float_t> weights;

      Int_t isSR;
      Int_t isCR1mubveto;
      Int_t isCR1mubtag;
      Int_t isCR1ebveto;
      Int_t isCR1ebtag;
      //Int_t isCR1e_metnoel;
      Int_t isCR2e;
      Int_t isCR2mu;
      Int_t isCR1ph;
      Float_t syst_weight;

      // Monophoton variables
      Float_t met_tst_sig; 
      Float_t met_tst_sig_old; 
      Bool_t pass_PFlowCVC;
      Float_t met_ph_MT;
      Int_t n_ph_tot;
      Int_t n_ph_signal;
      Int_t n_mu_tot;
      //Int_t n_mu_good;
      Int_t n_el_tot;
      //Int_t n_el_good;
      Int_t n_el_probe;
      Int_t n_jet_tot;
      Int_t n_jet_good;
      Int_t n_tau_tot;
      Int_t n_pv;
      Float_t xSec_AMI;
      Float_t xSec_SUSY;
      Float_t k_factor;
      Float_t filter_eff;

      Float_t evsf_baseline_nominal_EL;
      Float_t evsf_baseline_nominal_MU;
      Float_t evsf_baseline_nominal_PH;
      Float_t evsf_baseline_nominal_TAU;
      Float_t evsf_good_nominal_EL;
      Float_t evsf_good_nominal_MU;
      Float_t evsf_leading_nominal_PH;
      std::map<TString, std::vector<Float_t>> jet_SF_Sys;
      std::map<TString, std::vector<Float_t>> pu_weight_Sys;
      //std::map<TString, std::vector<Float_t>> mconly_weight_Sys;
      std::map<TString, std::vector<Float_t>> el_SF_Sys; //Systematics reorganization
      std::map<TString, std::vector<Float_t>> mu_SF_Sys;
      std::map<TString, std::vector<Float_t>> tau_SF_Sys;
      std::map<TString, std::vector<Float_t>> ph_SF_Sys;
      //std::map<TString, std::vector<Float_t>> baseline_ph_SF_Sys;
      //Float_t evsf_good_syst_tot_EL;
      //Float_t evsf_good_syst_tot_MU;
      //Float_t evsf_leading_syst_tot_PH;
      //std::map<TString, Float_t> evsf_leading_syst_PH;
      //std::map<TString, Float_t> evsf_good_syst_EL;
      //std::map<TString, Float_t> evsf_good_syst_MU;



   public:
      OutputEvent(TString name = "", Bool_t doTrim = kFALSE/*, Bool_t doMonophoton = kFALSE*/);  
      ~OutputEvent();
      void reset();     
      void attachToTree(TTree *tree);

      void setTriggers(const std::vector<TString> &trigs); // define interesting triggers
      void setWeights(const std::vector<TString> &names); // define manually-added event weights (e.g. V+jets reweight)
      void setTriggers(const std::vector<std::string> &trigs); // define interesting triggers
      void setWeights(const std::vector<std::string> &names); // define manually-added event weights (e.g. V+jets reweight)
   };
}

#endif

