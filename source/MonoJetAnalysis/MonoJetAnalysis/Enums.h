#ifndef __MonoJetDC14_Enums_h__
#define __MonoJetDC14_Enums_h__

namespace MonoJetCuts {
   typedef enum {
      processed = 0,
      GRL = 1,
      cleaning = 2,
      trigger = 3,
      vertex = 4,
      MET_cleaning = 5,
      BCH_cleaning = 6,
      Leading_Pt_cut = 7,
      MET_cut = 8,
      Leading_cleaning = 9,
      tau_veto = 10,
      electron_veto = 11,
      muon_veto = 12,
      photon_veto = 13,
      jet_multiplicity = 14,
      jet_MET_overlap = 15,
      MET_cut_2 = 16,
      Leading_Pt_cut_2 = 17,
   } CutID;
}

namespace MonoJetMuonCuts {
   typedef enum {
      exists = 0,
      family,
      quality,
      eta,
      pt,
      MCP,
      isolated,
   } CutID;
}

namespace MonoJetElectronCuts {
   typedef enum {
      exists = 0,
      family,
      isMedium,
      author,
      kinematics,
      OQ,
   } CutID;
}

//FEDE Monophoton
namespace MonoPhotonCuts {
   typedef enum {
      processed = 0,
      GRL = 1,
      cleaning = 2,
      trigger = 3,
      vertex = 4,
      MET_cleaning = 5,
      BCH_cleaning = 6,
      MET_cut = 7,
      loose_photon = 8,
      MET_sig = 9,
      Leading_cleaning = 10,
      leading_MET_overlap = 11,
      Z_vtx = 12,
      tau_veto = 13,
      electron_veto = 14,
      muon_veto = 15,
      jet_veto = 16,
   } CutID;
}

#endif
