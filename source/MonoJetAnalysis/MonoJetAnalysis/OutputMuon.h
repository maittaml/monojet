#ifndef __Analysis_OutputMuon__
#define __Analysis_OutputMuon__

#include <MonoJetAnalysis/OutputObject.h>
#include <xAODMuon/Muon.h>
#include <map>

namespace Analysis {
   class OutputMuon : public OutputObject {
   public:

      std::vector<Float_t> charge;

      std::vector<double> SF;
      std::vector<double> SF_iso;
      std::map<TString, std::vector<double>> mu_SF_Sys;

      std::vector<Float_t> pt;
      std::vector<Float_t> eta;
      std::vector<Float_t> phi;
      std::vector<Float_t> m;
      std::vector<Float_t> id_pt;
      std::vector<Float_t> id_eta;
      std::vector<Float_t> id_phi;
      std::vector<Float_t> id_m;
      std::vector<Float_t> me_pt;
      std::vector<Float_t> me_eta;
      std::vector<Float_t> me_phi;
      std::vector<Float_t> me_m;
      std::vector<Float_t> ptcone20;
      std::vector<Float_t> ptvarcone20;
      std::vector<Float_t> etcone20;
      std::vector<Float_t> topoetcone20;
      std::vector<Float_t> ptcone30;
      std::vector<Float_t> ptvarcone30;
      std::vector<Float_t> ptvarcone30_TightTTVA_pt1000;
      std::vector<Float_t> etcone30;
      std::vector<Float_t> topoetcone30;
      std::vector<Float_t> ptcone40;
      std::vector<Float_t> ptvarcone40;
      std::vector<Float_t> etcone40;
      std::vector<Float_t> topoetcone40;
      std::vector<Float_t> d0;
      std::vector<Float_t> d0sig;
      std::vector<Float_t> z0;
      std::vector<Float_t> z0sig;
      std::vector<Int_t> author;
      std::vector<Int_t> quality;
      std::vector<Int_t> isSA;
      std::vector<Int_t> isBad;
      std::vector<Int_t>  isotool_pass_loosetrackonly;
      std::vector<Float_t> met_nomuon_dphi;
      std::vector<Float_t> met_wmuon_dphi;

      std::vector<Float_t> truth_pt;
      std::vector<Float_t> truth_eta;
      std::vector<Float_t> truth_phi;
      std::vector<Int_t> truth_status;
      std::vector<Int_t> truth_type;
      std::vector<Int_t> truth_origin;

      //for monophton
      std::vector<Float_t> pt_1;
      std::vector<Float_t> eta_1;
      std::vector<Float_t> phi_1;
      std::vector<Float_t> pt_2;
      std::vector<Float_t> eta_2;
      std::vector<Float_t> phi_2;
      /*std::vector<Float_t> pt_3;
      std::vector<Float_t> eta_3;
      std::vector<Float_t> phi_3;*/

   public:
      OutputMuon(TString name = "", Bool_t doTrim = kFALSE);
      ~OutputMuon();
      void reset();
      void attachToTree(TTree *tree);
      void add(const xAOD::Muon &input);
      void add_SFs(const xAOD::Muon &input, std::string Sys, bool iso);
      void add_monophoton(const xAOD::Muon &input);
   };
}

#endif

