#ifndef __Analysis_OutputElectron__
#define __Analysis_OutputElectron__

#include <MonoJetAnalysis/OutputObject.h>
#include <xAODEgamma/Electron.h>
#include <map>

namespace Analysis {
   class OutputElectron : public OutputObject {
   public:

      std::vector<double> SF;
      std::vector<double> SF_iso;
      std::vector<double> SF_trigger;
      std::vector<double> SF_tot;
      std::map<TString, std::vector<double>> el_SF_Sys;

      std::vector<Float_t> charge;
      std::vector<Float_t> pt;
      std::vector<Float_t> eta;
      std::vector<Float_t> phi;
      std::vector<Float_t> m;
      std::vector<Float_t> id_pt;
      std::vector<Float_t> id_eta;
      std::vector<Float_t> id_phi;
      std::vector<Float_t> id_m;
      std::vector<Float_t> cl_pt;
      std::vector<Float_t> cl_eta;
      std::vector<Float_t> cl_etaBE2;
      std::vector<Float_t> cl_phi;
      std::vector<Float_t> cl_m;
      std::vector<Float_t> ptcone20;
      std::vector<Float_t> ptvarcone20;
      std::vector<Float_t> ptvarcone20_TightTTVA_pt1000;
      std::vector<Float_t> etcone20;
      std::vector<Float_t> topoetcone20;
      std::vector<Float_t> ptcone30;
      std::vector<Float_t> ptvarcone30;
      std::vector<Float_t> etcone30;
      std::vector<Float_t> topoetcone30;
      std::vector<Float_t> ptcone40;
      std::vector<Float_t> ptvarcone40;
      std::vector<Float_t> etcone40;
      std::vector<Float_t> topoetcone40;
      std::vector<Float_t> d0;
      std::vector<Float_t> d0sig;
      std::vector<Float_t> z0;
      std::vector<Float_t> z0sig;
      std::vector<Int_t> author;
      std::vector<Int_t> isConv;
      /*
      std::vector<Float_t> demaxs1;
      std::vector<Float_t> fside;
      std::vector<Float_t> weta2;
      std::vector<Float_t> ws3;
      std::vector<Float_t> eratio;
      std::vector<Float_t> reta;
      std::vector<Float_t> rphi;
      std::vector<Float_t> time_cl;
      std::vector<Float_t> time_maxEcell;
      std::vector<Int_t> truth_matched;
      std::vector<Int_t> truth_mothertype;
      std::vector<Int_t> truth_typebkg;
      std::vector<Int_t> truth_originbkg;
      */
      std::vector<Float_t> truth_pt;
      std::vector<Float_t> truth_eta;
      std::vector<Float_t> truth_phi;
      std::vector<Int_t> truth_status;
      std::vector<Int_t> truth_type;
      std::vector<Int_t> truth_origin;
      std::vector<Int_t> isotool_pass_loosetrackonly;
      std::vector<Float_t> met_nomuon_dphi;
      std::vector<Float_t> met_wmuon_dphi;
      std::vector<Float_t> met_noelectron_dphi;
      std::vector<Float_t> met_noelectron_probe_dphi;

      //for monophton
      std::vector<Float_t> pt_1;
      std::vector<Float_t> eta_1;
      std::vector<Float_t> phi_1;
      std::vector<Float_t> pt_2;
      std::vector<Float_t> eta_2;
      std::vector<Float_t> phi_2;
      /*std::vector<Float_t> pt_3;
      std::vector<Float_t> eta_3;
      std::vector<Float_t> phi_3;*/
      std::vector<Float_t> _1_met_nomuon_dphi;
      std::vector<Float_t> _2_met_nomuon_dphi;
      //std::vector<Float_t> _3_met_nomuon_dphi;
      std::vector<Float_t> _1_met_noelectron_dphi;
      std::vector<Float_t> _2_met_noelectron_dphi;
      //std::vector<Float_t> _3_met_noelectron_dphi;
      std::vector<Float_t> _1_met_wmuon_dphi;
      std::vector<Float_t> _2_met_wmuon_dphi;
      //std::vector<Float_t> _3_met_wmuon_dphi;

   public:
      OutputElectron(TString name = "", Bool_t doTrim = kFALSE);
      ~OutputElectron();
      void reset();
      void attachToTree(TTree *tree);
      void add(const xAOD::Electron &input);
      void add_SFs(const xAOD::Electron &input, std::string Sys, bool trigger, bool iso, bool tot);
      void add_monophoton(const xAOD::Electron &input);
   };
}

#endif

