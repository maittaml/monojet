// Local include(s):
#include "MonoJetAnalysis/OutputTau.h"
#include "xAODTruth/xAODTruthHelpers.h"

// ROOT include(s):
#include <TTree.h>

Analysis::OutputTau::OutputTau(TString name, Bool_t doTrim/*, Bool_t doMonophoton*/) : Analysis::OutputObject::OutputObject(name, doTrim/*, doMonophoton*/)
{
   reset();
}

void Analysis::OutputTau::reset()
{
   SF.clear();
   //   SF_iso.clear();
   for (auto &kv : tau_SF_Sys) {
     kv.second.clear();
   }

   pt.clear();
   eta.clear();
   phi.clear();
   m.clear();

   idtool_pass_veryloose.clear();
   idtool_pass_loose.clear();
   idtool_pass_medium.clear();
   idtool_pass_tight.clear();

   veryloose_multiplicity = 0;
   loose_multiplicity = 0;
   medium_multiplicity = 0;
   tight_multiplicity = 0;

   //   truth_pt.clear();
   //   truth_eta.clear();
   //   truth_phi.clear();
   //   truth_status.clear();
   //   truth_type.clear();
   //   truth_origin.clear();

}

void Analysis::OutputTau::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {

		if(!doMonophoton()){

		  /*
		  if (!prefix.Contains("baseline")){
		    tree->Branch(prefix + "eta", &eta);
		    tree->Branch(prefix + "phi", &phi);
		    tree->Branch(prefix + "pt", &pt);
		  }
		  */
			if (!doTrim()) {

				tree->Branch(prefix + "pt", &pt);
				tree->Branch(prefix + "SF", &SF);
				tree->Branch(prefix + "idtool_pass_veryloose", &idtool_pass_veryloose);
				tree->Branch(prefix + "idtool_pass_loose", &idtool_pass_loose);
				tree->Branch(prefix + "idtool_pass_medium", &idtool_pass_medium);
				tree->Branch(prefix + "idtool_pass_tight", &idtool_pass_tight);


				if (prefix.Contains("baseline")){
					for (auto &kv : tau_SF_Sys) tree->Branch(prefix + "SF_syst_" + kv.first.Data(), &kv.second);
				}

				if (!prefix.Contains("baseline")){
					tree->Branch(prefix + "eta", &eta);
					tree->Branch(prefix + "phi", &phi);

					//       tree->Branch(prefix + "SF_iso", &SF_iso);

					tree->Branch(prefix + "veryloose_multiplicity", &veryloose_multiplicity);
					tree->Branch(prefix + "loose_multiplicity", &loose_multiplicity);
					tree->Branch(prefix + "medium_multiplicity", &medium_multiplicity);
					tree->Branch(prefix + "tight_multiplicity", &tight_multiplicity);


					//    tree->Branch(prefix + "truth_pt", &truth_pt);
					//    tree->Branch(prefix + "truth_eta", &truth_eta);
					//    tree->Branch(prefix + "truth_phi", &truth_phi);
					//    tree->Branch(prefix + "truth_status", &truth_status);
					//    tree->Branch(prefix + "truth_type", &truth_type);
					//    tree->Branch(prefix + "truth_origin", &truth_origin);
				}

			}
		}

		else if(doMonophoton()){


			if (!doTrim()) {

		   		tree->Branch(prefix + "pt", &pt);
				tree->Branch(prefix + "eta", &eta);
				tree->Branch(prefix + "phi", &phi);		    
				tree->Branch(prefix + "idtool_pass_veryloose", &idtool_pass_veryloose);
				tree->Branch(prefix + "idtool_pass_loose", &idtool_pass_loose);
				tree->Branch(prefix + "idtool_pass_medium", &idtool_pass_medium);
				tree->Branch(prefix + "idtool_pass_tight", &idtool_pass_tight);
				tree->Branch(prefix + "veryloose_multiplicity", &veryloose_multiplicity);
				tree->Branch(prefix + "loose_multiplicity", &loose_multiplicity);
				tree->Branch(prefix + "medium_multiplicity", &medium_multiplicity);
				tree->Branch(prefix + "tight_multiplicity", &tight_multiplicity);
			}
		}
	}
}

void Analysis::OutputTau::add(const xAOD::TauJet &input)
{
   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());

   //   const xAOD::TruthParticle *truth_particle = xAOD::TruthHelpers::getTruthParticle(input);
   //   if (truth_particle) {
   //     truth_pt.push_back(truth_particle->pt());
   //     truth_eta.push_back(truth_particle->eta());
   //     truth_phi.push_back(truth_particle->phi());
   //     truth_status.push_back(truth_particle->status());
   //     truth_type.push_back(xAOD::TruthHelpers::getParticleTruthType(input));
   //     truth_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(input));
   //   }

   static SG::AuxElement::Accessor<int> acc_is_veryloose("is_veryloose");
   static SG::AuxElement::Accessor<int> acc_is_loose("is_loose");
   static SG::AuxElement::Accessor<int> acc_is_medium("is_medium");
   static SG::AuxElement::Accessor<int> acc_is_tight("is_tight");
   idtool_pass_veryloose.push_back(acc_is_veryloose(input));
   idtool_pass_loose.push_back(acc_is_loose(input));
   idtool_pass_medium.push_back(acc_is_medium(input));
   idtool_pass_tight.push_back(acc_is_tight(input));

   veryloose_multiplicity++;

   if (acc_is_loose(input))
      loose_multiplicity++;

   if (acc_is_medium(input))
      medium_multiplicity++;

   if (acc_is_tight(input))
      tight_multiplicity++;

   /*
   // MCTruthClassifier
   Int_t tp_type(-9999), tp_origin(-9999);

   static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
   static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

   try {
     tp_type = acc_truthType(input);
     tp_origin = acc_truthOrigin(input);
   } catch (SG::ExcBadAuxVar) {
     tp_type = -9999;
     tp_origin = -9999;
   }

   truth_type.push_back(tp_type);
   truth_origin.push_back(tp_origin);
   */
}


void Analysis::OutputTau::add_SFs(const xAOD::TauJet &input, std::string Sys)
{
  static SG::AuxElement::ConstAccessor<double> acc_lep_SF("effscalefact");
  double tmp_lep_SF(-9999);
  try { tmp_lep_SF = acc_lep_SF(input); } catch (SG::ExcBadAuxVar) { tmp_lep_SF = 1.0; }
  if (Sys=="Nom") SF.push_back(tmp_lep_SF);
  for (auto &kv : tau_SF_Sys) {
    if (Sys==kv.first.Data()) kv.second.push_back(tmp_lep_SF);
  }
  return;
}
