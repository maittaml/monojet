#include "MonoJetAnalysis/OutputPhoton.h"

#include <TTree.h>
#include <xAODEgamma/EgammaxAODHelpers.h>
#include "xAODTruth/xAODTruthHelpers.h"
#include "IsolationCorrections/IsolationCorrectionTool.h"
#include "PhotonVertexSelection/IPhotonPointingTool.h"



Analysis::OutputPhoton::OutputPhoton(TString name, Bool_t doTrim/*, Bool_t doMonophoton*/) : Analysis::OutputObject::OutputObject(name, doTrim/*, doMonophoton*/)
{
   reset();
}

Analysis::OutputPhoton::~OutputPhoton()
{
}

void Analysis::OutputPhoton::reset()
{

   SF.clear();
   SF_iso.clear();
   for (auto &kv : ph_SF_Sys) {
     kv.second.clear();
   }

   pt.clear();
   eta.clear();
   phi.clear();
   m.clear();
   ptcone20.clear();
   ptvarcone20.clear();
   etcone20.clear();
   topoetcone20.clear();
   ptcone30.clear();
   ptvarcone30.clear();
   etcone30.clear();
   topoetcone30.clear();
   ptcone40.clear();
   ptvarcone40.clear();
   etcone40.clear();
   topoetcone40.clear();
   OQ.clear();
   /*
   Rphi.clear();
   weta2.clear();
   fracs1.clear();
   weta1.clear();
   emaxs1.clear();
   f1.clear();
   Reta.clear();
   wtots1.clear();
   Eratio.clear();
   Rhad.clear();
   Rhad1.clear();
   e277.clear();
   deltae.clear();
   */
   author.clear();
   isConv.clear();
   /*
   time_cl.clear();
   time_maxEcell.clear();
   truth_E.clear();
   truth_matched.clear();
   truth_mothertype.clear();
   truth_typebkg.clear();
   truth_originbkg.clear();
   */
   truth_pt.clear();
   truth_eta.clear();
   truth_phi.clear();
   truth_type.clear();
   truth_origin.clear();
   truth_status.clear();
   isTight.clear();
   isEM.clear();
   isotool_pass_fixedcuttight.clear();
   met_nomuon_dphi.clear();
   met_wmuon_dphi.clear();
   met_nophoton_dphi.clear();
   

   //FEDE Monophoton variables
   met_noele_dphi.clear();
   eta2.clear();  		
   time.clear();
   pdgId.clear();
   mother_pdgId.clear();
   ZVtx.clear();
   ZVtx_err.clear();
   pt_2.clear();
   eta_2.clear();
   phi_2.clear();	
   /*pt_3.clear();
   eta_3.clear();
   phi_3.clear();*/

   return;
}

void Analysis::OutputPhoton::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {
		if(!doMonophoton()){
			tree->Branch(prefix + "pt", &pt);

			if (!prefix.Contains("baseline")){
				tree->Branch(prefix + "eta", &eta);
				tree->Branch(prefix + "phi", &phi);
				tree->Branch(prefix + "truth_pt", &truth_pt);
			}

			if (!doTrim()) {

				tree->Branch(prefix + "SF", &SF);

				if (prefix.Contains("baseline")){
					tree->Branch(prefix + "eta", &eta);
					tree->Branch(prefix + "phi", &phi);
					for (auto &kv : ph_SF_Sys) tree->Branch(prefix + "SF_syst_" + kv.first, &kv.second);
				}
				else {
					tree->Branch(prefix + "SF_iso", &SF_iso);
					tree->Branch(prefix + "isotool_pass_fixedcuttight", &isotool_pass_fixedcuttight);
					tree->Branch(prefix + "m", &m);
					tree->Branch(prefix + "ptcone20", &ptcone20);
					tree->Branch(prefix + "ptvarcone20", &ptvarcone20);
					tree->Branch(prefix + "etcone20", &etcone20);
					tree->Branch(prefix + "topoetcone20", &topoetcone20);
					tree->Branch(prefix + "ptcone30", &ptcone30);
					tree->Branch(prefix + "ptvarcone30", &ptvarcone30);
					tree->Branch(prefix + "etcone30", &etcone30);
					tree->Branch(prefix + "topoetcone30", &topoetcone30);
					tree->Branch(prefix + "ptcone40", &ptcone40);
					tree->Branch(prefix + "ptvarcone40", &ptvarcone40);
					tree->Branch(prefix + "etcone40", &etcone40);
					tree->Branch(prefix + "topoetcone40", &topoetcone40);
					tree->Branch(prefix + "isTight", &isTight);
					tree->Branch(prefix + "isEM", &isEM);
					tree->Branch(prefix + "OQ", &OQ);
					/*
					tree->Branch(prefix + "Rphi", &Rphi);
					tree->Branch(prefix + "weta2", &weta2);
					tree->Branch(prefix + "fracs1", &fracs1);
					tree->Branch(prefix + "weta1", &weta1);
					tree->Branch(prefix + "emaxs1", &emaxs1);
					tree->Branch(prefix + "f1", &f1);
					tree->Branch(prefix + "Reta", &Reta);
					tree->Branch(prefix + "wtots1", &wtots1);
					tree->Branch(prefix + "Eratio", &Eratio);
					tree->Branch(prefix + "Rhad", &Rhad);
					tree->Branch(prefix + "Rhad1", &Rhad1);
					tree->Branch(prefix + "e277", &e277);
					tree->Branch(prefix + "deltae", &DeltaE);
					*/
					tree->Branch(prefix + "author", &author);
					tree->Branch(prefix + "isConv", &isConv);
					/*
					tree->Branch(prefix + "time_cl", &time_cl);
					tree->Branch(prefix + "time_maxEcell", &time_maxEcell);
					tree->Branch(prefix + "truth_E", &truth_E);
					tree->Branch(prefix + "truth_matched", &truth_matched);
					tree->Branch(prefix + "truth_mothertype", &truth_mothertype);
					tree->Branch(prefix + "truth_typebkg", &truth_typebkg);
					tree->Branch(prefix + "truth_originbkg", &truth_originbkg);
					*/
					tree->Branch(prefix + "truth_eta", &truth_eta);
					tree->Branch(prefix + "truth_phi", &truth_phi);
					tree->Branch(prefix + "truth_status", &truth_status);
					tree->Branch(prefix + "truth_type", &truth_type);
					tree->Branch(prefix + "truth_origin", &truth_origin);

					tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
					tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
					tree->Branch(prefix + "met_nophoton_dphi", &met_nophoton_dphi);

				}
			}
		}

         //Monophoton branches
      if(doMonophoton()){ 		
		   tree->Branch(prefix + "pt", &pt);
		   if(!doTrim()){tree->Branch(prefix + "eta", &eta);}
		   if(!doTrim()){tree->Branch(prefix + "phi", &phi);}	
	      tree->Branch(prefix + "eta2", &eta2); 

	      tree->Branch(prefix + "ptcone20", &ptcone20);
	      tree->Branch(prefix + "topoetcone40", &topoetcone40);
	      tree->Branch(prefix + "isTight", &isTight);
	      if(!doTrim()){tree->Branch(prefix + "isEM", &isEM);}

	      tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
	      tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
	      tree->Branch(prefix + "met_noele_dphi", &met_noele_dphi);
	      tree->Branch(prefix + "ZVtx", &ZVtx);

			if(!doTrim()){

	      	tree->Branch(prefix + "ZVtx_err", &ZVtx_err);
		      tree->Branch(prefix + "OQ", &OQ);

		      tree->Branch(prefix + "author", &author);
		      tree->Branch(prefix + "isConv", &isConv);
		 
		      tree->Branch(prefix + "truth_pt", &truth_pt);
		      tree->Branch(prefix + "truth_eta", &truth_eta);
		      tree->Branch(prefix + "truth_phi", &truth_phi);
		      tree->Branch(prefix + "truth_status", &truth_status);
		      tree->Branch(prefix + "truth_type", &truth_type);
		      tree->Branch(prefix + "truth_origin", &truth_origin);

		      tree->Branch(prefix + "time", &time);
		      tree->Branch(prefix + "pdgId", &pdgId); 
		      tree->Branch(prefix + "mother_pdgId", &mother_pdgId);
			}
  
      }
   }
   

   return;
}


void Analysis::OutputPhoton::add(const xAOD::Photon &input)
{

   author.push_back(input.author()); 
   if(!doMonophoton()){isConv.push_back(xAOD::EgammaHelpers::isConvertedPhoton(&input));}	
   else if(doMonophoton()){isConv.push_back(xAOD::EgammaHelpers::conversionType(&input));}  
   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());

   /*
   Rphi.push_back(input.showerShapeValue(xAOD::EgammaParameters::Rphi));
   weta2.push_back(input.showerShapeValue(xAOD::EgammaParameters::weta2));
   fracs1.push_back(input.showerShapeValue(xAOD::EgammaParameters::fracs1));
   weta1.push_back(input.showerShapeValue(xAOD::EgammaParameters::weta1));
   emaxs1.push_back(input.showerShapeValue(xAOD::EgammaParameters::emaxs1));
   f1.push_back(input.showerShapeValue(xAOD::EgammaParameters::f1));
   Reta.push_back(input.showerShapeValue(xAOD::EgammaParameters::Reta));
   wtots1.push_back(input.showerShapeValue(xAOD::EgammaParameters::wtots1));
   Eratio.push_back(input.showerShapeValue(xAOD::EgammaParameters::Eratio));
   Rhad.push_back(input.showerShapeValue(xAOD::EgammaParameters::Rhad));
   Rhad1.push_back(input.showerShapeValue(xAOD::EgammaParameters::Rhad1));
   e277.push_back(input.showerShapeValue(xAOD::EgammaParameters::e277));

   deltae.push_back(input.showerShapeValue(xAOD::EgammaParameters::DeltaE));
   */

	/*static SG::AuxElement::ConstAccessor< unsigned int > acc_ph_isEM(  "DFCommonPhotonsIsEMTightIsEMValue" );
	isEM.push_back(acc_ph_isEM(input));
	static SG::AuxElement::ConstAccessor< char > acc_ph_isTight(  "DFCommonPhotonsIsEMTight" );
	isTight.push_back(acc_ph_isTight(input));*/

   static SG::AuxElement::ConstAccessor< uint32_t > acc_OQ("OQ");
   //   OQ.push_back(input.auxdata< uint32_t >("OQ"));
   OQ.push_back(acc_OQ(input));
   eta2.push_back(input.caloCluster()->etaBE(2));
   
   /*
   time_cl.push_back(-9999); // TODO: implement or remove
   time_maxEcell.push_back(-9999);  // TODO: implement or remove
   */

   // isolation variables
   Float_t tmp_ptcone20(-9999);
   Float_t tmp_ptvarcone20(-9999);
   Float_t tmp_etcone20(-9999);
   Float_t tmp_topoetcone20(-9999);
   Float_t tmp_ptcone30(-9999);
   Float_t tmp_ptvarcone30(-9999);
   Float_t tmp_etcone30(-9999);
   Float_t tmp_topoetcone30(-9999);
   Float_t tmp_ptcone40(-9999);
   Float_t tmp_ptvarcone40(-9999);
   Float_t tmp_etcone40(-9999);
   Float_t tmp_topoetcone40(-9999);
   input.isolationValue(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
   input.isolationValue(tmp_ptvarcone20, xAOD::Iso::IsolationType::ptvarcone20);
   input.isolationValue(tmp_etcone20, xAOD::Iso::IsolationType::etcone20);
   input.isolationValue(tmp_topoetcone20, xAOD::Iso::IsolationType::topoetcone20);
   input.isolationValue(tmp_ptcone30, xAOD::Iso::IsolationType::ptcone30);
   input.isolationValue(tmp_ptvarcone30, xAOD::Iso::IsolationType::ptvarcone30);
   input.isolationValue(tmp_etcone30, xAOD::Iso::IsolationType::etcone30);
   input.isolationValue(tmp_topoetcone30, xAOD::Iso::IsolationType::topoetcone30);
   input.isolationValue(tmp_ptcone40, xAOD::Iso::IsolationType::ptcone40);
   input.isolationValue(tmp_ptvarcone40, xAOD::Iso::IsolationType::ptvarcone40);
   input.isolationValue(tmp_etcone40, xAOD::Iso::IsolationType::etcone40);
   input.isolationValue(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
   ptcone20.push_back(tmp_ptcone20);
   ptvarcone20.push_back(tmp_ptvarcone20);
   etcone20.push_back(tmp_etcone20);
   topoetcone20.push_back(tmp_topoetcone20);
   ptcone30.push_back(tmp_ptcone30);
   ptvarcone30.push_back(tmp_ptvarcone30);
   etcone30.push_back(tmp_etcone30);
   topoetcone30.push_back(tmp_topoetcone30);
   ptcone40.push_back(tmp_ptcone40);
   ptvarcone40.push_back(tmp_ptvarcone40);
   etcone40.push_back(tmp_etcone40);
   topoetcone40.push_back(tmp_topoetcone40);
   

   // decorations we define in our analysis code
   static SG::AuxElement::ConstAccessor<char> acc_pass_fixedcuttight("pass_fixedcuttight");
   try {
      isotool_pass_fixedcuttight.push_back(acc_pass_fixedcuttight(input));
   } catch (SG::ExcBadAuxVar) {
      isotool_pass_fixedcuttight.push_back(-9999);
   }

   static SG::AuxElement::ConstAccessor<char> acc_ph_isTight("ph_isTight");
   //static SG::AuxElement::ConstAccessor< char > acc_isTight(  "DFCommonPhotonsIsEMTight" );
   try {
      isTight.push_back(acc_ph_isTight(input));
   } catch (SG::ExcBadAuxVar) {
      isTight.push_back(-9999);
   }

   static SG::AuxElement::ConstAccessor<unsigned int> acc_ph_isEM("ph_isEM");
   //static SG::AuxElement::ConstAccessor< unsigned int > acc_isEM(  "DFCommonPhotonsIsEMTightIsEMValue" );
   try {
      isEM.push_back(acc_ph_isEM(input));
   } catch (SG::ExcBadAuxVar) {
      isEM.push_back(-9999);
		
   }




   // dphi's
   
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nophoton_dphi("new_met_nophoton_dphi");
   
   try {
      met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
      if(!doMonophoton()) met_nophoton_dphi.push_back(acc_new_met_nophoton_dphi(input));
      met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
     
   } catch (SG::ExcBadAuxVar) {
      met_nomuon_dphi.push_back(-9999); 
      met_wmuon_dphi.push_back(-9999);
      if(!doMonophoton()) met_nophoton_dphi.push_back(-9999);
   }
   
   

   const xAOD::TruthParticle *truth_particle = xAOD::TruthHelpers::getTruthParticle(input);
   if (truth_particle) {
      truth_pt.push_back(truth_particle->pt());
      truth_eta.push_back(truth_particle->eta());
      truth_phi.push_back(truth_particle->phi());
      truth_status.push_back(truth_particle->status());
      //     truth_type.push_back(xAOD::TruthHelpers::getParticleTruthType(input));
      //     truth_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(input));
   }

   // MCTruthClassifier
   Int_t tp_type(-9999), tp_origin(-9999);

   static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
   static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

   try {
      tp_type = acc_truthType(input);
      tp_origin = acc_truthOrigin(input);
   } catch (SG::ExcBadAuxVar) {
      tp_type = -9999;
      tp_origin = -9999;
   }

   truth_type.push_back(tp_type);
   truth_origin.push_back(tp_origin);

  

   return;
}


void Analysis::OutputPhoton::add_SFs(const xAOD::Photon &input, std::string Sys, bool iso)
{
  static SG::AuxElement::ConstAccessor<double> acc_ph_SF("effscalefact");
  double tmp_ph_SF(-9999);
  try { tmp_ph_SF = acc_ph_SF(input); } catch (SG::ExcBadAuxVar) { tmp_ph_SF = 1.0; }
  if (Sys=="Nom"){
    if (iso) SF_iso.push_back(tmp_ph_SF);
    else SF.push_back(tmp_ph_SF);
  }
  for (auto &kv : ph_SF_Sys) {
    if (Sys==kv.first) kv.second.push_back(tmp_ph_SF);
  }
  return;
}


void Analysis::OutputPhoton::add_monophoton(const xAOD::Photon &input){

	static SG::AuxElement::ConstAccessor<unsigned int> acc_isLeading("isLeading");
	UInt_t ph_isLeading = acc_isLeading(input);

	if ((!doTrim()&&ph_isLeading < 4) || (doTrim()&&ph_isLeading==1)){
		pt.push_back(input.pt());
		eta.push_back(input.eta());
		eta2.push_back(input.caloCluster()->etaBE(2));
		phi.push_back(input.phi());

		if (ph_isLeading < 3){
			// isolation variables
			Float_t tmp_ptcone20(-9999);
			Float_t tmp_topoetcone40(-9999);
			input.isolationValue(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
			input.isolationValue(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
			ptcone20.push_back(tmp_ptcone20);
			topoetcone40.push_back(tmp_topoetcone40);
		
			static SG::AuxElement::ConstAccessor<char> acc_ph_isTight("ph_isTight");
			//static SG::AuxElement::ConstAccessor< char > acc_isTight(  "DFCommonPhotonsIsEMTight" );
			try {
				isTight.push_back(acc_ph_isTight(input));
			} catch (SG::ExcBadAuxVar) {
				isTight.push_back(-9999);
			}

			static SG::AuxElement::ConstAccessor<unsigned int> acc_ph_isEM("ph_isEM");
			//static SG::AuxElement::ConstAccessor< unsigned int > acc_isEM(  "DFCommonPhotonsIsEMTightIsEMValue" );
			try {
				isEM.push_back(acc_ph_isEM(input));
			} catch (SG::ExcBadAuxVar) {
				isEM.push_back(-9999);
		
			}

			// dphi's	
			static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
			static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
			static SG::AuxElement::ConstAccessor<float> acc_new_met_noelectron_dphi("new_met_noelectron_dphi"); 

		
			try {
				met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
				met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input)); 
			   met_noele_dphi.push_back(acc_new_met_noelectron_dphi(input));  
			  
			} catch (SG::ExcBadAuxVar) {
				met_nomuon_dphi.push_back(-9999); 
				met_wmuon_dphi.push_back(-9999);
			   met_noele_dphi.push_back(-9999); 
				
			}
		
			static SG::AuxElement::ConstAccessor<float> acc_ph_zvx("ph_zvx");  
			static SG::AuxElement::ConstAccessor<float> acc_ph_zvx_err("ph_zvx_err");
			Float_t tmp_ph_zvx(-9999);
			Float_t tmp_ph_zvx_err(-9999);	   
			try {
			   tmp_ph_zvx = acc_ph_zvx(input);
			   tmp_ph_zvx_err = acc_ph_zvx_err(input);
			} catch (SG::ExcBadAuxVar) {
			   tmp_ph_zvx = -9999;
			   tmp_ph_zvx_err = -9999;
			}
			ZVtx.push_back(tmp_ph_zvx);
			ZVtx_err.push_back(tmp_ph_zvx_err);


			if (!doTrim()){
				author.push_back(input.author()); 
				isConv.push_back(xAOD::EgammaHelpers::conversionType(&input));

				static SG::AuxElement::ConstAccessor< uint32_t > acc_OQ("OQ");
				OQ.push_back(acc_OQ(input));
		
				const xAOD::TruthParticle *truth_particle = xAOD::TruthHelpers::getTruthParticle(input);
				if (truth_particle) {
					truth_pt.push_back(truth_particle->pt());
					truth_eta.push_back(truth_particle->eta());
					truth_phi.push_back(truth_particle->phi());
					truth_status.push_back(truth_particle->status());
				}

				// MCTruthClassifier
				Int_t tp_type(-9999), tp_origin(-9999);

				static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
				static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

				try {
					tp_type = acc_truthType(input);
					tp_origin = acc_truthOrigin(input);
				} catch (SG::ExcBadAuxVar) {
					tp_type = -9999;
					tp_origin = -9999;
				}

				truth_type.push_back(tp_type);
				truth_origin.push_back(tp_origin);


				time.push_back(input.caloCluster()->time());
				if(truth_particle){
					pdgId.push_back(truth_particle->pdgId());
					mother_pdgId.push_back(truth_particle->parent()->pdgId());
				}
			}
		}


	}
	  
   return;
}

