#include "MonoJetAnalysis/OutputEvent.h"

#include <TTree.h>
#include <TError.h>



Analysis::OutputEvent::OutputEvent(TString name, Bool_t doTrim/*, Bool_t doMonophoton*/) : Analysis::OutputObject::OutputObject(name, doTrim/*, doMonophoton*/)
{
   reset();
}

Analysis::OutputEvent::~OutputEvent()
{
}

void Analysis::OutputEvent::reset()
{
   run = -9999;
   event = 0;
   lbn = -9999;
   bcid = -9999;
   last = -9999;
   year = -9999;
   averageIntPerXing = -9999;
   actualIntPerXing = -9999;
   corAverageIntPerXing = -9999;
   corActualIntPerXing = -9999;
   pu_weight = 1.0;
   pu_hash = -9999;
   mconly_weight = 1.0;
   mconly_weights.clear();
//    btag_weight = 1.0;
   jvt_weight = 1.0;
   jvt_all_weight = 1.0;
   pdf_x1 = -9999;
   pdf_x2 = -9999;
   pdf_pdf1 = -9999;
   pdf_pdf2 = -9999;
   pdf_scale = -9999;
   allmu_tot_SF = 1.0;
   for (auto &kv : trigger) {
      kv.second = -9999;
   }

   trigger_matched_electron = 0;
   trigger_matched_muon = 0;

   trigger_matched_HLT_e60_lhmedium = 0;
   trigger_matched_HLT_e120_lhloose = 0;
   trigger_matched_HLT_e24_lhmedium_L1EM18VH = 0;
   trigger_matched_HLT_e24_lhmedium_L1EM20VH = 0;
   trigger_pass = -9999;
   //   trigger_pass = false;
   hfor = -9999;
   n_vx = -9999;
   n_ph = -9999;
   n_ph_tight = -9999;
   n_ph_baseline = -9999;
   n_ph_baseline_tight = -9999;
   n_jet = -9999;
   n_jet_preor = -9999;
   n_tau_preor = -9999;
   n_mu_preor = -9999;
   n_el_preor = -9999;
   n_ph_preor = -9999;
   n_bjet = -9999;
   n_el = -9999;
   n_el_baseline = -9999;
   n_tau = -9999;
   n_tau_baseline = -9999;
   n_mu = -9999;
   n_allmu_bad = -9999;
   n_mu_baseline = -9999;
   n_mu_baseline_bad = -9999;
   pdf_id1 = -9999;
   pdf_id2 = -9999;
   bb_decision = -9999;
   flag_bib = 0;
   flag_bib_raw = 0;
   flag_sct = 0;
   flag_core = 0;

   /*
   shatR = -9999;
   gaminvR = -9999;
   gaminvRp1 = -9999;
   dphi_BETA_R = -9999;
   dphi_J1_J2_R = -9999;
   gamma_Rp1 = -9999;
   costhetaR = -9999;
   dphi_R_Rp1 = -9999;
   mdeltaR = -9999;
   cosptR = -9999;
   costhetaRp1 = -9999;
   */
   munu_mT = -9999;
   enu_mT = -9999;
   //   jj_m = -9999;
   mumu_m = -9999;
   //   mumu_pt = -9999;
   //   mumu_eta = -9999;
   //   mumu_phi = -9999;
   ee_m = -9999;
   //   ee_pt = -9999;
   //   ee_eta = -9999;
   //   ee_phi = -9999;


   n_jet_truth = -9999;
   //   truth_jet1_pt = -9999;
   //   truth_jet1_eta = -9999;
   //   truth_jet1_phi = -9999;
   //   truth_jet1_m = -9999;
   //   truth_jet2_pt = -9999;
   //   truth_jet2_eta = -9999;
   //   truth_jet2_phi = -9999;
   //   truth_jet2_m = -9999;

   //   truth_ph1_pt = -9999;
   //   truth_ph1_eta = -9999;
   //   truth_ph1_phi = -9999;
   //   truth_ph1_type = -9999;
   //   truth_ph1_origin = -9999;

   truth_V_bare_pt = -9999;
   truth_V_bare_eta = -9999;
   truth_V_bare_phi = -9999;
   truth_V_bare_m = -9999;

   truth_V_dressed_pt = -9999;
   truth_V_dressed_eta = -9999;
   truth_V_dressed_phi = -9999;
   truth_V_dressed_m = -9999;

   //   truth_V_simple_pt = -9999;
   //   truth_V_simple_eta = -9999;
   //   truth_V_simple_phi = -9999;
   //   truth_V_simple_m = -9999;  


   //   truth_mu_pt.clear();
   //   truth_mu_eta.clear();
   //   truth_mu_phi.clear();
   //   truth_mu_m.clear();
   //   truth_mu_origin.clear();
   //   truth_mu_type.clear();

   //   truth_el_pt.clear();
   //   truth_el_eta.clear();
   //   truth_el_phi.clear();
   //   truth_el_m.clear();
   //   truth_el_origin.clear();
   //   truth_el_type.clear();

   //   truth_tau_pt.clear();
   //   truth_tau_eta.clear();
   //   truth_tau_phi.clear();
   //   truth_tau_m.clear();
   //   truth_tau_origin.clear();
   //   truth_tau_type.clear();

   //   truth_W_decay.clear();
   n_truthTop = -9999;
   //   GenFiltMet = -9999;

   for (auto &kv : weights) {
      kv.second = -9999;
   }

   met_tst_sig = -9999; 
   

   isSR = -1;
   isCR1mubveto = -1;
   isCR1mubtag = -1;
   //isCR1e_metnoel = -1;
   isCR1ebveto = -1;
   isCR1ebtag = -1;
   isCR2e = -1;
   isCR2mu = -1;
   isCR1ph = -1;
   syst_weight = -9999;


   // MonoPhoton variables
   if(doMonophoton()) {
      met_ph_MT=-1;
      met_tst_sig_old = -1;
      pass_PFlowCVC = -1; //crack veto cleaning
      n_ph_tot = -9999;
      //n_ph_signal = -9999;
      n_el_tot = -9999;
      n_el_probe = -9999;
      //n_el_good = -9999;
      n_mu_tot = -9999;
      //n_mu_good = -9999;
      n_jet_tot = -9999;
      n_jet_good = -9999;
      n_el_tot = -9999;
      n_tau_tot = -9999;
      n_pv = -9999;
      xSec_AMI = -9999;
      xSec_SUSY = -9999;
      k_factor = -9999;
      filter_eff = -9999;
      evsf_good_nominal_EL = -9999;
      evsf_good_nominal_MU = -9999;
      evsf_leading_nominal_PH = -9999;
		evsf_baseline_nominal_EL = -9999;
		evsf_baseline_nominal_MU = -9999;
		evsf_baseline_nominal_PH = -9999;
		evsf_baseline_nominal_TAU = -9999;

      /*for (auto &kv : EL_SF_Sys) {
         kv.second = -9999;
      }

      for (auto &kv : MU_SF_Sys) {
         kv.second = -9999;
      }

      for (auto &kv : PH_SF_Sys) {
         kv.second = -9999;
      }*/

		for (auto &kv : el_SF_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}
		for (auto &kv : mu_SF_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}
		for (auto &kv : ph_SF_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}
		/*for (auto &kv : baseline_ph_SF_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}*/
		for (auto &kv : tau_SF_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}
		for (auto &kv : jet_SF_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}
		for (auto &kv : pu_weight_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}
		/*for (auto &kv : mconly_weight_Sys) {  //Systematics reorganization
		  kv.second.clear();
		}*/
   }
   return;
}

void Analysis::OutputEvent::attachToTree(TTree *tree)
{
   const TString prefix = (name() != "") ? name() + "_" : ""; // no prefix by default


	if(!doMonophoton()){
		tree->Branch(prefix + "run", &run);
		tree->Branch(prefix + "event", &event);
		tree->Branch(prefix + "last", &last);
		tree->Branch(prefix + "year", &year);
		tree->Branch("met_tst_sig", &met_tst_sig);
		tree->Branch(prefix + "isSR", &isSR);
		tree->Branch(prefix + "isCR1mubveto", &isCR1mubveto);
		tree->Branch(prefix + "isCR1mubtag", &isCR1mubtag);
		tree->Branch(prefix + "isCR1ebveto", &isCR1ebveto);
		tree->Branch(prefix + "isCR1ebtag", &isCR1ebtag);
		//tree->Branch(prefix + "isCR1e_metnoel", &isCR1e_metnoel);
		tree->Branch(prefix + "isCR2e", &isCR2e);
		tree->Branch(prefix + "isCR2mu", &isCR2mu);
		tree->Branch(prefix + "isCR1ph", &isCR1ph);
		tree->Branch(prefix + "n_tau_baseline", &n_tau_baseline);
		tree->Branch(prefix + "mconly_weight", &mconly_weight);
		tree->Branch(prefix + "syst_weight", &syst_weight);
		tree->Branch(prefix + "pu_weight", &pu_weight);
		// tree->Branch(prefix + "btag_weight", &btag_weight);
		tree->Branch(prefix + "jvt_weight", &jvt_weight);
		tree->Branch(prefix + "truth_V_bare_pt", &truth_V_bare_pt);
		tree->Branch(prefix + "truth_V_dressed_pt", &truth_V_dressed_pt);
		//   tree->Branch(prefix + "truth_V_simple_pt", &truth_V_simple_pt);
		tree->Branch(prefix + "munu_mT", &munu_mT);
		tree->Branch(prefix + "enu_mT", &enu_mT);
		tree->Branch(prefix + "mumu_m", &mumu_m);
		tree->Branch(prefix + "ee_m", &ee_m);

		if (!doTrim()) {
		   tree->Branch(prefix + "n_jet", &n_jet);
		   tree->Branch(prefix + "n_jet_preor", &n_jet_preor);
		   tree->Branch(prefix + "n_tau_preor", &n_tau_preor);
		   tree->Branch(prefix + "n_mu_preor", &n_mu_preor);
		   tree->Branch(prefix + "n_el_preor", &n_el_preor);
		   tree->Branch(prefix + "n_ph_preor", &n_ph_preor);
		   tree->Branch(prefix + "n_bjet", &n_bjet);
		   tree->Branch(prefix + "n_el", &n_el);
		   tree->Branch(prefix + "n_el_baseline", &n_el_baseline);
		   tree->Branch(prefix + "n_mu_baseline", &n_mu_baseline);
		   tree->Branch(prefix + "n_mu_baseline_bad", &n_mu_baseline_bad);
		   tree->Branch(prefix + "n_allmu_bad", &n_allmu_bad);
		   tree->Branch(prefix + "n_tau", &n_tau);
		   tree->Branch(prefix + "n_mu", &n_mu);
		   // TODO: add back overlap_weight when needed
		   tree->Branch(prefix + "mconly_weights", &mconly_weights);
		   tree->Branch(prefix + "jvt_all_weight", &jvt_all_weight);

		   //mono-b
		   //      tree->Branch(prefix + "truth_W_decay", &truth_W_decay);
		   //      tree->Branch(prefix + "GenFiltMet", &GenFiltMet);
		   tree->Branch(prefix + "n_truthTop", &n_truthTop);
		   tree->Branch(prefix + "averageIntPerXing", &averageIntPerXing);
		   tree->Branch(prefix + "actualIntPerXing", &actualIntPerXing);
		   tree->Branch(prefix + "corAverageIntPerXing", &corAverageIntPerXing);
		   tree->Branch(prefix + "corActualIntPerXing", &corActualIntPerXing);
		   tree->Branch(prefix + "n_vx", &n_vx);
		   tree->Branch(prefix + "pu_hash", &pu_hash);
		   tree->Branch(prefix + "allmu_tot_SF", &allmu_tot_SF);
		   tree->Branch(prefix + "trigger_matched_electron", &trigger_matched_electron);
		   tree->Branch(prefix + "trigger_matched_muon", &trigger_matched_muon);


		   for (auto &kv : trigger) {
		      const TString trigName = kv.first;
		      if (trigName == "HLT_xe70"
		          || trigName == "HLT_e60_lhmedium"
		          || trigName == "HLT_e120_lhloose"
		          || trigName == "HLT_e24_lhmedium_L1EM20VH" // 2015
		          || trigName == "HLT_xe80_tc_lcw_L1XE50"
		          || trigName == "HLT_xe90_mht_L1XE50"
		          || trigName == "HLT_xe100_mht_L1XE50"
		          || trigName == "HLT_xe110_mht_L1XE50"
		          || trigName == "HLT_xe130_mht_L1XE50"
		          || trigName == "HLT_xe120_L1XE50"
		          || trigName == "HLT_xe110_L1XE50"
		          || trigName == "HLT_noalg_L1J400"
		          || trigName == "HLT_e24_lhtight_nod0_ivarloose"
		          || trigName == "HLT_e26_lhtight_nod0_ivarloose"
		          || trigName == "HLT_e60_lhmedium_nod0"
		          || trigName == "HLT_e140_lhloose_nod0"
		          || trigName == "HLT_g140_loose" // 2016

		          //Mono-b trigger for syst
		          || trigName == "HLT_xe70_mht"
		          || trigName == "HLT_xe90_mht_wEFMu_L1XE50"
		          || trigName == "HLT_j80_xe80"
		          || trigName == "HLT_e24_lhmedium_L1EM20VH"
		          || trigName == "HLT_e24_lhmedium_nod0_L1EM20VH"
		          || trigName == "HLT_e24_lhmedium_L1EM20VHI"
		          || trigName == "HLT_e26_lhtight_ivarloose"
		          || trigName == "HLT_e60_lhmedium"
		          || trigName == "HLT_e60_medium"
		          || trigName == "HLT_2e17_lhvloose"
		          || trigName == "HLT_2e17_lhvloose_nod0"
		          || trigName == "HLT_mu20_ivarloose_L1MU15"
		          || trigName == "HLT_mu24_ivarmedium"
		          || trigName == "HLT_mu26_imedium"
		          || trigName == "HLT_mu26_ivarmedium"
		          || trigName == "HLT_mu20_2mu4noL1"
		          || trigName == "HLT_mu22_mu8noL1"
		          || trigName == "HLT_mu40"
		          || trigName == "HLT_mu50"
		          || trigName == "HLT_2mu14"
		         )
		         tree->Branch(prefix + "trigger_" + trigName, &kv.second);
		   }

		   for (auto &kv : weights) {
		      tree->Branch(prefix + "weight_" + kv.first, &kv.second);
		   }

		   tree->Branch(prefix + "trigger_pass", &trigger_pass);
		   tree->Branch(prefix + "trigger_matched_HLT_e60_lhmedium", &trigger_matched_HLT_e60_lhmedium);
		   tree->Branch(prefix + "trigger_matched_HLT_e120_lhloose", &trigger_matched_HLT_e120_lhloose);
		   tree->Branch(prefix + "trigger_matched_HLT_e24_lhmedium_L1EM18VH", &trigger_matched_HLT_e24_lhmedium_L1EM18VH);
		   tree->Branch(prefix + "trigger_matched_HLT_e24_lhmedium_L1EM20VH", &trigger_matched_HLT_e24_lhmedium_L1EM20VH);
		   tree->Branch(prefix + "lbn", &lbn);
		   tree->Branch(prefix + "bcid", &bcid);
		   tree->Branch(prefix + "pdf_x1", &pdf_x1);
		   tree->Branch(prefix + "pdf_x2", &pdf_x2);
		   tree->Branch(prefix + "pdf_pdf1", &pdf_pdf1);
		   tree->Branch(prefix + "pdf_pdf2", &pdf_pdf2);
		   tree->Branch(prefix + "pdf_scale", &pdf_scale);
		   tree->Branch(prefix + "flag_bib", &flag_bib);
		   tree->Branch(prefix + "flag_bib_raw", &flag_bib_raw);
		   tree->Branch(prefix + "flag_sct", &flag_sct);
		   tree->Branch(prefix + "flag_core", &flag_core);
		   for (auto &kv : trigger) {
		      const TString trigName = kv.first;
		      if (!(trigName == "HLT_xe70"
		            || trigName == "HLT_e60_lhmedium"
		            || trigName == "HLT_e120_lhloose"
		            || trigName == "HLT_e24_lhmedium_L1EM20VH" // 2015
		            || trigName == "HLT_xe80_tc_lcw_L1XE50"
		            || trigName == "HLT_xe90_mht_L1XE50"
		            || trigName == "HLT_xe100_mht_L1XE50"
		            || trigName == "HLT_xe110_mht_L1XE50"
		            || trigName == "HLT_xe130_mht_L1XE50"
		            || trigName == "HLT_xe120_L1XE50"
		            || trigName == "HLT_xe110_L1XE50"
		            || trigName == "HLT_noalg_L1J400"
		            || trigName == "HLT_e24_lhtight_nod0_ivarloose"
		            || trigName == "HLT_e26_lhtight_nod0_ivarloose"
		            || trigName == "HLT_e60_lhmedium_nod0"
		            || trigName == "HLT_e140_lhloose_nod0"
		            || trigName == "HLT_g140_loose")) // 2016
		         tree->Branch(prefix + "trigger_" + trigName, &kv.second);
		   }
		   tree->Branch(prefix + "hfor", &hfor);
		   tree->Branch(prefix + "n_ph", &n_ph);
		   tree->Branch(prefix + "n_ph_tight", &n_ph_tight);
		   tree->Branch(prefix + "n_ph_baseline", &n_ph_baseline);
		   tree->Branch(prefix + "n_ph_baseline_tight", &n_ph_baseline_tight);
		   tree->Branch(prefix + "pdf_id1", &pdf_id1);
		   tree->Branch(prefix + "pdf_id2", &pdf_id2);
		   tree->Branch(prefix + "bb_decision", &bb_decision);

		   /*
		   tree->Branch(prefix + "shatR", &shatR);
		   tree->Branch(prefix + "gaminvR", &gaminvR);
		   tree->Branch(prefix + "gaminvRp1", &gaminvRp1);
		   tree->Branch(prefix + "dphi_BETA_R", &dphi_BETA_R);
		   tree->Branch(prefix + "dphi_J1_J2_R", &dphi_J1_J2_R);
		   tree->Branch(prefix + "gamma_Rp1", &gamma_Rp1);
		   tree->Branch(prefix + "costhetaR", &costhetaR);
		   tree->Branch(prefix + "dphi_R_Rp1", &dphi_R_Rp1);
		   tree->Branch(prefix + "mdeltaR", &mdeltaR);
		   tree->Branch(prefix + "cosptR", &cosptR);
		   tree->Branch(prefix + "costhetaRp1", &costhetaRp1);
		   tree->Branch(prefix + "jj_m", &jj_m);
		   tree->Branch(prefix + "mumu_pt", &mumu_pt);
		   tree->Branch(prefix + "mumu_eta", &mumu_eta);
		   tree->Branch(prefix + "mumu_phi", &mumu_phi);
		   tree->Branch(prefix + "ee_pt", &ee_pt);
		   tree->Branch(prefix + "ee_eta", &ee_eta);
		   tree->Branch(prefix + "ee_phi", &ee_phi);
		   */


		   tree->Branch(prefix + "n_jet_truth", &n_jet_truth);
		   //      tree->Branch(prefix + "truth_jet1_pt", &truth_jet1_pt);
		   //      tree->Branch(prefix + "truth_jet1_eta", &truth_jet1_eta);
		   //      tree->Branch(prefix + "truth_jet1_phi", &truth_jet1_phi);
		   //      tree->Branch(prefix + "truth_jet1_m", &truth_jet1_m);
		   //      tree->Branch(prefix + "truth_jet2_pt", &truth_jet2_pt);
		   //      tree->Branch(prefix + "truth_jet2_eta", &truth_jet2_eta);
		   //      tree->Branch(prefix + "truth_jet2_phi", &truth_jet2_phi);
		   //      tree->Branch(prefix + "truth_jet2_m", &truth_jet2_m);

		   //      tree->Branch(prefix + "truth_ph1_pt", &truth_ph1_pt);
		   //      tree->Branch(prefix + "truth_ph1_eta", &truth_ph1_eta);
		   //      tree->Branch(prefix + "truth_ph1_phi", &truth_ph1_phi);
		   //      tree->Branch(prefix + "truth_ph1_type", &truth_ph1_type);
		   //      tree->Branch(prefix + "truth_ph1_origin", &truth_ph1_origin);

		   //      tree->Branch(prefix + "truth_V_bare_pt", &truth_V_bare_pt);
		   tree->Branch(prefix + "truth_V_bare_eta", &truth_V_bare_eta);
		   tree->Branch(prefix + "truth_V_bare_phi", &truth_V_bare_phi);
		   tree->Branch(prefix + "truth_V_bare_m", &truth_V_bare_m);

		   //      tree->Branch(prefix + "truth_V_dressed_pt", &truth_V_dressed_pt);
		   tree->Branch(prefix + "truth_V_dressed_eta", &truth_V_dressed_eta);
		   tree->Branch(prefix + "truth_V_dressed_phi", &truth_V_dressed_phi);
		   tree->Branch(prefix + "truth_V_dressed_m", &truth_V_dressed_m);

		   //tree->Branch(prefix + "truth_V_simple_pt", &truth_V_simple_pt);
		   //      tree->Branch(prefix + "truth_V_simple_eta", &truth_V_simple_eta);
		   //      tree->Branch(prefix + "truth_V_simple_phi", &truth_V_simple_phi);
		   //      tree->Branch(prefix + "truth_V_simple_m", &truth_V_simple_m);      

		   //tree->Branch(prefix + "truth_V_simple_pt", &truth_V_simple_pt);
		   tree->Branch(prefix + "truth_V_simple_eta", &truth_V_simple_eta);
		   tree->Branch(prefix + "truth_V_simple_phi", &truth_V_simple_phi);
		   tree->Branch(prefix + "truth_V_simple_m", &truth_V_simple_m);   

		   //      tree->Branch(prefix + "truth_mu_pt", &truth_mu_pt);
		   //      tree->Branch(prefix + "truth_mu_eta", &truth_mu_eta);
		   //      tree->Branch(prefix + "truth_mu_phi", &truth_mu_phi);
		   //      tree->Branch(prefix + "truth_mu_m", &truth_mu_m);
		   //      tree->Branch(prefix + "truth_mu_origin", &truth_mu_origin);
		   //      tree->Branch(prefix + "truth_mu_type", &truth_mu_type);

		   //      tree->Branch(prefix + "truth_el_pt", &truth_el_pt);
		   //      tree->Branch(prefix + "truth_el_eta", &truth_el_eta);
		   //      tree->Branch(prefix + "truth_el_phi", &truth_el_phi);
		   //      tree->Branch(prefix + "truth_el_m", &truth_el_m);
		   //      tree->Branch(prefix + "truth_el_origin", &truth_el_origin);
		   //      tree->Branch(prefix + "truth_el_type", &truth_el_type);

		   //      tree->Branch(prefix + "truth_tau_pt", &truth_tau_pt);
		   //      tree->Branch(prefix + "truth_tau_eta", &truth_tau_eta);
		   //      tree->Branch(prefix + "truth_tau_phi", &truth_tau_phi);
		   //      tree->Branch(prefix + "truth_tau_m", &truth_tau_m);
		   //      tree->Branch(prefix + "truth_tau_origin", &truth_tau_origin);
		   //      tree->Branch(prefix + "truth_tau_type", &truth_tau_type);

		}
	}

   // MonoPhoton variables
   if(doMonophoton()) {

		tree->Branch(prefix + "run", &run);
		tree->Branch(prefix + "event", &event);
		tree->Branch(prefix + "year", &year);
		//tree->Branch(prefix + "pass_CVC", &pass_PFlowCVC); //crack veto cleaning for PFlow

		//tree->Branch(prefix + "last", &last);
		for (auto &kv : el_SF_Sys) tree->Branch(prefix + "el_SF_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization
		for (auto &kv : mu_SF_Sys) tree->Branch(prefix + "mu_SF_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization
		/*for (auto &kv : tau_SF_Sys) tree->Branch(prefix + "tau_SF_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization*/
		for (auto &kv : ph_SF_Sys) tree->Branch(prefix + "ph_SF_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization
		//tmp for (auto &kv : baseline_ph_SF_Sys) tree->Branch(prefix + "baseline_ph_SF_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization
		for (auto &kv : jet_SF_Sys) tree->Branch(prefix + "jet_SF_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization
		for (auto &kv : pu_weight_Sys) tree->Branch(prefix + "pu_weight_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization
		//for (auto &kv : mconly_weight_Sys) tree->Branch(prefix + "mconly_weight_syst_" + kv.first.Data(), &kv.second); //Systematics reorganization

		if(!isData()){
			tree->Branch(prefix + "mconly_weights", &mconly_weights); //theo syst
		}
		

		if(!isData()){
			tree->Branch(prefix + "mconly_weight", &mconly_weight);
			//tree->Branch(prefix + "mconly_weights", &mconly_weights); //theo syst
			tree->Branch(prefix + "pu_weight", &pu_weight);
			tree->Branch(prefix + "jvt_weight", &jvt_weight);
			tree->Branch(prefix + "xSec_SUSY", &xSec_SUSY);
			tree->Branch(prefix + "k_factor", &k_factor);
			tree->Branch(prefix + "filter_eff", &filter_eff);
			tree->Branch(prefix + "evsf_good_nominal_EL", &evsf_good_nominal_EL);
			tree->Branch(prefix + "evsf_good_nominal_MU", &evsf_good_nominal_MU);
			tree->Branch(prefix + "evsf_leading_nominal_PH", &evsf_leading_nominal_PH);
		}   

	   	for (auto &kv : trigger) {
			const TString trigName = kv.first;
			if(!doTrim()){
				if (trigName == "HLT_g120_loose"
		       || trigName == "HLT_g140_loose"
		       || trigName == "HLT_g160_loose"
		       || trigName == "HLT_xe90_tc_lcw_L1XE50"
		       || trigName == "HLT_xe90_mht" // 2015
		       || trigName == "HLT_xe100_mht_L1XE50"
		       || trigName == "HLT_xe100"
		       || trigName == "HLT_xe90_mht_L1XE50"
		       || trigName == "HLT_xe110_mht_L1XE50"
		       || trigName == "HLT_xe130_mht_L1XE50"
		         ) tree->Branch(prefix + "trigger_" + trigName, &kv.second);
			}
			else if (doTrim()){
				if (trigName == "HLT_g140_loose") tree->Branch(prefix + "trigger_" + trigName, &kv.second);
			}
	    }

	    /*for (auto &kv : weights) {
	      tree->Branch(prefix + "weight_" + kv.first, &kv.second);
	    }*/

	if(!doTrim()) {tree->Branch(prefix + "met_tst_sig_old", &met_tst_sig_old);}

	   tree->Branch(prefix + "met_tst_sig", &met_tst_sig);
	   tree->Branch(prefix + "mumu_m", &mumu_m);
	   tree->Branch(prefix + "ee_m", &ee_m);
	   tree->Branch(prefix + "n_jet", &n_jet);
	   tree->Branch(prefix + "n_jet_good", &n_jet_good);
	   tree->Branch(prefix + "n_el", &n_el);
	   tree->Branch(prefix + "n_el_baseline", &n_el_baseline);
   	   tree->Branch(prefix + "n_mu", &n_mu);
	   tree->Branch(prefix + "n_mu_baseline", &n_mu_baseline);
	   //tree->Branch(prefix + "n_tau", &n_tau);
           tree->Branch(prefix + "n_tau_baseline", &n_tau_baseline);

		if(!doTrim()){
			//tree->Branch(prefix + "jvt_weight", &jvt_weight);
			//tree->Branch(prefix + "mconly_weights", &mconly_weights);
			//tree->Branch(prefix + "jvt_all_weight", &jvt_all_weight);
		   
			tree->Branch(prefix + "n_jet_truth", &n_jet_truth);
			tree->Branch(prefix + "n_ph_baseline_tight", &n_ph_baseline_tight);
			tree->Branch(prefix + "n_ph_tight", &n_ph_tight);
			tree->Branch(prefix + "n_ph_baseline", &n_ph_baseline);
			tree->Branch(prefix + "n_el_probe", &n_el_probe);
			//tree->Branch(prefix + "munu_mT", &munu_mT);
			//tree->Branch(prefix + "enu_mT", &enu_mT);
			tree->Branch(prefix + "met_ph_MT", &met_ph_MT);

		   tree->Branch(prefix + "averageIntPerXing", &averageIntPerXing);
		   tree->Branch(prefix + "actualIntPerXing", &actualIntPerXing);
		   tree->Branch(prefix + "corAverageIntPerXing", &corAverageIntPerXing);
		   tree->Branch(prefix + "corActualIntPerXing", &corActualIntPerXing);
			tree->Branch(prefix + "n_vx", &n_vx);
			tree->Branch(prefix + "n_pv", &n_pv);
			//tree->Branch(prefix + "pu_hash", &pu_hash);
			//tree->Branch(prefix + "allmu_tot_SF", &allmu_tot_SF);
			tree->Branch(prefix + "lbn", &lbn);
			tree->Branch(prefix + "bcid", &bcid);

			tree->Branch(prefix + "pdf_x1", &pdf_x1);
			tree->Branch(prefix + "pdf_x2", &pdf_x2);
			tree->Branch(prefix + "pdf_pdf1", &pdf_pdf1);
			tree->Branch(prefix + "pdf_pdf2", &pdf_pdf2);
			tree->Branch(prefix + "pdf_scale", &pdf_scale);
			tree->Branch(prefix + "flag_bib", &flag_bib);
			tree->Branch(prefix + "flag_bib_raw", &flag_bib_raw);
			tree->Branch(prefix + "flag_sct", &flag_sct);
			tree->Branch(prefix + "flag_core", &flag_core);
			tree->Branch(prefix + "pdf_id1", &pdf_id1);
			tree->Branch(prefix + "pdf_id2", &pdf_id2);
		   
		}

	}


   return;
}

void Analysis::OutputEvent::setTriggers(const std::vector<TString> &trigs)
{
   for (auto &trigName : trigs) {
      trigger[trigName] = 0;
      //Info("OutputEvent", "adding trigger %s", trigName.Data());
   }
   trigger_pass = 0;
   //   trigger_pass = false;
}

void Analysis::OutputEvent::setWeights(const std::vector<TString> &names)
{
   for (auto &weightName : names) {
      weights[weightName] = 0;
      //Info("OutputEvent", "adding weight %s", weightName.Data());
   }
}

void Analysis::OutputEvent::setTriggers(const std::vector<std::string> &trigs)
{
   for (auto &trigName : trigs) {
      trigger[trigName] = 0;
      //Info("OutputEvent", "adding trigger %s", trigName.Data());
   }
   trigger_pass = 0;
   //   trigger_pass = false;
}

void Analysis::OutputEvent::setWeights(const std::vector<std::string> &names)
{
   for (auto &weightName : names) {
      weights[weightName] = 0;
      //Info("OutputEvent", "adding weight %s", weightName.Data());
   }
}
