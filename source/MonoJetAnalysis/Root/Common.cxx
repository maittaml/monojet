#include "CxxUtils/fpcompare.h"
#include "MonoJetAnalysis/Common.h"
#include "xAODBase/IParticle.h"

namespace MonoJetAnalysis {
   Bool_t comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b)
   {
      return CxxUtils::fpcompare::greater(a->pt(), b->pt());
   }
}

