#include "MonoJetAnalysis/OutputJet.h"

#include <TTree.h>
#include <xAODBase/IParticleHelpers.h>
#include "xAODTruth/xAODTruthHelpers.h"

Analysis::OutputJet::OutputJet(TString name, Bool_t doTrim/*, Bool_t doMonophoton*/) : Analysis::OutputObject::OutputObject(name, doTrim/*, doMonophoton*/)
{
   reset();
}

Analysis::OutputJet::~OutputJet()
{
}

void Analysis::OutputJet::reset()
{

   weight.clear(); // TODO: add variations
   pt.clear();
   eta.clear();
   phi.clear();
   m.clear();
   raw_pt.clear();
   raw_eta.clear();
   raw_phi.clear();
   raw_m.clear();
   timing.clear();
   emfrac.clear();
   fmax.clear();
   hecf.clear();
   hecq.clear();
   larq.clear();
   avglarq.clear();
   fch.clear();
   negE.clear();
   lambda.clear();
   lambda2.clear();
   jvtxf.clear();
   //   MV2c00_discriminant.clear();
   MV2c10_discriminant.clear();
   MV2c20_discriminant.clear();
   fmaxi.clear();
   isbjet.clear();
   isbjet_loose.clear();
   jvt.clear();
   cleaning.clear();
   met_nomuon_dphi.clear();
   met_wmuon_dphi.clear();
   met_noelectron_dphi.clear();
   met_nophoton_dphi.clear();

   PartonTruthLabelID.clear();
   ConeTruthLabelID.clear();
   TruthLabelDeltaR_B.clear();
   TruthLabelDeltaR_C.clear();
   TruthLabelDeltaR_T.clear();
   //   truth_pt.clear();
   //   truth_eta.clear();
   //   truth_phi.clear();
   //   truth_status.clear();
   //   truth_type.clear();
   //   truth_origin.clear();

   //for monophoton   FEDE
	ph_dphi.clear();
   pt_all.clear();
   eta_all.clear();
   phi_all.clear();
   isBad_all.clear();
   passOR_all.clear();
   jvt_all.clear();
   
   return;
}

void Analysis::OutputJet::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

	if(!doMonophoton()){

		tree->Branch(prefix + "pt", &pt);
		tree->Branch(prefix + "eta", &eta);
		tree->Branch(prefix + "phi", &phi);
		tree->Branch(prefix + "m", &m);

		if (!doTrim()) {
			tree->Branch(prefix + "fmax", &fmax);
			tree->Branch(prefix + "fch", &fch);


			//Mono-b
			//      tree->Branch(prefix + "MV2c00_discriminant", &MV2c00_discriminant);
			tree->Branch(prefix + "MV2c10_discriminant", &MV2c10_discriminant);
			tree->Branch(prefix + "MV2c20_discriminant", &MV2c20_discriminant);
			tree->Branch(prefix + "isbjet", &isbjet);
			tree->Branch(prefix + "PartonTruthLabelID", &PartonTruthLabelID);
			tree->Branch(prefix + "ConeTruthLabelID", &ConeTruthLabelID);      

			tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
			tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
			tree->Branch(prefix + "met_noelectron_dphi", &met_noelectron_dphi);
			tree->Branch(prefix + "met_nophoton_dphi", &met_nophoton_dphi);
			tree->Branch(prefix + "weight", &weight); // TODO: implement also for syst, when available
			tree->Branch(prefix + "raw_pt", &raw_pt);
			tree->Branch(prefix + "raw_eta", &raw_eta);
			tree->Branch(prefix + "raw_phi", &raw_phi);
			tree->Branch(prefix + "raw_m", &raw_m);
			tree->Branch(prefix + "timing", &timing);
			tree->Branch(prefix + "emfrac", &emfrac);
			tree->Branch(prefix + "hecf", &hecf);
			tree->Branch(prefix + "hecq", &hecq);
			tree->Branch(prefix + "larq", &larq);
			tree->Branch(prefix + "avglarq", &avglarq);
			tree->Branch(prefix + "negE", &negE);
			tree->Branch(prefix + "lambda", &lambda);
			tree->Branch(prefix + "lambda2", &lambda2);
			tree->Branch(prefix + "jvtxf", &jvtxf);

			tree->Branch(prefix + "fmaxi", &fmaxi);
			tree->Branch(prefix + "isbjet_loose", &isbjet_loose);
			tree->Branch(prefix + "jvt", &jvt);

			tree->Branch(prefix + "cleaning", &cleaning);

			tree->Branch(prefix + "TruthLabelDeltaR_B", &TruthLabelDeltaR_B);
			tree->Branch(prefix + "TruthLabelDeltaR_C", &TruthLabelDeltaR_C);
			tree->Branch(prefix + "TruthLabelDeltaR_T", &TruthLabelDeltaR_T);
			//}
			//      tree->Branch(prefix + "truth_pt", &truth_pt);
			//      tree->Branch(prefix + "truth_eta", &truth_eta);
			//      tree->Branch(prefix + "truth_phi", &truth_phi);
			//      tree->Branch(prefix + "truth_status", &truth_status);
			//      tree->Branch(prefix + "truth_type", &truth_type);
			//      tree->Branch(prefix + "truth_origin", &truth_origin);
		}
	}
   if(doMonophoton()){   		
		if(!doTrim()){
			tree->Branch(prefix + "pt", &pt);
			tree->Branch(prefix + "eta", &eta);
			tree->Branch(prefix + "phi", &phi);
			tree->Branch(prefix + "m", &m);
			tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
			tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
			//tree->Branch(prefix + "met_noelectron_dphi", &met_noelectron_dphi);
			tree->Branch(prefix + "ph_dphi", &ph_dphi);
			/*
			tree->Branch(prefix + "raw_pt", &raw_pt);
			tree->Branch(prefix + "raw_eta", &raw_eta);
			tree->Branch(prefix + "raw_phi", &raw_phi);
			tree->Branch(prefix + "raw_m", &raw_m);
			tree->Branch(prefix + "pt_all", &pt_all); //Trim
			tree->Branch(prefix + "eta_all", &eta_all);
			tree->Branch(prefix + "phi_all", &phi_all);
			tree->Branch(prefix + "isBad_all", &isBad_all);
			tree->Branch(prefix + "passOR_all", &passOR_all);
			tree->Branch(prefix + "jvt_all", &jvt_all);*/
			//if(!doMonophoton()){
			/*tree->Branch(prefix + "fmax", &fmax);
			tree->Branch(prefix + "fch", &fch);

   
			tree->Branch(prefix + "weight", &weight); // TODO: implement also for syst, when available
			tree->Branch(prefix + "raw_pt", &raw_pt);
			tree->Branch(prefix + "raw_eta", &raw_eta);
			tree->Branch(prefix + "raw_phi", &raw_phi);
			tree->Branch(prefix + "raw_m", &raw_m);*/
			tree->Branch(prefix + "PartonTruthLabelID", &PartonTruthLabelID);
			tree->Branch(prefix + "timing", &timing);
			tree->Branch(prefix + "emfrac", &emfrac);
			tree->Branch(prefix + "jvt", &jvt);
			/*tree->Branch(prefix + "hecf", &hecf);
			tree->Branch(prefix + "hecq", &hecq);
			tree->Branch(prefix + "larq", &larq);
			tree->Branch(prefix + "avglarq", &avglarq);
			tree->Branch(prefix + "negE", &negE);
			tree->Branch(prefix + "lambda", &lambda);
			tree->Branch(prefix + "lambda2", &lambda2);
			tree->Branch(prefix + "jvtxf", &jvtxf);

			tree->Branch(prefix + "fmaxi", &fmaxi);
			

			tree->Branch(prefix + "cleaning", &cleaning);*/
		}
   
   }

   return;
}

void Analysis::OutputJet::add(const xAOD::Jet &input)
{

   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());
   const xAOD::Jet *thisRawJet = dynamic_cast< const xAOD::Jet* >(xAOD::getOriginalObject(input));
   
   if (!thisRawJet) return;
   raw_pt.push_back(thisRawJet->pt());
   raw_eta.push_back(thisRawJet->eta());
   raw_phi.push_back(thisRawJet->phi());
   raw_m.push_back(thisRawJet->m());
   Float_t tmp_timing(-9999);
   Float_t tmp_emfrac(-9999);
   Float_t tmp_hecf(-9999);
   Float_t tmp_hecq(-9999);
   Float_t tmp_larq(-9999);
   Float_t tmp_avglarq(-9999);
   Float_t tmp_fmax(-9999);
   Int_t tmp_fmaxi(-9999);
   Float_t tmp_negE(-9999);
   input.getAttribute(xAOD::JetAttribute::Timing, tmp_timing);
   input.getAttribute(xAOD::JetAttribute::EMFrac, tmp_emfrac);
   input.getAttribute(xAOD::JetAttribute::HECFrac, tmp_hecf);
   input.getAttribute(xAOD::JetAttribute::HECQuality, tmp_hecq);
   input.getAttribute(xAOD::JetAttribute::LArQuality, tmp_larq);
   input.getAttribute(xAOD::JetAttribute::AverageLArQF, tmp_avglarq);
   input.getAttribute(xAOD::JetAttribute::FracSamplingMax, tmp_fmax);
   input.getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex, tmp_fmaxi);
   input.getAttribute(xAOD::JetAttribute::NegativeE, tmp_negE);
   timing.push_back(tmp_timing);
   emfrac.push_back(tmp_emfrac);
   hecf.push_back(tmp_hecf);
   hecq.push_back(tmp_hecq);
   larq.push_back(tmp_larq);
   avglarq.push_back(tmp_avglarq);
   fmax.push_back(tmp_fmax);
   fmaxi.push_back(tmp_fmaxi);
   negE.push_back(tmp_negE);


   std::vector<Float_t> sumpttrk_vec;
   input.getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumpttrk_vec);
   Float_t tmp_fch = (sumpttrk_vec.size() > 0) ? sumpttrk_vec[0] /   input.pt() : 0;
   fch.push_back(tmp_fch);

   std::vector<Float_t> jvf_vec;
   input.getAttribute(xAOD::JetAttribute::JVF, jvf_vec);
   jvtxf.push_back((jvf_vec.size() > 0) ? jvf_vec[0] : -9999);

   //cleaning
   static SG::AuxElement::Accessor<char> acc_bad("bad");
   Int_t tmp_cleaning = JetCleaningLevel::BAD;
   try {
      if (acc_bad(input) == 0) {
         tmp_cleaning = JetCleaningLevel::LOOSE;
         if (fabs(input.eta()) > 2.4 || tmp_fch / tmp_fmax > 0.1)
            tmp_cleaning = JetCleaningLevel::TIGHT;
      }
   } catch (SG::ExcBadAuxVar) {
      tmp_cleaning = 1234;
   }
   cleaning.push_back(tmp_cleaning);


   // B-tagging
   //   Double_t tmp_MV2c00_discriminant(-9999);
   //   input.btagging()->MVx_discriminant("MV2c00", tmp_MV2c00_discriminant);
   //   MV2c00_discriminant.push_back(tmp_MV2c00_discriminant);
   if(!doMonophoton()){
		Double_t tmp_MV2c10_discriminant(-9999);
		input.btagging()->MVx_discriminant("MV2c10", tmp_MV2c10_discriminant);
		MV2c10_discriminant.push_back(tmp_MV2c10_discriminant);
		Double_t tmp_MV2c20_discriminant(-9999);
		input.btagging()->MVx_discriminant("MV2c20", tmp_MV2c20_discriminant);
		MV2c20_discriminant.push_back(tmp_MV2c20_discriminant);
	}

   // TODO: implement also lambda, lambda2 and clust_* variables
   lambda.push_back(-9999);
   lambda2.push_back(-9999);

   // dphi's
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_noelectron_dphi("new_met_noelectron_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nophoton_dphi("new_met_nophoton_dphi");
   try {
      met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
      met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
      if(!doMonophoton()) met_noelectron_dphi.push_back(acc_new_met_noelectron_dphi(input)); // ?
      if(!doMonophoton()) met_nophoton_dphi.push_back(acc_new_met_nophoton_dphi(input));
   } catch (SG::ExcBadAuxVar) {
      met_nomuon_dphi.push_back(-9999);
      met_wmuon_dphi.push_back(-9999);
      if(!doMonophoton())met_noelectron_dphi.push_back(-9999); //?
      if(!doMonophoton())met_nophoton_dphi.push_back(-9999);
   }

   // truth B-tagging variables, only if available
	if(!doMonophoton()){
		static SG::AuxElement::ConstAccessor<int> acc_PartonTruthLabelID("PartonTruthLabelID");
		static SG::AuxElement::ConstAccessor<int> acc_ConeTruthLabelID("ConeTruthLabelID");
		static SG::AuxElement::ConstAccessor<float> acc_TruthLabelDeltaR_B("TruthLabelDeltaR_B");
		static SG::AuxElement::ConstAccessor<float> acc_TruthLabelDeltaR_C("TruthLabelDeltaR_C");
		static SG::AuxElement::ConstAccessor<float> acc_TruthLabelDeltaR_T("TruthLabelDeltaR_T");
		Int_t tmp_PartonTruthLabelID(-9999);
		Int_t tmp_ConeTruthLabelID(-9999);
		Float_t tmp_TruthLabelDeltaR_B(-9999);
		Float_t tmp_TruthLabelDeltaR_C(-9999);
		Float_t tmp_TruthLabelDeltaR_T(-9999);
		if (acc_PartonTruthLabelID.isAvailable(input)) {
		   tmp_PartonTruthLabelID = acc_PartonTruthLabelID(input);
		   tmp_ConeTruthLabelID = acc_ConeTruthLabelID(input);
		   if (acc_TruthLabelDeltaR_B.isAvailable(input)) { // not always available...
		      tmp_TruthLabelDeltaR_B = acc_TruthLabelDeltaR_B(input);
		      tmp_TruthLabelDeltaR_C = acc_TruthLabelDeltaR_C(input);
		      tmp_TruthLabelDeltaR_T = acc_TruthLabelDeltaR_T(input);
		   }
		}
		PartonTruthLabelID.push_back(tmp_PartonTruthLabelID);
		ConeTruthLabelID.push_back(tmp_ConeTruthLabelID);
		TruthLabelDeltaR_B.push_back(tmp_TruthLabelDeltaR_B);
		TruthLabelDeltaR_C.push_back(tmp_TruthLabelDeltaR_C);
		TruthLabelDeltaR_T.push_back(tmp_TruthLabelDeltaR_T);
	}

   // TODO: add segment variables

   // decorations from SUSYTools
   //static SG::AuxElement::ConstAccessor<int> acc_effscalefact("effscalefact"); // no scale factors for jets...
   static SG::AuxElement::ConstAccessor<char> acc_bjet("bjet");
   static SG::AuxElement::ConstAccessor<char> acc_bjet_loose("bjet_loose");
   static SG::AuxElement::ConstAccessor<float> acc_jvt("Jvt");
   // weight.push_back(acc_effscalefact(input));
   weight.push_back(1); // no scale factors for jets...
   if (acc_bjet.isAvailable(input)) {
      isbjet.push_back(acc_bjet(input));
   } else {
      isbjet.push_back(9999);
   }
   if (acc_bjet_loose.isAvailable(input)) {
      isbjet_loose.push_back(acc_bjet_loose(input));
   } else {
      isbjet_loose.push_back(9999);
   }
   if (acc_jvt.isAvailable(input)) {
      jvt.push_back(acc_jvt(input));
   } else {
      jvt.push_back(-9999);
   }

   /*
   const xAOD::TruthParticle *truth_particle = xAOD::TruthHelpers::getTruthParticle(input);
   if (truth_particle) {
     truth_pt.push_back(truth_particle->pt());
     truth_eta.push_back(truth_particle->eta());
     truth_phi.push_back(truth_particle->phi());
     truth_status.push_back(truth_particle->status());
     truth_type.push_back(xAOD::TruthHelpers::getParticleTruthType(input));
     truth_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(input));
   }
   */
   /*
   // MCTruthClassifier
   Int_t tp_type(-9999), tp_origin(-9999);

   static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
   static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

   try {
     tp_type = acc_truthType(input);
     tp_origin = acc_truthOrigin(input);
   } catch (SG::ExcBadAuxVar) {
     tp_type = -9999;
     tp_origin = -9999;
   }

   truth_type.push_back(tp_type);
   truth_origin.push_back(tp_origin);
   */



   return;
}

void Analysis::OutputJet::add_monophoton(const xAOD::Jet &input)
{
   static SG::AuxElement::ConstAccessor<bool> acc_isGood("isGood");
   static SG::AuxElement::ConstAccessor<bool> acc_isSelected("isSelected");
   static SG::AuxElement::ConstAccessor<unsigned int> acc_isLeading("isLeading");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_jet_ph_dphi("new_jet_ph_dphi");
   static SG::AuxElement::Accessor<char> acc_bad("bad");
   static SG::AuxElement::Accessor<char> acc_passOR("passOR");

   Bool_t jet_isGood = acc_isGood(input);
   UInt_t jet_isLeading = acc_isLeading(input);
   Bool_t jet_isSelected = acc_isSelected(input);

   //static SG::AuxElement::ConstAccessor<float> acc_new_jet_ph_dphi("new_jet_ph_dphi"); //test JVT
             /* JVT test try {
               ph_dphi.push_back(acc_new_jet_ph_dphi(input));
            } catch (SG::ExcBadAuxVar) {
               ph_dphi.push_back(-10);
      
            } 

   static SG::AuxElement::ConstAccessor<float> acc_jvt("Jvt");

   if (acc_jvt.isAvailable(input)) {
      jvt.push_back(acc_jvt(input));
   } else {
      jvt.push_back(-9999);
   }*/

  /* if(!jet_isSelected && !doTrim()){  //Trim
      pt_all.push_back(input.pt());
      eta_all.push_back(input.eta());
      phi_all.push_back(input.phi());
      isBad_all.push_back(acc_bad(input)); 
      passOR_all.push_back(acc_passOR(input));
      jvt_all.push_back(acc_jvt(input));
   }*/

   if(jet_isSelected && !doTrim()){  // we don't need any jet info in Sytematics ...

      if(jet_isGood){
         if(jet_isLeading < 4){

            pt.push_back(input.pt());
            eta.push_back(input.eta());
            phi.push_back(input.phi());
            
            try {
               met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
               met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
               ph_dphi.push_back(acc_new_jet_ph_dphi(input));
            } catch (SG::ExcBadAuxVar) {
               met_nomuon_dphi.push_back(-10);
               met_wmuon_dphi.push_back(-10);
               ph_dphi.push_back(-10);
      
            }
   			m.push_back(input.m());

				/*
				raw_pt.push_back(thisRawJet->pt());
				raw_eta.push_back(thisRawJet->eta());
				raw_phi.push_back(thisRawJet->phi());
				raw_m.push_back(thisRawJet->m());*/
				Float_t tmp_timing(-9999);
				Float_t tmp_emfrac(-9999);
				/*Float_t tmp_hecf(-9999);
				Float_t tmp_hecq(-9999);
				Float_t tmp_larq(-9999);
				Float_t tmp_avglarq(-9999);
				Float_t tmp_fmax(-9999);
				Int_t tmp_fmaxi(-9999);
				Float_t tmp_negE(-9999);*/
				input.getAttribute(xAOD::JetAttribute::Timing, tmp_timing);
				input.getAttribute(xAOD::JetAttribute::EMFrac, tmp_emfrac);
				/*input.getAttribute(xAOD::JetAttribute::HECFrac, tmp_hecf);
				input.getAttribute(xAOD::JetAttribute::HECQuality, tmp_hecq);
				input.getAttribute(xAOD::JetAttribute::LArQuality, tmp_larq);
				input.getAttribute(xAOD::JetAttribute::AverageLArQF, tmp_avglarq);
				input.getAttribute(xAOD::JetAttribute::FracSamplingMax, tmp_fmax);
				input.getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex, tmp_fmaxi);
				input.getAttribute(xAOD::JetAttribute::NegativeE, tmp_negE);*/
				timing.push_back(tmp_timing);
				emfrac.push_back(tmp_emfrac);
				/*hecf.push_back(tmp_hecf);
				hecq.push_back(tmp_hecq);
				larq.push_back(tmp_larq);
				avglarq.push_back(tmp_avglarq);
				fmax.push_back(tmp_fmax);
				fmaxi.push_back(tmp_fmaxi);
				negE.push_back(tmp_negE);*/
				static SG::AuxElement::ConstAccessor<int> acc_PartonTruthLabelID("PartonTruthLabelID");
				Int_t tmp_PartonTruthLabelID(-9999);
				
				if (acc_PartonTruthLabelID.isAvailable(input)) {
					tmp_PartonTruthLabelID = acc_PartonTruthLabelID(input);
				}
				PartonTruthLabelID.push_back(tmp_PartonTruthLabelID);
            
         }

      }
    }
   return;
}

