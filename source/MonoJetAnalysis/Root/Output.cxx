#include "MonoJetAnalysis/Output.h"

#include <TTree.h>
#include <stdexcept>

Analysis::Output::Output() : m_attachedTree(nullptr)
{
   reset();
}

Analysis::Output::~Output()
{
}

void Analysis::Output::reset()
{
   evt.reset();

   for (auto &kv : met) kv.second.reset();
   for (auto &kv : mu) kv.second.reset();
   for (auto &kv : el) kv.second.reset();
   for (auto &kv : jet) kv.second.reset();
   for (auto &kv : ph) kv.second.reset();
   for (auto &kv : tau) kv.second.reset();
}

void Analysis::Output::setDoTrim(Bool_t val)
{
   m_doTrim = val;
}

void Analysis::Output::setDoMonophoton(Bool_t val)   //FEDE
{
   m_doMonophoton = val;

}

void Analysis::Output::setIsData(Bool_t val)   //FEDE
{
   m_isData = val;

}


void Analysis::Output::attachToTree(TTree *tree)
{
   
   // first, set trimming if appropriate
   evt.setDoTrim(m_doTrim);    			
   for (auto &kv : met) kv.second.setDoTrim(m_doTrim);
   for (auto &kv : mu)  kv.second.setDoTrim(m_doTrim);
   for (auto &kv : el)  kv.second.setDoTrim(m_doTrim);
   for (auto &kv : jet) kv.second.setDoTrim(m_doTrim);
   for (auto &kv : ph) kv.second.setDoTrim(m_doTrim);
   for (auto &kv : tau) kv.second.setDoTrim(m_doTrim);

   evt.setDoMonophoton(m_doMonophoton);
   for (auto &kv : met) kv.second.setDoMonophoton(m_doMonophoton);
   for (auto &kv : mu)  kv.second.setDoMonophoton(m_doMonophoton);
   for (auto &kv : el)  kv.second.setDoMonophoton(m_doMonophoton);
   for (auto &kv : jet) kv.second.setDoMonophoton(m_doMonophoton);
   for (auto &kv : ph)  kv.second.setDoMonophoton(m_doMonophoton);
   for (auto &kv : tau) kv.second.setDoMonophoton(m_doMonophoton);

   evt.setIsData(m_isData);
   for (auto &kv : met) kv.second.setIsData(m_isData);
   for (auto &kv : mu)  kv.second.setIsData(m_isData);
   for (auto &kv : el)  kv.second.setIsData(m_isData);
   for (auto &kv : jet) kv.second.setIsData(m_isData);
   for (auto &kv : ph)  kv.second.setIsData(m_isData);
   for (auto &kv : tau) kv.second.setIsData(m_isData);
   

   // then, attach to TTree
   if (tree != m_attachedTree) {
      m_attachedTree = tree; // do NOT take ownership

      evt.attachToTree(tree);

      for (auto &kv : met) kv.second.attachToTree(tree);
      for (auto &kv : mu)  kv.second.attachToTree(tree);
      for (auto &kv : el)  kv.second.attachToTree(tree);
      for (auto &kv : jet) kv.second.attachToTree(tree);
      for (auto &kv : ph)  kv.second.attachToTree(tree);
      for (auto &kv : tau) kv.second.attachToTree(tree);
   }
}

void Analysis::Output::save()
{
   if (!m_attachedTree) throw std::runtime_error("Output candidate not attached to any TTree");
   m_attachedTree->Fill();
}
