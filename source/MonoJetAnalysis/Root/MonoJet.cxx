#ifdef ROOTCORE
#include <xAODRootAccess/tools/TReturnCode.h>
#include <AsgTools/StatusCode.h>
#endif
#include <PATInterfaces/CorrectionCode.h>
#include <PATInterfaces/SystematicCode.h>

#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/OutputStream.h>
#include <MonoJetAnalysis/MonoJet.h>
#include <xAODCutFlow/CutBookkeeper.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>

#include <xAODBase/IParticleHelpers.h>
#include "FourMomUtils/xAODP4Helpers.h"

//xAODTruth
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthEvent.h>
#include "xAODTruth/xAODTruthHelpers.h"

#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TTree.h>

#include <TEnv.h>

// utils
#include <MonoJetAnalysis/Common.h>
#include <boost/algorithm/string.hpp>

// For reading metadata
#include "xAODMetaData/FileMetaData.h"
#include "AsgTools/AsgMetadataTool.h"

//tools
#include <MonoJetAnalysis/GetTruthBosonP4.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <IsolationSelection/IsolationSelectionTool.h>

//for monophoton analysis
#include <PhotonVertexSelection/PhotonPointingTool.h>
#include <ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h>
#include <PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h>
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"
#include <SUSYTools/SUSYCrossSection.h>
#include <PMGTools/PMGTruthWeightTool.h>

// this is needed to distribute the algorithm to the workers
ClassImp(MonoJet)

std::vector<std::string> getTokens(TString line, TString delim)
{
   std::vector<std::string> vtokens;
   TObjArray* tokens = TString(line).Tokenize(delim); //delimiters
   if (tokens->GetEntriesFast()) {
      TIter iString(tokens);
      TObjString* os = 0;
      while ((os = (TObjString*)iString())) {
         vtokens.push_back(os->GetString().Data());
      }
   }
   delete tokens;

   return vtokens;
}

MonoJet :: MonoJet()
   :
   config_file(""),
   ST_config_file(""),
   PrwActualMu2017File(""),
   PrwActualMu2018File(""),
   prw_file(""),
   lumicalc_file(""),
   GRL_file(""),
   //   MC_campaign(""),
   forbidden_syst(""),
   trigger_list(""),
   metSkim(0),
   metSkimForSyst(0),
   ptSkim(0),
   ptSkimForSyst(0),
   m_grl("GoodRunsListSelectionTool/grl", this),
   m_objTool("ST::SUSYObjDef_xAOD/ST", this),
   // m_leptonIsolationLooseTrackOnly(""),
   m_determinedDerivation(kFALSE),
   m_isEXOT5(kFALSE),
   m_isEXOT6(kFALSE),
   m_eventCounter(0),
   m_isMC(kFALSE),
   m_isAFII(kFALSE),
   //m_doSystTrees(kFALSE),

   // mono-photon
   m_isMC_Wenu_Zee(kFALSE), 
   m_photonPointingTool("CP::PhotonPointingTool", this),
   m_photonTightIsEMSelector("AsgPhotonIsEMSelector", this),
   m_photonIsoSF("AsgPhotonEfficiencyCorrectionTool", this),
   m_photonIDSF("AsgPhotonEfficiencyCorrectionTool", this),
   doMonophoton(kFALSE),
   cutSystematics(kFALSE), 
   m_doSkim(kFALSE),
   m_doTrim(kFALSE),
   MC_sample("") 

{
   // Here you put any code for the base initialization of variables,
   // e.g. initialize all pointers to 0.  Note that you should only put
   // the most basic initialization here, since this method will be
   // called on both the submission and the worker node.  Most of your
   // initialization code will go into histInitialize() and
   // initialize().
#ifdef ROOTCORE
//xAOD::TReturnCode::enableFailure();
//StatusCode::enableFailure();
#endif
//CP::SystematicCode::enableFailure();
//CP::CorrectionCode::enableFailure();
}



EL::StatusCode MonoJet :: setupJob(EL::Job& job)
{
   // Here you put code that sets up the job on the submission object
   // so that it is ready to work with your algorithm, e.g. you can
   // request the D3PDReader service or add output files.  Any code you
   // put here could instead also go into the submission script.  The
   // sole advantage of putting it here is that it gets automatically
   // activated/deactivated when you add/remove the algorithm from your
   // job, which may or may not be of value to you.

   ANA_MSG_INFO("in setupJob");

   // let's initialize the algorithm to use the xAODRootAccess package
   job.useXAOD();

   // add output stream (i.e. files in the data-XXX directory)
   EL::OutputStream output("minitrees");
   job.outputAdd(output);

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: histInitialize()
{
   // Here you do everything that needs to be done at the very
   // beginning on each worker node, e.g. create histograms and output
   // trees.  This method gets called before any input files are
   // connected.

   ANA_MSG_INFO("in histInitialize");

   TString cf_name("SignalRegion");

   ANA_MSG_INFO("histInitialize...Name will be " << cf_name.Data());
   m_cutFlow = Analysis::CutFlowTool(cf_name);
   m_cutFlow.addCut("processed");
   m_cutFlow.addCut("GRL");
   m_cutFlow.addCut("cleaning");	
   m_cutFlow.addCut("trigger");
   m_cutFlow.addCut("vertex");		
   m_cutFlow.addCut("MET_cleaning");	
   m_cutFlow.addCut("BCH_cleaning");
   if(!doMonophoton){
      m_cutFlow.addCut("Leading_Pt_cut");
      m_cutFlow.addCut("MET_cut");
      m_cutFlow.addCut("Leading_cleaning");
      m_cutFlow.addCut("tau_veto");   		
      m_cutFlow.addCut("electron_veto");
      m_cutFlow.addCut("muon_veto");  			
      m_cutFlow.addCut("photon_veto");		
      m_cutFlow.addCut("jet_multiplicity");
      m_cutFlow.addCut("jet_MET_overlap");
      m_cutFlow.addCut("MET_cut_2");		
      m_cutFlow.addCut("Leading_Pt_cut_2");
   }
   if(doMonophoton){
      m_cutFlow.addCut("MET_cut");
      m_cutFlow.addCut("loose_photon");
      m_cutFlow.addCut("MET_sig");
      m_cutFlow.addCut("leading_cleaning");
      m_cutFlow.addCut("leading_MET_overlap");
      m_cutFlow.addCut("ZVtx");
      m_cutFlow.addCut("tau_veto");		
      m_cutFlow.addCut("electron_veto");
      m_cutFlow.addCut("muon_veto");
      m_cutFlow.addCut("jet_veto");
   }				


   //
   // keep track of the number of events pre-derivation
   //
   ANA_MSG_INFO("Initializing event-count histogram");

   if(doMonophoton&&m_doTheoSystematics){  //theo syst
      m_histoEventCountNonNominal= new TH1F("histoEventCountNonNominal", "event count (only for derivations, only MC weight applied)", 120, 0, 120);  // sum of weights from cbk->SumOfEventWeights()
      m_histoEventCountNonNominal_PMG= new TH1F("histoEventCountNonNominalPMG", "event count (only for derivations, only MC weight applied)", 120, 0, 120); // sum of pre-skim weights from m_weightTool->GetWeight(weight) (does it work?)
      wk()->addOutput(m_histoEventCountNonNominal);
      wk()->addOutput(m_histoEventCountNonNominal_PMG);
   }
   m_histoEventCount = new TH1F("histoEventCount", "event count (only for derivations, only MC weight applied)", 100, 0, 100);
   m_histoEventCount->Fill("initial_weighted", 0);
   m_histoEventCount->Fill("initial_raw", 0);

   wk()->addOutput(m_histoEventCount);

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: fileExecute()
{
   // Here you do everything that needs to be done exactly once for every
   // single file, e.g. collect a list of all lumi-blocks processed

   ANA_MSG_INFO("in fileExecute");

   xAOD::TEvent *event = wk()->xaodEvent();

   //
   // read original number of processed events, if any
   // from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisMetadata
   //

   TTree *MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
   if (!MetaData) {
      ANA_MSG_FATAL("MetaData not found!");
      return EL::StatusCode::FAILURE;
   }
   MetaData->LoadTree(0);

   if(doMonophoton){    //MonoPhoton 
      TString input_file = wk()->metaData()->castString("sample_name") ;
      //ANA_MSG_INFO("INPUT FILE    "<<input_file);
      if (input_file.Contains("Wenu") || input_file.Contains("Zee")) {
         m_isMC_Wenu_Zee = true;
         ANA_MSG_INFO("isMC_Wenu_Zee    "<<m_isMC_Wenu_Zee);
      }
   }
   const Bool_t isDerivation = !MetaData->GetBranch("StreamAOD");
   const Bool_t shouldDoThis = (wk()->metaData()->castString("isData") == "NO"); // m_isMC not filled yet
   ANA_MSG_INFO("isDerivation = " << isDerivation << " (used to decide if reading CutBookkeeperContainer or not)");
   ANA_MSG_INFO("is this MC = " << shouldDoThis << " (used to decide if reading CutBookkeeperContainer or not)");


   //get sum of event weights in derivation skims
   if (isDerivation && shouldDoThis) {
      // set pointers & retrieve the bookkeeper container
      const xAOD::CutBookkeeperContainer* bookkeepers = nullptr;
      if (!event->retrieveMetaInput(bookkeepers, "CutBookkeepers").isSuccess()) {
         ANA_MSG_ERROR("Failed to retrieve CutBookkeepers from MetaData");
         return EL::StatusCode::FAILURE;
      }
      const xAOD::CutBookkeeper* event_bookkeeper = nullptr;

      // find the max cycle where input stream is StreamAOD and the name is AllExecutedEvents
      int maxCycle = -1;
		
      for (auto cbk : *bookkeepers) {
			ANA_MSG_INFO("Book keeper name " <<cbk->name());
         if (cbk->inputStream() == "StreamAOD" && cbk->name() == "AllExecutedEvents" && cbk->cycle() > maxCycle) {
            maxCycle = cbk->cycle();
            event_bookkeeper = cbk;
         }

         if(doMonophoton && m_doTheoSystematics){ //theo syst
            //if(cbk->name().find("NonNominalMCWeight")!=std::string::npos ){ //check weight
            if(cbk->name().find("AllExecutedEvents")!=std::string::npos ){ 
               m_histoEventCountNonNominal->Fill(cbk->name().c_str(),cbk->sumOfEventWeights());
	    }
         }
      }
      // if the right & proper bookkeeper is found, read info
      if (event_bookkeeper) {
         m_histoEventCount->Fill("initial_weighted", event_bookkeeper->sumOfEventWeights());
         m_histoEventCount->Fill("initial_raw", event_bookkeeper->nAcceptedEvents());

			
      }


   } // derivation
   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: changeInput(bool /*firstFile*/)
{
   // Here you do everything you need to do when we change input files,
   // e.g. resetting branch addresses on trees.  If you are using
   // D3PDReader or a similar service this method is not needed.
   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: initialize()
{
   // Here you do everything that you need to do after the first input
   // file has been connected and before the first event is processed,
   // e.g. create additional histograms based on which variables are
   // available in the input files.  You can also create all of your
   // histograms and trees in here, but be aware that this method
   // doesn't get called if no events are processed.  So any objects
   // you create here won't be available in the output if you have no
   // input events.

   ANA_MSG_INFO("in initialize");

   //
   // read analysis config from configuration file
   //
   ANA_CHECK(readConfig());


   
   //
   // read job config from class settings
   //
   m_isMC = (wk()->metaData()->castString("isData") == "NO");
   m_isAFII = (wk()->metaData()->castString("isAFII") == "YES");
   Int_t showerType = -9999;
   if (m_isMC) showerType = ST::getMCShowerType(wk()->metaData()->castString("sample_name"));

   m_inputFileName = wk()->metaData()->castString("sample_name");

   //
   // print config
   //
   ANA_MSG_INFO("called with:");
   ANA_MSG_INFO("  - Running on: " << m_inputFileName);
   ANA_MSG_INFO("  - isMC = " << m_isMC);
   ANA_MSG_INFO("  - isAFII = " << m_isAFII);
   ANA_MSG_INFO("  - doSystematics = " << m_doSystematics);
   ANA_MSG_INFO("  - doTheoSystematics = " << m_doTheoSystematics); //mono-photon
   ANA_MSG_INFO("  - MonoJetAnalysis config file = " << config_file);
   ANA_MSG_INFO("  - SUSYTools config file = " << ST_config_file);
   ANA_MSG_INFO("  - prw_file = " << prw_file);
   ANA_MSG_INFO("  - PrwActualMuFile for 2017 = " <<  PrwActualMu2017File);
   ANA_MSG_INFO("  - PrwActualMuFile for 2018 = " <<  PrwActualMu2018File);
   ANA_MSG_INFO("  - lumicalc_file = " << lumicalc_file);
   ANA_MSG_INFO("  - GRL_file = " << GRL_file);
   //   ANA_MSG_INFO("  - MC campaign = " << MC_campaign);
   ANA_MSG_INFO("  - ptSkim = " << ptSkim << " MeV ( " << ptSkimForSyst << " MeV for systematics)");
   ANA_MSG_INFO("  - metSkim = " << metSkim << " MeV ( " << metSkimForSyst << " MeV for systematics)");
   ANA_MSG_INFO("  - forbidden_syst = " << forbidden_syst);
   ANA_MSG_INFO("  - trigger_list = " << trigger_list);
   ANA_MSG_INFO("  - showerType = " << showerType);
   ANA_MSG_INFO("  - doIncludeTaus = " << m_doIncludeTaus);

   // ANA_MSG_INFO("  - doSystTrees = " << m_doSystTrees);   non in monojet
   ANA_MSG_INFO("  - doMonophoton = " << doMonophoton);		
   ANA_MSG_INFO("  - doSkim = " << m_doSkim);		
   ANA_MSG_INFO("  - doTrim = " << m_doTrim);	
   ANA_MSG_INFO("  - isMC_Wenu_Zee = " << m_isMC_Wenu_Zee); // monophoton


   //
   // number of events
   //
   m_eventCounter = 0;
   xAOD::TEvent *event = wk()->xaodEvent();
   ANA_MSG_INFO("Number of events = " << event->getEntries());


   //
   // GRL
   //
   std::vector<std::string> vecStringGRL = getTokens(GRL_file, ",");
   std::transform(vecStringGRL.begin(), vecStringGRL.end(), vecStringGRL.begin(), [](std::string x) -> std::string { return PathResolverFindCalibFile(x); });
   ANA_CHECK(m_grl.setProperty("GoodRunsListVec", vecStringGRL));
   ANA_CHECK(m_grl.setProperty("PassThrough", kFALSE));
   ANA_CHECK(m_grl.initialize());

   //
   // SUSYTools
   //
   const ST::ISUSYObjDef_xAODTool::DataSource datasource = (!m_isMC ? ST::ISUSYObjDef_xAODTool::Data : (m_isAFII ? ST::ISUSYObjDef_xAODTool::AtlfastII : ST::ISUSYObjDef_xAODTool::FullSim));
   ANA_CHECK(m_objTool.setProperty("DataSource", datasource));
   ANA_CHECK(m_objTool.setProperty("ConfigFile", ST_config_file.Data()));
   ANA_CHECK(m_objTool.setProperty("METDoTrkSyst", kTRUE));
   ANA_CHECK(m_objTool.setProperty("METDoCaloSyst", kFALSE));

   //Pile up Autoconfiguration
   if (m_isMC) {

      //get MetaData
      const xAOD::FileMetaData* fmd = nullptr;
      std::string amiTag = "unknown";
      if (event->retrieveMetaInput(fmd, "FileMetaData").isSuccess()) {
         if (! fmd->value(xAOD::FileMetaData::amiTag, amiTag)) {
            ATH_MSG_ERROR("The object is available, the variable is not.");
         }
      } else {
         ATH_MSG_ERROR("The object is not available.");
      }
      std::cout << amiTag << std::endl;

      //Get mc campaign and set properly lumicalc files for reweighting
      const xAOD::EventInfo* evtInfo = 0;
      ANA_CHECK(evtStore()->retrieve(evtInfo, "EventInfo"));
      //      std::string mc_campaign;
      std::vector<std::string> prw_lumicalc;

      getMCcampaignInfo(lumicalc_file, *evtInfo, amiTag, prw_lumicalc, MC_campaign);
      std::transform(prw_lumicalc.begin(), prw_lumicalc.end(), prw_lumicalc.begin(), [](std::string x) -> std::string { return PathResolverFindCalibFile(x); });
      
      ANA_CHECK(m_objTool.setProperty("PRWLumiCalcFiles", prw_lumicalc));

      //Adding actual MU pileup file
      std::vector<std::string> prw_conf;
      if (MC_campaign == "mc16c" || MC_campaign == "mc16d") prw_conf.push_back((std::string)PrwActualMu2017File);
      else if (MC_campaign == "mc16e") prw_conf.push_back(std::string(PrwActualMu2018File));
      /*
      std::vector<std::string> tmp_actualMU_conf = getTokens(PrwActualMuFile, ","); 
      for(uint i = 0; i < tmp_actualMU_conf.size(); i++){
        if (mc_campaign == "mc16c" || mc_campaign == "mc16d" || mc_campaign == "mc16e") prw_conf.push_back(tmp_actualMU_conf.at(i));
      }
      */

      //Now, if ST database has sample in database set autoconfiguration, else manual config with pile-up files in MonoJetanalysis package
      uint32_t dsid = evtInfo->mcChannelNumber();
		ANA_MSG_INFO(evtInfo->mcChannelNumber());
      std::string fsim = (m_isAFII) ? "AFII" : "FS";

      std::string prwConfigFile = PathResolverFindCalibFile("dev/PileupReweighting/share/DSID" + std::to_string(dsid/1000) + "xxx/pileup_" + MC_campaign.Data() + "_dsid" + std::to_string(getRun(dsid,m_inputFileName)) + "_" + fsim + ".root");
      //      std::string prwConfigFile = PathResolverFindCalibFile("dev/SUSYTools/PRW_AUTOCONFIG/files/pileup_" + mc_campaign + "_dsid" + std::to_string(dsid) + ".root");
      if (prwConfigFile.empty()) {
	//      if (prwConfigFile.empty() || dsid == 366015) {
         ANA_MSG_INFO("File " << prwConfigFile << " not found in ST database. Set manual prw configuration with the following files:\n\t" << prw_file);
         std::vector<std::string> tmp_prw_conf = getTokens(prw_file, ",");
         for (uint i = 0; i < tmp_prw_conf.size(); i++) {
            if (tmp_prw_conf.at(i).find(MC_campaign) != std::string::npos) prw_conf.push_back(tmp_prw_conf.at(i));
         }
         std::transform(prw_conf.begin(), prw_conf.end(), prw_conf.begin(), [](std::string x) -> std::string { return PathResolverFindCalibFile(x); });

      } else {
         //pile-up autoconfiguration
         ANA_MSG_INFO("Sample " << dsid << " is in ST pileup database. Going to set PRW auconfiguration");
         ANA_CHECK(m_objTool.setProperty("AutoconfigurePRWTool", true));
	 //=====================================================
	 if(dsid>=366010 && dsid<= 366035)
	  {
	      ATH_MSG_INFO("****Applying a patch for buggy Znunu samples with p3712 suffix. Remove when new will be available!****");
	      if(m_inputFileName.Contains("BFilter")){
		ANA_CHECK(m_objTool.setProperty("AutoconfigurePRWToolHFFilter","BFilter"));}
	      else if(m_inputFileName.Contains("CFilterBVeto")){ 
		ANA_CHECK(m_objTool.setProperty("AutoconfigurePRWToolHFFilter","CFilterBVeto"));}
	      else if(m_inputFileName.Contains("CVetoBVeto")){ 
		ANA_CHECK(m_objTool.setProperty("AutoconfigurePRWToolHFFilter","CvetoBVeto"));}
	      else ATH_MSG_ERROR ("Heavy flavor filter naming is wrong and cannot re-map dsid! SHould be BFilter, CFilterBVeto, or CVetoBVeto.");
	    }
	 //=====================================================
	 ANA_CHECK(m_objTool.setProperty("AutoconfigurePRWToolHFFilter", "BFilter"));
	 ANA_MSG_INFO("Setting MC campaign: " << MC_campaign);
         ANA_CHECK(m_objTool.setProperty("mcCampaign", MC_campaign));
         prw_conf.push_back(prwConfigFile);
         std::transform(prw_conf.begin(), prw_conf.end(), prw_conf.begin(), [](std::string x) -> std::string { return PathResolverFindCalibFile(x); });
      }

      for (uint i = 0; i < prw_conf.size(); i++) ATH_MSG_INFO("Prw file added: " << prw_conf.at(i));
      ANA_CHECK(m_objTool.setProperty("PRWConfigFiles", prw_conf));
   }


   //Guess shower type for btagging MC/MC SFs
   if (m_isMC) {
      ANA_CHECK(m_objTool.setProperty("ShowerType", (Int_t)showerType));
   }

   if (!m_doIncludeTaus) {
      ANA_CHECK(m_objTool->setBoolProperty("DoTauOR", false));
      ATH_MSG_INFO("Excluded taus in the overlap removal");
   } else {
      ANA_CHECK(m_objTool->setBoolProperty("DoTauOR", true));
      ATH_MSG_INFO("Included taus in the overlap removal");
   }

   //if (m_debug) m_objTool.msg().setLevel(MSG::VERBOSE);
   //ANA_CHECK(m_objTool.setBoolProperty("DebugMode", kTRUE));
   // ANA_CHECK(m_objTool.initialize());


   // electron, muon and photon isolation tools. do this here to allow advance studies. presumably will someday be provided by SUSYTools.
   /*if (!m_leptonIsolationLooseTrackOnly.isUserConfigured()) {
      SET_DUAL_TOOL(m_leptonIsolationLooseTrackOnly, CP::IsolationSelectionTool, "electronLooseTrackOnly");
      ANA_CHECK(m_leptonIsolationLooseTrackOnly.setProperty("ElectronWP", "LooseTrackOnly"));
      ANA_CHECK(m_leptonIsolationLooseTrackOnly.setProperty("MuonWP", "LooseTrackOnly"));
      //      ANA_CHECK(m_leptonIsolationLooseTrackOnly.setProperty("PhotonWP", "FixedCutTightCaloOnly")); //<JL>
      ANA_CHECK(m_leptonIsolationLooseTrackOnly.initialize());
   }*/



   if (m_doIncludeTaus) {	
      ANA_MSG_INFO("Setting tau tool properties");
      m_tauVeryLoose = CxxUtils::make_unique<TauAnalysisTools::TauSelectionTool>("TauSelectionToolVeryLoose");
      m_tauLoose = CxxUtils::make_unique<TauAnalysisTools::TauSelectionTool>("TauSelectionToolLoose");
      m_tauMedium = CxxUtils::make_unique<TauAnalysisTools::TauSelectionTool>("TauSelectionToolMedium");
      m_tauTight = CxxUtils::make_unique<TauAnalysisTools::TauSelectionTool>("TauSelectionToolTight");

      ANA_CHECK(m_tauVeryLoose->setProperty("JetIDWP", static_cast<int>(TauAnalysisTools::JETIDBDTVERYLOOSE)));
      ANA_CHECK(m_tauLoose->setProperty("JetIDWP", static_cast<int>(TauAnalysisTools::JETIDBDTLOOSE)));
      ANA_CHECK(m_tauMedium->setProperty("JetIDWP", static_cast<int>(TauAnalysisTools::JETIDBDTMEDIUM)));
      ANA_CHECK(m_tauTight->setProperty("JetIDWP", static_cast<int>(TauAnalysisTools::JETIDBDTTIGHT)));

      ANA_MSG_INFO("Initializing tau tool");
      ANA_CHECK(m_tauVeryLoose->initialize());
      ANA_CHECK(m_tauLoose->initialize());
      ANA_CHECK(m_tauMedium->initialize());
      ANA_CHECK(m_tauTight->initialize());
   }

   if(doMonophoton){
      ANA_MSG_INFO("Setting photon pointing tool (used for Z vertex selection)");
      if(!m_photonPointingTool.isUserConfigured()){
         ANA_CHECK(m_photonPointingTool.initialize());
      }
      ANA_MSG_INFO("Setting photon isEMSelector tool");
      if(!m_photonTightIsEMSelector.isUserConfigured()){
         SET_DUAL_TOOL(m_photonTightIsEMSelector, AsgPhotonIsEMSelector, "PhotonTightIsEMSelector");
         ANA_CHECK(m_photonTightIsEMSelector.setProperty("isEMMask",egammaPID::PhotonTight));
         ANA_CHECK(m_photonTightIsEMSelector.setProperty("ConfigFile", "ElectronPhotonSelectorTools/offline/20180825/PhotonIsEMTightSelectorCutDefs.conf"));
         ANA_CHECK(m_photonTightIsEMSelector.initialize());
      }
      m_xSec_SUSY = new SUSY::CrossSectionDB(); 

      if(m_isMC){
         m_weightTool = new PMGTools::PMGTruthWeightTool("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");  
         ANA_CHECK(m_weightTool->initialize());
      }

      if(!m_photonIsoSF.isUserConfigured()){
         SET_DUAL_TOOL(m_photonIsoSF, AsgPhotonEfficiencyCorrectionTool, "IsoPhotonEfficiencyCorrectionTool");
         ANA_CHECK(m_photonIsoSF.setProperty("IsoKey","Tight"));
         ANA_CHECK(m_photonIsoSF.setProperty("ForceDataType",1));
         ANA_CHECK(m_photonIsoSF.initialize());

      }

      if(!m_photonIDSF.isUserConfigured()){
         SET_DUAL_TOOL(m_photonIDSF, AsgPhotonEfficiencyCorrectionTool, "IDPhotonEfficiencyCorrectionTool");
	 ANA_CHECK(m_photonIDSF.setProperty("ForceDataType",1));
         ANA_CHECK(m_photonIDSF.initialize());

      }
		
   }

   //
   // list of systematics to process
   //

   m_sysList.clear();
   if (m_doSystematics && m_isMC) {
      auto fullSystList = m_objTool->getSystInfoList();

      // list of uncertainties to be skipped
      if (m_doIncludeTaus) forbidden_syst += ",TAU_EFF_TRIGGER"; // we don't use tau triggers
      else  forbidden_syst += ",TAUS"; 
      std::vector<std::string> forbidden = getTokens(forbidden_syst, ",");

      for (const auto& syst : fullSystList) {
         const TString thisSyst = syst.systset.name();
         Bool_t keepThis(kTRUE);
         for (const auto& badWord : forbidden) {
            if (thisSyst.Contains(badWord)) {
               ANA_MSG_INFO("Skipping the systematic variation \"" << thisSyst << "\"");
               keepThis = kFALSE;
               break;
            }
         } // loop over forbidden systematics

         if (keepThis) {
            m_sysList.push_back(syst);
         }
      }
   } else {
      ST::SystInfo nominal;
      nominal.affectsKinematics = kFALSE;
      nominal.affectsWeights = kFALSE;
      nominal.affectsType = ST::Unknown;
      m_sysList.push_back(nominal);
   }


   //
   // define the output TTree
   //
   m_tree.clear();
   m_cand.clear();

   // handle to the output file
   TFile *thisFile = wk()->getOutputFile("minitrees");

   for (const auto& syst : m_sysList) {
      const TString thisSyst = syst.systset.name();
      const TString treeName = (thisSyst == "") ? MC_sample : (MC_sample+ "syst" + thisSyst).ReplaceAll(" ", "_");  
      const TString treeTitle = thisSyst;
      ANA_MSG_INFO("Will consider the systematic variation \"" << thisSyst << "\"");

      const Bool_t isNominal = (thisSyst == "");
      const Bool_t trim = !isNominal || m_doTrim;

      if((!cutSystematics&&!thisSyst.Contains("TAUS_TRUEELECTRON_EFF_")&&!thisSyst.Contains("TAU_EFF")) || (cutSystematics && !thisSyst.Contains("_EFF_")&&!thisSyst.Contains("JET_JvtEfficiency")&&!thisSyst.Contains("PRW_DATASF_"))){  // cut unnecessary systematics trees, for weight-related uncertainties
         ANA_MSG_INFO("Creating TTree named " <<  treeName.Data() << " for systematic named " << thisSyst.Data());
         m_tree[thisSyst] = new TTree(treeName, treeTitle);
         m_tree[thisSyst]->SetDirectory(thisFile);
      }

      // save, in all trees, the trigger decision
      //trim strings in order to avoid undersidered empty spaces (we need to create the string trigger_ + triggersToSave.at(i))
      std::vector<std::string> triggersToSave = getTokens(trigger_list, ",");
      for (uint i = 0; i < triggersToSave.size(); i++)    boost::algorithm::trim(triggersToSave.at(i));
      //for (uint i = 0; i< triggersToSave.size();i++) std::cout << triggersToSave.at(i)<<std::endl;
      m_cand[thisSyst].evt.setTriggers(triggersToSave);




      // define all elements of the output tree

      if(!doMonophoton){
         m_cand[thisSyst].met["met_tst"] = Analysis::OutputMET("met_tst", trim);
         m_cand[thisSyst].met["met_cst"] = Analysis::OutputMET("met_cst", trim);	// monophoton
         m_cand[thisSyst].met["met_nomuon_tst"] = Analysis::OutputMET("met_nomuon_tst", trim);
         m_cand[thisSyst].met["met_noelectron_tst"] = Analysis::OutputMET("met_noelectron_tst", trim);
         m_cand[thisSyst].met["met_nophoton_tst"] = Analysis::OutputMET("met_nophoton_tst", trim);	
         m_cand[thisSyst].met["met_track"] = Analysis::OutputMET("met_track", trim);			
         m_cand[thisSyst].met["met_truth"] = Analysis::OutputMET("met_truth", trim);			
         m_cand[thisSyst].met["met_softerm_tst"] = Analysis::OutputMET("met_softerm_tst", trim);
         m_cand[thisSyst].met["met_softerm_cst"] = Analysis::OutputMET("met_softerm_cst", trim);

         // monophoton
         //m_cand[thisSyst].met["met_muonterm_tst"] = Analysis::OutputMET("met_muonterm_tst", trim);
         m_cand[thisSyst].met["met_muonterm"] = Analysis::OutputMET("met_muonterm", trim);
         m_cand[thisSyst].met["met_eleterm"] = Analysis::OutputMET("met_eleterm", trim);
         m_cand[thisSyst].met["met_phterm"] = Analysis::OutputMET("met_phterm", trim);
         m_cand[thisSyst].met["met_jetterm"] = Analysis::OutputMET("met_jetterm", trim);
         m_cand[thisSyst].mu["mu"] = Analysis::OutputMuon("mu", trim);
         m_cand[thisSyst].el["el"] = Analysis::OutputElectron("el", trim);
         m_cand[thisSyst].jet["jet"] = Analysis::OutputJet("jet", trim);
		   
         m_cand[thisSyst].ph["ph"] = Analysis::OutputPhoton("ph", trim);
	 m_cand[thisSyst].mu["mu_baseline"] = Analysis::OutputMuon("mu_baseline", trim);
	 m_cand[thisSyst].el["el_baseline"] = Analysis::OutputElectron("el_baseline", trim);
	 m_cand[thisSyst].ph["ph_baseline"] = Analysis::OutputPhoton("ph_baseline", trim);

	 if (m_doIncludeTaus){
            m_cand[thisSyst].tau["tau"] = Analysis::OutputTau("tau", trim);
            m_cand[thisSyst].tau["tau_baseline"] = Analysis::OutputTau("tau_baseline", trim);
	    m_cand[thisSyst].tau["tau_baseline"].tau_SF_Sys = {};
         }
         m_cand[thisSyst].el["el_baseline"].el_SF_Sys = {};
         m_cand[thisSyst].mu["mu_baseline"].mu_SF_Sys = {};
         m_cand[thisSyst].ph["ph_baseline"].ph_SF_Sys = {};
      }

      else if (doMonophoton ){
         m_cand[thisSyst].met["met_tst"] = Analysis::OutputMET("met_tst", trim);
         m_cand[thisSyst].met["met_cst"] = Analysis::OutputMET("met_cst", trim);	// monophoton
         m_cand[thisSyst].met["met_nomuon_tst"] = Analysis::OutputMET("met_nomuon_tst", trim);
         m_cand[thisSyst].met["met_noelectron_tst"] = Analysis::OutputMET("met_noelectron_tst", trim);
         m_cand[thisSyst].met["met_noelectron_tst_probe"] = Analysis::OutputMET("met_noelectron_tst_probe", trim);
         m_cand[thisSyst].met["met_track"] = Analysis::OutputMET("met_track", trim);			
         m_cand[thisSyst].met["met_truth"] = Analysis::OutputMET("met_truth", trim);			
         m_cand[thisSyst].met["met_softerm_tst"] = Analysis::OutputMET("met_softerm_tst", trim);
         m_cand[thisSyst].met["met_softerm_cst"] = Analysis::OutputMET("met_softerm_cst", trim);  
         m_cand[thisSyst].met["met_muonterm"] = Analysis::OutputMET("met_muonterm", trim);
	if (m_doIncludeTaus){m_cand[thisSyst].met["met_tauterm"] = Analysis::OutputMET("met_tauterm", trim);}
         m_cand[thisSyst].met["met_eleterm"] = Analysis::OutputMET("met_eleterm", trim);
         m_cand[thisSyst].met["met_phterm"] = Analysis::OutputMET("met_phterm", trim);
         m_cand[thisSyst].met["met_jetterm"] = Analysis::OutputMET("met_jetterm", trim);
         m_cand[thisSyst].mu["mu"] = Analysis::OutputMuon("mu", trim);
         m_cand[thisSyst].el["el"] = Analysis::OutputElectron("el", trim);
         m_cand[thisSyst].jet["jet"] = Analysis::OutputJet("jet", trim);
         //JVT test m_cand[thisSyst].jet["noJVT_jet"] = Analysis::OutputJet("noJVT_jet", trim); 
         m_cand[thisSyst].ph["ph"] = Analysis::OutputPhoton("ph", trim);
         if (m_doIncludeTaus) {  //Systematics reorganization
         	//m_cand[thisSyst].tau["tau"] = Analysis::OutputTau("tau", trim);
	    m_cand[thisSyst].tau["tau_baseline"] = Analysis::OutputTau("tau_baseline", trim);
	 }
	 if(cutSystematics && isNominal){
	    if (m_doIncludeTaus) {  //Systematics reorganization
               m_cand[thisSyst].evt.tau_SF_Sys = {};
            }
				m_cand[thisSyst].evt.el_SF_Sys = {};
				m_cand[thisSyst].evt.mu_SF_Sys = {};
				m_cand[thisSyst].evt.ph_SF_Sys = {};
				//m_cand[thisSyst].evt.baseline_ph_SF_Sys = {};
				m_cand[thisSyst].evt.jet_SF_Sys = {};
				m_cand[thisSyst].evt.pu_weight_Sys = {};
				//m_cand[thisSyst].evt.mconly_weight_Sys = {};
         }

         

			// antiSF histogram inizialization **************************************
			if(!cutSystematics || (cutSystematics && (thisSyst.Contains("EL_EFF") || thisSyst.Contains("MUON_EFF")|| thisSyst.Contains("TAU_EFF")|| thisSyst.Contains("TAUS_TRUEELECTRON_EFF")|| thisSyst.Contains("PRW_DATASF")|| thisSyst=="" ))){
				const TString histoName_eleSF = (thisSyst == "") ? "histo_eleSF_baseline" : ("histo_eleSF_baseline_syst_" + thisSyst).ReplaceAll(" ", "_");  
				const TString histoName_muSF = (thisSyst == "") ? "histo_muSF_baseline" : ("histo_muSF_baseline_syst_" + thisSyst).ReplaceAll(" ", "_");  
				const TString histoName_tauSF = (thisSyst == "") ? "histo_tauSF_baseline" : ("histo_tauSF_baseline_syst_" + thisSyst).ReplaceAll(" ", "_");  
				const TString histoName_countSF = (thisSyst == "") ? "histo_countSF" : ("histo_countSF_syst_" + thisSyst).ReplaceAll(" ", "_");

	  			m_histoEleSF = new TH2F(histoName_eleSF, "baseline electron SFs for antiSF", 100, 0., 2., 3, 0., 3.);
	  			m_histoMuSF = new TH2F(histoName_muSF, "baseline muon SFs for antiSF", 100, 0., 2., 3, 0., 3.);
	  			m_histoTauSF = new TH2F(histoName_tauSF, "baseline tau SFs for antiSF", 100, 0., 2., 3, 0., 3.);
	  			m_histoCountSF = new TH1F(histoName_countSF, "count event for SR-like and CR-like selections for antiSF", 18, 0., 18.);

				//m_histoCountSF->Sumw2();

				wk()->addOutput(m_histoEleSF); 
				wk()->addOutput(m_histoMuSF);  
				wk()->addOutput(m_histoCountSF);
				if(m_doIncludeTaus) wk()->addOutput(m_histoTauSF);  

				//SR no MET, SR with ph_dphi_met, SR1/2/3 with MET, CR no MET, CR with ph_dphi_nomuon/ele_met, CR with sig lepton no MET
				//std::string region[12]={"SR", "dphiSR","SR1","SR2","SR3","CR","muCR","1muCR","2muCR","elCR","2elCR", "PhJetCR"};

				std::string region[3]={"SR", "CR", "1muCR"}; 

				for (auto reg : region){
					m_histoEleSF->Fill(0.,reg.c_str(),0.);
					m_histoMuSF->Fill(0.,reg.c_str(),0.);
					if(m_doIncludeTaus) m_histoTauSF->Fill(0.,reg.c_str(),0.);
				}
				m_histoCountSF->Fill("SR_noEle",0);
				m_histoCountSF->Fill("SR_withEle",0);
				m_histoCountSF->Fill("SR_noMu",0);
				m_histoCountSF->Fill("SR_withMu",0);

				/*m_histoCountSF->Fill("dphiSR_noEle",0);
				m_histoCountSF->Fill("dphiSR_withEle",0);
				m_histoCountSF->Fill("dphiSR_noMu",0);
				m_histoCountSF->Fill("dphiSR_withMu",0);
				m_histoCountSF->Fill("SR1_noEle",0);
				m_histoCountSF->Fill("SR1_withEle",0);
				m_histoCountSF->Fill("SR1_noMu",0);
				m_histoCountSF->Fill("SR1_withMu",0);
				m_histoCountSF->Fill("SR2_noEle",0);
				m_histoCountSF->Fill("SR2_withEle",0);
				m_histoCountSF->Fill("SR2_noMu",0);
				m_histoCountSF->Fill("SR2_withMu",0);
				m_histoCountSF->Fill("SR3_noEle",0);
				m_histoCountSF->Fill("SR3_withEle",0);
				m_histoCountSF->Fill("SR3_noMu",0);
				m_histoCountSF->Fill("SR3_withMu",0);*/
				m_histoCountSF->Fill("CR_noEle",0);
				m_histoCountSF->Fill("CR_withEle",0);
				m_histoCountSF->Fill("CR_noMu",0);
				m_histoCountSF->Fill("CR_withMu",0);
				/*m_histoCountSF->Fill("muCR_noEle",0);
				m_histoCountSF->Fill("muCR_withEle",0);
				m_histoCountSF->Fill("muCR_noMu",0);
				m_histoCountSF->Fill("muCR_withMu",0);
				m_histoCountSF->Fill("elCR_noEle",0);
				m_histoCountSF->Fill("elCR_withEle",0);
				m_histoCountSF->Fill("elCR_noMu",0);
				m_histoCountSF->Fill("elCR_withMu",0);*/
				m_histoCountSF->Fill("1muCR_noEle",0);
				m_histoCountSF->Fill("1muCR_withEle",0);
				m_histoCountSF->Fill("1muCR_noMu",0);
				m_histoCountSF->Fill("1muCR_withMu",0);
				/*m_histoCountSF->Fill("2muCR_noEle",0);
				m_histoCountSF->Fill("2muCR_withEle",0);
				m_histoCountSF->Fill("2muCR_noMu",0);
				m_histoCountSF->Fill("2muCR_withMu",0);
				m_histoCountSF->Fill("2elCR_noEle",0);
				m_histoCountSF->Fill("2elCR_withEle",0);
				m_histoCountSF->Fill("2elCR_noMu",0);
				m_histoCountSF->Fill("2elCR_withMu",0);
				m_histoCountSF->Fill("PhJetCR_noEle",0);
				m_histoCountSF->Fill("PhJetCR_withEle",0);
				m_histoCountSF->Fill("PhJetCR_noMu",0);
				m_histoCountSF->Fill("PhJetCR_withMu",0);*/
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR_withTau",0);
				/*if(m_doIncludeTaus) m_histoCountSF->Fill("dphiSR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("dphiSR_withTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR1_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR1_withTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR2_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR2_withTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR3_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("SR3_withTau",0);*/
				if(m_doIncludeTaus) m_histoCountSF->Fill("CR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("CR_withTau",0);
				/*if(m_doIncludeTaus) m_histoCountSF->Fill("muCR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("muCR_withTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("elCR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("elCR_withTau",0);*/
				if(m_doIncludeTaus) m_histoCountSF->Fill("1muCR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("1muCR_withTau",0);
				/*if(m_doIncludeTaus) m_histoCountSF->Fill("2muCR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("2muCR_withTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("2elCR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("2elCR_withTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("PhJetCR_noTau",0);
				if(m_doIncludeTaus) m_histoCountSF->Fill("PhJetCR_withTau",0);*/
			
				m_mapEleSF[thisSyst]=m_histoEleSF;
				m_mapMuSF[thisSyst]=m_histoMuSF;
				if(m_doIncludeTaus) m_mapTauSF[thisSyst]=m_histoTauSF;
				m_mapCountSF[thisSyst]=m_histoCountSF;
			}
			
			// *************************************************************************


		}

		//if(!doMonophoton){  //Systematics reorganization
		   // save, in the nominal tree, the individual baseline-level lepton and photon SFs
		   if (m_isMC && isNominal) {
				std::vector<std::string> forbidden = getTokens(forbidden_syst, ",");
				for (auto tmp_syst : m_objTool->getSystInfoList()) {
					const TString tmp_systName = tmp_syst.systset.name();
					Bool_t keepThis(kTRUE);
					for (const auto& badWord : forbidden) {
						if (tmp_systName.Contains(badWord)) {
							keepThis = kFALSE;
							break;
						}
					} // loop over forbidden systematics

					if(!doMonophoton){
						if (keepThis) {
							/*if (m_doIncludeTaus && tmp_systName.Contains("TAU_EFF")){
								m_cand[thisSyst].tau["tau_baseline"].tau_SF_Sys[tmp_systName].clear();
							}*/
							if (tmp_systName.Contains("EL_EFF")){
								m_cand[thisSyst].el["el_baseline"].el_SF_Sys[tmp_systName].clear();
							}
							if (tmp_systName.Contains("MUON_EFF")){
								m_cand[thisSyst].mu["mu_baseline"].mu_SF_Sys[tmp_systName].clear();
							}
							if (tmp_systName.Contains("PH_EFF")){
								m_cand[thisSyst].ph["ph_baseline"].ph_SF_Sys[tmp_systName].clear();
						 	}
			  			}
					}
					else if (doMonophoton && cutSystematics){   //Systematics reorganization
						if (keepThis) {
							if (m_doIncludeTaus && ( tmp_systName.Contains("TAU_EFF")||tmp_systName.Contains("TAUS_TRUEELECTRON_EFF"))){
								m_cand[thisSyst].evt.tau_SF_Sys[tmp_systName].clear();
							}
							if (tmp_systName.Contains("EL_EFF")){
								m_cand[thisSyst].evt.el_SF_Sys[tmp_systName].clear();
							}
							if (tmp_systName.Contains("MUON_EFF")){
								m_cand[thisSyst].evt.mu_SF_Sys[tmp_systName].clear();
							}
							if (tmp_systName.Contains("PH_EFF")){
								m_cand[thisSyst].evt.ph_SF_Sys[tmp_systName].clear();
						 	}
							//if (tmp_systName.Contains("PH_EFF")){
								//m_cand[thisSyst].evt.baseline_ph_SF_Sys[tmp_systName].clear();
						 	//}
							if (tmp_systName.Contains("JET_JvtEfficiency")){
								m_cand[thisSyst].evt.jet_SF_Sys[tmp_systName].clear();
						 	}
							if (tmp_systName.Contains("PRW_DATASF")){
								m_cand[thisSyst].evt.pu_weight_Sys[tmp_systName].clear();
						 	}
							/*if (tmp_systName.Contains("GEN")){
								m_cand[thisSyst].evt.mconly_weight_Sys[tmp_systName].clear();
						 	}*/


			  			}
					}
				}
		   }
		//}

      // trim non-nominal trees (might be redundant)
      m_cand[thisSyst].setDoMonophoton(doMonophoton);   //set doMonophoton variable in Output classes (default value = kFALSE)
      m_cand[thisSyst].setIsData(!m_isMC);   //set isData variable in Output classes (default value = kFALSE)
      m_cand[thisSyst].setDoTrim(trim);
      m_cand[thisSyst].attachToTree(m_tree[thisSyst]);

   }






   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: execute()
{
   // Here you do everything that needs to be done on every single
   // events, e.g. read input variables, apply cuts, and fill
   // histograms and trees.  This is where most of your actual analysis
   // code will go.

   // set type of return code you are expecting
   // (add to top of each function once)
   ANA_CHECK_SET_TYPE(EL::StatusCode);


   if ((m_eventCounter % 100) == 0){
		ANA_MSG_INFO("Event number = " << m_eventCounter);

	}



	if(doMonophoton&&m_doTheoSystematics&&m_isMC){ //theo syst  (for a cross-check of the sum of weights... )
		int id=0;
		for (auto weight : m_weightTool->getWeightNames()) {
			/*if (id>0){*/m_histoEventCountNonNominal_PMG->Fill(weight.c_str(), m_weightTool->getWeight(weight));//}
			//id++;   //check weight  
		}
	}


   m_eventCounter++;
   // loop on systematic variations
   Bool_t isFirstIteration = kTRUE; // first iteration is nominal (strictly needed!)

   for (const auto& systInfo : m_sysList) {
      const TString systName = systInfo.systset.name();
		if((!cutSystematics && !systName.Contains("TAUS_TRUEELECTRON_EFF_")&&!systName.Contains("TAU_EFF")) || (cutSystematics && !systName.Contains("_EFF_")&&!systName.Contains("JET_JvtEfficiency")&&!systName.Contains("PRW_DATASF_"))){   // cut unnecessary systematics trees systematics reorganization
		   if (isFirstIteration) {
		      m_content_nominal.isNominal = kTRUE;
		   } else {
		      m_content_current = m_content_nominal; // copy from nominal
		      m_content_current.isNominal = kFALSE;
		   }
		  
		   Analysis::ContentHolder &content = (isFirstIteration) ? m_content_nominal : m_content_current;

		   analyzeEvent(content, systInfo, m_cand[systName]);

		   isFirstIteration = kFALSE;
		}
   }

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: postExecute()
{
   // Here you do everything that needs to be done after the main event
   // processing.  This is typically very rare, particularly in user
   // code.  It is mainly used in implementing the NTupleSvc.
   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: finalize()
{
   // This method is the mirror image of initialize(), meaning it gets
   // called after the last event has been processed on the worker node
   // and allows you to finish up any objects you created in
   // initialize() before they are written to disk.  This is actually
   // fairly rare, since this happens separately for each worker node.
   // Most of the time you want to do your post-processing on the
   // submission node after all your histogram outputs have been
   // merged.  This is different from histFinalize() in that it only
   // gets called on worker nodes that processed input events.

   ANA_MSG_INFO("in finalize");

   ANA_MSG_INFO("Number of processed events = " << m_eventCounter);

	if(doMonophoton&&m_doTheoSystematics&&m_isMC){ //theo syst  (change labels of the histo of sum of weights from the bookkeepers (no check on the order...))
		int id=0;
		for (auto weight : m_weightTool->getWeightNames()) {
			ANA_MSG_INFO(id<<"  "<<weight);
			/*if (id>0){*/m_histoEventCountNonNominal->GetXaxis()->SetBinLabel(id+1,weight.c_str());//} +1 modificato
			//id++; //check weight
		}
	}

   return EL::StatusCode::SUCCESS;
}



EL::StatusCode MonoJet :: histFinalize()
{
   // This method is the mirror image of histInitialize(), meaning it
   // gets called after the last event has been processed on the worker
   // node and allows you to finish up any objects you created in
   // histInitialize() before they are written to disk.  This is
   // actually fairly rare, since this happens separately for each
   // worker node.  Most of the time you want to do your
   // post-processing on the submission node after all your histogram
   // outputs have been merged.  This is different from finalize() in
   // that it gets called on all worker nodes regardless of whether
   // they processed input events.

   m_cutFlow.printUnw();
   m_cutFlow.print();

   TH1F *cflow_hist = m_cutFlow.createTH1F();
   TH1F *cflow_hist_unw = m_cutFlow.createTH1Fraw();
   wk()->addOutput(cflow_hist);
   wk()->addOutput(cflow_hist_unw);


   return EL::StatusCode::SUCCESS;
}

EL::StatusCode MonoJet :: getMCcampaignInfo(TString lumicalc_file,  const xAOD::EventInfo evtInfo, std::string amiTag, std::vector<std::string> &prw_lumicalc, TString &mc_campaign)
{

   //Set mc campaign depending on run number and amiTag
   uint32_t runNum = evtInfo.runNumber();
   ANA_MSG_INFO("getMCcampaignInfo(): found MC run number: " << runNum);
   ANA_MSG_INFO("getMCcampaignInfo(): found amiTag: " << amiTag);

   if (runNum == 284500) mc_campaign = "mc16a";
   else if (runNum == 300000) {
     if ( (amiTag.find("r10201") != std::string::npos) || (amiTag.find("r11037")!=std::string::npos)) mc_campaign = "mc16d";
      else mc_campaign = "mc16c";
   } else if (runNum == 310000) {
     mc_campaign = "mc16e";
   } else {
      ANA_MSG_ERROR("getMCcampaignInfo(): unrecognized MC run number, " << runNum << "! Impossible to autocongigure PRW. Aborting.");
      return StatusCode::FAILURE;
   }

   ANA_MSG_INFO("getMCcampaignInfo(): found MC campaign: " << mc_campaign);
   std::vector<std::string> tmp_prw_lumicalc = getTokens(lumicalc_file, ",");
   for (uint i = 0;  i < tmp_prw_lumicalc.size(); i++) {
      TString tmp_path = tmp_prw_lumicalc.at(i);
      if (mc_campaign == "mc16a") {
	if (tmp_path.Contains("data16_13TeV") || tmp_path.Contains("data15_13TeV")) prw_lumicalc.push_back(tmp_prw_lumicalc.at(i));
      } else if (mc_campaign == "mc16c" || mc_campaign == "mc16d") {
         if (tmp_path.Contains("data17_13TeV")) prw_lumicalc.push_back(tmp_prw_lumicalc.at(i));
      } else if (mc_campaign == "mc16e") {
	if (tmp_path.Contains("data18_13TeV")) prw_lumicalc.push_back(tmp_prw_lumicalc.at(i));
      } else prw_lumicalc.push_back(tmp_prw_lumicalc.at(i));
   }

   for (uint i = 0;  i < prw_lumicalc.size(); i++) ANA_MSG_INFO("lumicalc to be added: " << prw_lumicalc.at(i));
   return EL::StatusCode::SUCCESS;
}


EL::StatusCode MonoJet :: readConfig()
{
   const std::string readFrom = PathResolverFindCalibFile(config_file.Data());
   ANA_MSG_INFO("Reading from file \"" << readFrom << "\"");
   TEnv env;
   if (env.ReadFile(readFrom.c_str(), kEnvAll) != 0) {
      ANA_MSG_ERROR("Unable to read configuration file from PathResolverFindCalibFile of input \"" << config_file << "\"");
      return EL::StatusCode::FAILURE;
   }

   ST_config_file = env.GetValue("MJ.ST_config_file", "EMPTY");
   PrwActualMu2017File =  env.GetValue("MJ.prw_file_actualMU2017", "EMPTY");
   PrwActualMu2018File =  env.GetValue("MJ.prw_file_actualMU2018", "EMPTY");
   prw_file = env.GetValue("MJ.prw_file", "EMPTY");
   lumicalc_file = env.GetValue("MJ.lumicalc_file", "EMPTY");
   GRL_file = env.GetValue("MJ.GRL_file", "EMPTY");
   //   MC_campaign = env.GetValue("MJ.MC_campaign", "EMPTY");
   forbidden_syst = env.GetValue("MJ.forbidden_syst", "");
   trigger_list = env.GetValue("MJ.trigger_list", "");
   ptSkim = env.GetValue("MJ.ptSkim", 0);
   metSkim = env.GetValue("MJ.metSkim", 0);
   ptSkimForSyst = env.GetValue("MJ.ptSkimForSyst", 0);
   metSkimForSyst = env.GetValue("MJ.metSkimForSyst", 0);

   return EL::StatusCode::SUCCESS;
}


EL::StatusCode MonoJet :: analyzeEvent(Analysis::ContentHolder &content, const ST::SystInfo &systInfo, Analysis::Output &cand)
{
   xAOD::TEvent *event = wk()->xaodEvent();

   // needed!

   if(content.isNominal) {ANA_CHECK(m_objTool->ApplyPRWTool());} // ? WARNING Attempting to run pileup reweighting, but it has already been run with prefix "" - returning

   // event selection using a given systematic variation
   // event analysis, from object getting to saving-on-disk, is performed here

   // determine kind of systematic uncertainty
   const Bool_t syst_affectsElectrons = ST::testAffectsObject(xAOD::Type::Electron, systInfo.affectsType);
   const Bool_t syst_affectsMuons     = ST::testAffectsObject(xAOD::Type::Muon,     systInfo.affectsType);
   const Bool_t syst_affectsJets      = ST::testAffectsObject(xAOD::Type::Jet,      systInfo.affectsType);
   const Bool_t syst_affectsPhotons   = ST::testAffectsObject(xAOD::Type::Photon,   systInfo.affectsType);
   const Bool_t syst_affectsBTags      = ST::testAffectsObject(xAOD::Type::BTag,     systInfo.affectsType); 
   const Bool_t syst_affectsTaus = ST::testAffectsObject(xAOD::Type::Tau, systInfo.affectsType);

   // apply systematic uncertainty
   const CP::SystematicSet& sys = systInfo.systset;
   if (m_objTool->applySystematicVariation(sys) != CP::SystematicCode::Ok) {
      ANA_MSG_ERROR("Cannot configure SUSYTools for systematic var. " << sys.name().c_str());
      return EL::StatusCode::FAILURE;
   }

   // determine which objects should be retrieved
   //ADD in ContentHolder?
   const Bool_t doElectrons = (syst_affectsElectrons || content.isNominal);
   const Bool_t doMuons = (syst_affectsMuons || content.isNominal);
   const Bool_t doJets = (syst_affectsJets || content.isNominal);
   const Bool_t doPhotons = (syst_affectsPhotons || content.isNominal);
   const Bool_t doBTags = (syst_affectsBTags || content.isNominal);
   const Bool_t doTaus = (syst_affectsTaus || content.isNominal) && m_doIncludeTaus;
   const Bool_t doMET = kTRUE;
   const Bool_t doOR = (doElectrons || doMuons || doJets || doPhotons ||  doBTags || doElectrons || doTaus || doMET); 
 


   // our isolation decorators, used to fill our tree
   //static SG::AuxElement::Decorator<char> dec_pass_loosetrackonly("pass_loosetrackonly");
   static SG::AuxElement::Decorator<float> dec_new_d0("new_d0");
   static SG::AuxElement::Decorator<float> dec_new_d0sig("new_d0sig");
   static SG::AuxElement::Decorator<float> dec_new_z0("new_z0");
   static SG::AuxElement::Decorator<float> dec_new_z0sig("new_z0sig");


   // Initializing OutputTau decorators:   
   static SG::AuxElement::Decorator<int> dec_is_veryloose("is_veryloose");   
   static SG::AuxElement::Decorator<int> dec_is_loose("is_loose");
   static SG::AuxElement::Decorator<int> dec_is_medium("is_medium");
   static SG::AuxElement::Decorator<int> dec_is_tight("is_tight");


   // retrieve the eventInfo object from the event store
   content.eventInfo = nullptr;
   ANA_CHECK(evtStore()->retrieve(content.eventInfo, "EventInfo"));

   // sanity check
   if (m_isMC != content.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) {
      ANA_MSG_FATAL("Mismatch between isMC (" << m_isMC << ") and xAOD::EventInfo::IS_SIMULATION (" << xAOD::EventInfo::IS_SIMULATION << ")");
      return EL::StatusCode::FAILURE;
   }

   // print out run and event number from retrieved object
   ANA_MSG_VERBOSE("in execute, runNumber = " << content.eventInfo->runNumber() << ", eventNumber = " << content.eventInfo->eventNumber());

   if (!m_isMC) { // it's data!
      if (!m_grl->passRunLB(*content.eventInfo)) {
         //ANA_MSG_INFO("drop event: GRL");
         return EL::StatusCode::SUCCESS; // go to next event
      }
   } // end if not MC

   //-- VERTICES --
   content.vertices = nullptr;
    
   if (!event->retrieve(content.vertices, "PrimaryVertices").isSuccess()) {
      ANA_MSG_ERROR("Failed to retrieve PrimaryVertices container");
      return EL::StatusCode::FAILURE;
   }

   //-- JETS --
   if (doJets) {
      // Standard jets
      content.jets = nullptr;
      content.jetsAux = nullptr;
      content.allJets.clear(SG::VIEW_ELEMENTS);
      // get jet container of interest
      ANA_CHECK(m_objTool->GetJets(content.jets, content.jetsAux, kTRUE)); // note that recordSG is needed
      for (auto jet : *content.jets) {
         static SG::AuxElement::Accessor<char> acc_baseline("baseline");
         if (acc_baseline(*jet) == 1) {
             content.allJets.push_back(jet);
         }
      }
      content.allJets.sort(&MonoJetAnalysis::comparePt);  //sort by Pt
   }


   //-- MUONS --
   if (doMuons) {
      content.muons = nullptr;
      content.muonsAux = nullptr;
      content.allMuons.clear(SG::VIEW_ELEMENTS);
      ANA_CHECK(m_objTool->GetMuons(content.muons, content.muonsAux, kTRUE));
      for (auto mu : *content.muons) {
         static SG::AuxElement::Accessor<char> acc_baseline("baseline");
         if (acc_baseline(*mu) == 1) {
            // check isolation
            //dec_pass_loosetrackonly(*mu) = (m_leptonIsolationLooseTrackOnly->accept(*mu));
            // do our own d0 and z0 corrections
            const xAOD::TrackParticle *thisTrack = mu->primaryTrackParticle();
            dec_new_d0(*mu) = thisTrack->d0();
            dec_new_d0sig(*mu) = xAOD::TrackingHelpers::d0significance(thisTrack, content.eventInfo->beamPosSigmaX(), content.eventInfo->beamPosSigmaY(), content.eventInfo->beamPosSigmaXY());
            const xAOD::Vertex* pv = m_objTool->GetPrimVtx();
            const Float_t primvertex_z = pv ? pv->z() : 0;
            dec_new_z0(*mu) = thisTrack->z0() + thisTrack->vz() - primvertex_z;
            dec_new_z0sig(*mu) = TMath::Sqrt(thisTrack->definingParametersCovMatrix()(1, 1));
            content.allMuons.push_back(mu);
         }
      }
      content.allMuons.sort(&MonoJetAnalysis::comparePt);
   }


   //-- ELECTRONS --
   if (doElectrons) {
      content.electrons = nullptr;
      content.electronsAux = nullptr;
      content.allElectrons.clear(SG::VIEW_ELEMENTS);
      ANA_CHECK(m_objTool->GetElectrons(content.electrons, content.electronsAux, kTRUE));
      
      for (auto el : *content.electrons) {
         static SG::AuxElement::Accessor<char> acc_baseline("baseline");

         if (acc_baseline(*el) == 1) {

            // check isolation
            //dec_pass_loosetrackonly(*el) = (m_leptonIsolationLooseTrackOnly->accept(*el));

            // do our own d0 and z0 corrections
            const xAOD::TrackParticle *thisTrack = el->trackParticle();
            dec_new_d0(*el) = thisTrack->d0();
            dec_new_d0sig(*el) = xAOD::TrackingHelpers::d0significance(thisTrack, content.eventInfo->beamPosSigmaX(), content.eventInfo->beamPosSigmaY(), content.eventInfo->beamPosSigmaXY());
            const xAOD::Vertex* pv = m_objTool->GetPrimVtx();
            const Float_t primvertex_z = pv ? pv->z() : 0;
            dec_new_z0(*el) = thisTrack->z0() + thisTrack->vz() - primvertex_z;
            dec_new_z0sig(*el) = TMath::Sqrt(thisTrack->definingParametersCovMatrix()(1, 1));

	    		content.allElectrons.push_back(el);
	 		}
      }
      content.allElectrons.sort(&MonoJetAnalysis::comparePt);
   }


   //-- PHOTONS --
   if (doPhotons) {
      content.photons = nullptr;
      content.photonsAux = nullptr;
      content.allPhotons.clear(SG::VIEW_ELEMENTS);
      ANA_CHECK(m_objTool->GetPhotons(content.photons, content.photonsAux, kTRUE));

      for (auto ph : *content.photons) {
         static SG::AuxElement::Accessor<char> acc_baseline("baseline");
         if (acc_baseline(*ph) == 1) {
            content.allPhotons.push_back(ph);
         }
      }
      content.allPhotons.sort(&MonoJetAnalysis::comparePt);
   }


   //-- TAUS --
   content.taus = nullptr;
   content.tausAux = nullptr;
   content.allTaus.clear(SG::VIEW_ELEMENTS);

   if (m_doIncludeTaus) {
      ANA_CHECK(m_objTool->GetTaus(content.taus, content.tausAux, kTRUE));

      for (auto tau : *content.taus) {

         dec_is_veryloose(*tau) = -999; // TODO: update convention to 0?
         dec_is_loose(*tau) = -999; 
         dec_is_medium(*tau) = -999;
         dec_is_tight(*tau) = -999;

         static SG::AuxElement::Accessor<char> acc_baseline("baseline");

         if (acc_baseline(*tau) == 1) {

            // Check BDT working point
            dec_is_veryloose(*tau) = m_tauVeryLoose->accept(*tau);
            dec_is_loose(*tau) = m_tauLoose->accept(*tau);
            dec_is_medium(*tau) = m_tauMedium->accept(*tau);
            dec_is_tight(*tau) = m_tauTight->accept(*tau);

            content.allTaus.push_back(tau);
         }

      }
      content.allTaus.sort(&MonoJetAnalysis::comparePt);
   }


   //Good objects containers clear
   content.goodMuons.clear(SG::VIEW_ELEMENTS);
   content.baselineMuons.clear(SG::VIEW_ELEMENTS);
   content.goodElectrons.clear(SG::VIEW_ELEMENTS);
   content.baselineElectrons.clear(SG::VIEW_ELEMENTS);
   content.probeElectrons.clear(SG::VIEW_ELEMENTS);     //Monophoton
   content.goodJets.clear(SG::VIEW_ELEMENTS);     
   //Monophoton
   //JVT test content.noJVTJets.clear(SG::VIEW_ELEMENTS);
   content.goodPhotons.clear(SG::VIEW_ELEMENTS);
   content.baselinePhotons.clear(SG::VIEW_ELEMENTS);
   content.goodTaus.clear(SG::VIEW_ELEMENTS);
   content.baselineTaus.clear(SG::VIEW_ELEMENTS);

   ///////////
   // Overlap removal and good object selection
   ///////////
   if (doOR) {
     if (m_doIncludeTaus) {ANA_CHECK(m_objTool->OverlapRemoval(content.electrons, content.muons, content.jets, content.photons, content.taus));}
     else {
			ANA_CHECK(m_objTool->OverlapRemoval(content.electrons, content.muons, content.jets, content.photons));
		}

      static SG::AuxElement::Accessor<char> acc_bad("bad");
      static SG::AuxElement::Accessor<char> acc_signal("signal");
      static SG::AuxElement::Accessor<char> acc_passOR("passOR");


      //TODO: dirty jets need to be reconfigured

      for (auto thisJet : content.allJets) {
         // ask good jets to be SUSY signal jets, i.e.
         //   - baseline (aka considered for overlap removal)
         //   - passOR (i.e. pass overlap removal)
         //   - with pT > 30 GeV, |eta| < 2.8
         // and also
         //   - not bad (jet cleaning)
         if (acc_bad(*thisJet) == 0 && acc_passOR(*thisJet) == 1 && acc_signal(*thisJet) == 1 ) {
            content.goodJets.push_back(thisJet);
         }

         /* JVT test if (acc_bad(*thisJet) == 0 && acc_passOR(*thisJet) == 1) {
            content.noJVTJets.push_back(thisJet);
	    if( acc_signal(*thisJet) == 1) {
               content.goodJets.push_back(thisJet);
	    }
         }*/


      }

      // good muon selection (i.e. muons for veto)
      for (auto thisMuon : content.allMuons) {
         static SG::AuxElement::Accessor<char> acc_cosmic("cosmic");
         if (acc_passOR(*thisMuon) == 1 && acc_cosmic(*thisMuon) == 0) {

            if (acc_bad(*thisMuon) == 0) {
               content.baselineMuons.push_back(thisMuon);

               if (acc_signal(*thisMuon) == 1) content.goodMuons.push_back(thisMuon); // muons for CRs
            }
         }
      }

      // good electron selection (i.e. electrons for veto)
      for (auto thisElectron : content.allElectrons) {
         if (acc_passOR(*thisElectron) == 1) {
				if(  fabs( thisElectron->caloCluster()->eta() ) > 1.37   and    fabs( thisElectron->caloCluster()->eta() ) < 1.52  ) continue;
            content.baselineElectrons.push_back(thisElectron);
            if (acc_signal(*thisElectron) == 1) {
	       		content.goodElectrons.push_back(thisElectron); // electrons for CRs
	      		if(doMonophoton){
	          		if(thisElectron->pt()>150000) content.probeElectrons.push_back(thisElectron);
	       		}
	   		}
         }
      }

      // good tau selection (i.e. taus for veto)
      if (m_doIncludeTaus) {
         for (auto thisTau : content.allTaus) {
            if (acc_passOR(*thisTau) == 1) {
               content.baselineTaus.push_back(thisTau);
               if (acc_signal(*thisTau) == 1) {
		 				content.goodTaus.push_back(thisTau); // taus for CRs
	       		}
	    		}
         }
      }

      // good photon selection
      for (auto thisPhoton : content.allPhotons) {
         if (acc_passOR(*thisPhoton) == 1) {
            content.baselinePhotons.push_back(thisPhoton);
            if (acc_signal(*thisPhoton) == 1) {
	      		content.goodPhotons.push_back(thisPhoton);
            }
         }
      }

   } //end Overlap Removal


   ///////////
   // MET, with muons (real MET), TST soft term
   ///////////
   const TString met_prefix = systInfo.systset.name() + "_";

   getMET(content.met_tst,
          content.met_tstAux,
          content.jets, // use all objects (before OR and after corrections) for MET utility
          content.electrons,
          content.muons,
          content.photons,
          content.taus,
          kTRUE, // do TST
          kTRUE, // do JVT
          nullptr // invisible particles
         );


   ///////////
   // MET, with muons, CST soft term   (monophoton)
   //////////
	getMET(content.met_cst,
          content.met_cstAux,
          content.jets, // use all objects (before OR and after corrections) for MET utility
          content.electrons,
          content.muons,
          content.photons,
          content.taus,
          kFALSE, // not TST
          kFALSE, // not JVT
          nullptr // invisible particles
          );

    ///////////
    // MET, invisible muons, TST soft term
    //////////
	getMET(content.met_tst_nomuon,
			content.met_tst_nomuonAux,
			content.jets, // use all objects (before OR and after corrections) for MET utility
			content.electrons,
			content.muons,
			content.photons,
			content.taus,
			kTRUE, // do TST
			kTRUE, // do JVT
			nullptr // invisible particles
			);
    // create sum of of muon pts
	{
		if(!doMonophoton){
			Float_t px = 0;
			Float_t py = 0;
			for (auto thisMuon : content.goodMuons) {
				px += thisMuon->pt() * TMath::Cos(thisMuon->phi());
				py += thisMuon->pt() * TMath::Sin(thisMuon->phi());
			}
			//TVector2 mu_momenta(px,py);
			const Float_t mpx = (*content.met_tst_nomuon)["Final"]->mpx();
			const Float_t mpy = (*content.met_tst_nomuon)["Final"]->mpy();
			(*content.met_tst_nomuon)["Final"]->setMpx(mpx + px);
			(*content.met_tst_nomuon)["Final"]->setMpy(mpy + py);
		}
		else if (doMonophoton){
			Float_t px = (*content.met_tst)["Muons"]->mpx();
			Float_t py = (*content.met_tst)["Muons"]->mpy();
			const Float_t mpx = (*content.met_tst_nomuon)["Final"]->mpx();
			const Float_t mpy = (*content.met_tst_nomuon)["Final"]->mpy();
			(*content.met_tst_nomuon)["Final"]->setMpx(mpx - px);
			(*content.met_tst_nomuon)["Final"]->setMpy(mpy - py);	
		}
			
	}

   
    ///////////
    // MET, invisible good electrons, TST soft term
    ///////////
	getMET(content.met_tst_noelectron,
			content.met_tst_noelectronAux,
			content.jets, // use all objects (before OR and after corrections) for MET utility
			content.electrons,
			content.muons,
			content.photons,
			content.taus,
			kTRUE, // do TST
			kTRUE, // do JVT
			nullptr // invisible particles
			);
    // create sum of of electron pts
	{

		if(!doMonophoton){
			Float_t px = 0;
			Float_t py = 0;
			for (auto thisElectron : content.goodElectrons) {
				px += thisElectron->pt() * TMath::Cos(thisElectron->phi());
				py += thisElectron->pt() * TMath::Sin(thisElectron->phi());
			}
			//TVector2 mu_momenta(px,py);
			const Float_t mpx = (*content.met_tst_noelectron)["Final"]->mpx();
			const Float_t mpy = (*content.met_tst_noelectron)["Final"]->mpy();
			(*content.met_tst_noelectron)["Final"]->setMpx(mpx + px);
			(*content.met_tst_noelectron)["Final"]->setMpy(mpy + py);
		}

		else if (doMonophoton){
			Float_t px = (*content.met_tst)["RefEle"]->mpx();
			Float_t py = (*content.met_tst)["RefEle"]->mpy();
			const Float_t mpx = (*content.met_tst_noelectron)["Final"]->mpx();
			const Float_t mpy = (*content.met_tst_noelectron)["Final"]->mpy();
			(*content.met_tst_noelectron)["Final"]->setMpx(mpx - px);
			(*content.met_tst_noelectron)["Final"]->setMpy(mpy - py);	
		}
    }

    ///////////
    // MET for probe ele, invisible good electrons, TST soft term
    ///////////
	if (doMonophoton){
		getMET(content.met_tst_noelectron_probe,
				content.met_tst_noelectron_probeAux,
				content.jets, // use all objects (before OR and after corrections) for MET utility
				content.electrons,
				content.muons,
				content.photons,
				content.taus,
				kTRUE, // do TST
				kTRUE, // do JVT
				nullptr // invisible particles
				);
		 // create sum of of electron pts
		{

				Float_t px = (*content.met_tst)["RefEle"]->mpx();
				Float_t py = (*content.met_tst)["RefEle"]->mpy();
				Float_t probe_px = (content.goodElectrons.size()>0) ? content.goodElectrons[0]->pt() * TMath::Cos(content.goodElectrons[0]->phi()) : 0; 
				Float_t probe_py = (content.goodElectrons.size()>0) ? content.goodElectrons[0]->pt() * TMath::Sin(content.goodElectrons[0]->phi()) : 0;
				const Float_t mpx = (*content.met_tst_noelectron_probe)["Final"]->mpx();
				const Float_t mpy = (*content.met_tst_noelectron_probe)["Final"]->mpy();

				(*content.met_tst_noelectron_probe)["Final"]->setMpx(mpx - px + probe_px);
				(*content.met_tst_noelectron_probe)["Final"]->setMpy(mpy - py + probe_py);	
			}
    }
      
    ///////////
    // MET, invisible photons, TST soft term
    ///////////
    getMET(content.met_tst_nophoton,
           content.met_tst_nophotonAux,
           content.jets, // use all objects (before OR and after corrections) for MET utility
           content.electrons,
           content.muons,
           content.photons,
           content.taus,
           kTRUE, // do TST
           kTRUE, // do JVT
           nullptr // invisible particles
           );
    // create sum of of electron pts
	{
		Float_t px = 0;
		Float_t py = 0;

		for (auto thisPhoton : content.goodPhotons) {   
			px += thisPhoton->pt() * TMath::Cos(thisPhoton->phi());
			py += thisPhoton->pt() * TMath::Sin(thisPhoton->phi());
		}
			  
		//TVector2 mu_momenta(px,py);
		const Float_t mpx = (*content.met_tst_nophoton)["Final"]->mpx();
		const Float_t mpy = (*content.met_tst_nophoton)["Final"]->mpy();
		(*content.met_tst_nophoton)["Final"]->setMpx(mpx + px);
		(*content.met_tst_nophoton)["Final"]->setMpy(mpy + py);
	}
			 
			 
	 ///////////
	 // MET, with muons (real MET), TST soft term
	 ///////////
	 getTrackMET(content.met_track,
				    content.met_trackAux,
				    content.jets, // use all objects (before OR and after corrections) for MET utility
				    content.electrons,
				    content.muons
				    );
    
    
    ///////////
    // MET, truth (if available)
    ///////////
    if (m_isMC) {
        content.met_truth = nullptr;
        
        if (!event->retrieve(content.met_truth, "MET_Truth").isSuccess()) { // retrieve arguments: container type, container key
            ANA_MSG_ERROR("MonoJet::getObjects : Failed to retrieve MET_Truth container");
            return EL::StatusCode::FAILURE;
        }
        
        xAOD::MissingETContainer::const_iterator met_it = content.met_truth->find("NonInt");
        
        if (met_it == content.met_truth->end()) {
            ANA_MSG_ERROR("MonoJet::getObjects : No NonInt inside truth MET container");
            return EL::StatusCode::FAILURE;
        }
    }
    
    
    
    //Cutflow
    Int_t last;
    if(!doMonophoton){
        getLastCutPassed(content, m_last);
        last = m_last;
    }
    else if(doMonophoton){
        getLastCutPassedMonophoton(content, m_last_monophoton); 
        last = m_last_monophoton;                    // MonoPhotonCuts::CutID m_last_photon
    }
    if (content.isNominal) m_cutFlow.addCutCounter(last, (m_isMC) ? content.eventInfo->mcEventWeight() : 1.0); // TODO: add also pileup
    




    // tree filling
    const Double_t ptSkimToUse = (content.isNominal) ? ptSkim : ptSkimForSyst;
    const Double_t metSkimToUse = (content.isNominal) ? metSkim : metSkimForSyst;
    TVector2 met_nomuon_to_use = TVector2((*content.met_tst_nomuon)["Final"]->mpx(), (*content.met_tst_nomuon)["Final"]->mpy());
    TVector2 met_noelectron_to_use = TVector2((*content.met_tst_noelectron)["Final"]->mpx(), (*content.met_tst_noelectron)["Final"]->mpy());
    TVector2 met_nophoton_to_use = TVector2((*content.met_tst_nophoton)["Final"]->mpx(), (*content.met_tst_nophoton)["Final"]->mpy());
    
    Bool_t saveMe = kFALSE;
    
    if(!doMonophoton){
        const MonoJetCuts::CutID lastSkimToUse = (content.isNominal) ? MonoJetCuts::vertex : MonoJetCuts::BCH_cleaning;
        saveMe = (m_last >= lastSkimToUse
                  && (content.goodJets.size() > 0 && content.goodJets.size() < 5.)
                  && content.goodJets[0]->pt() > ptSkimToUse
                  && (met_nomuon_to_use.Mod() > metSkimToUse || met_noelectron_to_use.Mod() > metSkimToUse || met_nophoton_to_use.Mod() > metSkimToUse));
        if (saveMe) fillTree(content, cand, m_last);
    }
    else if(doMonophoton) {
        const MonoPhotonCuts::CutID lastSkimToUse = (content.isNominal) ? MonoPhotonCuts::BCH_cleaning : MonoPhotonCuts::BCH_cleaning;
        if(!m_isMC || (m_isMC&&!m_doSystematics&&m_isMC_Wenu_Zee)){
            saveMe = (m_last_monophoton >= lastSkimToUse
                      && ((content.baselinePhotons.size()>0 && content.baselinePhotons[0]->pt() > ptSkimToUse)
                      || (content.goodElectrons.size()>0 && content.probeElectrons.size()>0)));
        }
        else {
           saveMe = (m_last_monophoton >= lastSkimToUse
                      && content.baselinePhotons.size()>0
                      && content.baselinePhotons[0]->pt() > ptSkimToUse
                      && (met_nomuon_to_use.Mod() > metSkimToUse || met_noelectron_to_use.Mod() >  metSkimToUse ));
        }
        if (saveMe) fillTreeMonophoton(content, cand, m_last_monophoton, sys);
    }
    
    return EL::StatusCode::SUCCESS;
    

}


EL::StatusCode MonoJet::getMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer * jet, xAOD::ElectronContainer * el, xAOD::MuonContainer * mu, xAOD::PhotonContainer * ph, xAOD::TauJetContainer * tau, Bool_t doTST, Bool_t doJVT, xAOD::IParticleContainer * invis)
{
   met = std::make_shared<xAOD::MissingETContainer>();
   metAux = std::make_shared<xAOD::MissingETAuxContainer>();
   met->setStore(metAux.get());

   ANA_CHECK(m_objTool->GetMET(*met,
                               jet, // use all objects (before OR and after corrections) for MET utility
                               el,
                               mu,
                               ph,
                               tau,
                               doTST,
                               doJVT,
                               invis// invisible particles
                              ));

   xAOD::MissingETContainer::const_iterator met_it = met->find("Final");

   if (met_it == met->end()) {
      ANA_MSG_ERROR("MonoJet::getMET : No RefFinal inside MET container");
      return EL::StatusCode::FAILURE;
   }


   return EL::StatusCode::SUCCESS;
}


int MonoJet::getRun(Int_t run, TString inputFileName)
{
  Int_t tmp_run = run;
  if(MC_campaign == "mc16e" && run>=366001 && run<= 366008){
    if(inputFileName.Contains("BFilter"))tmp_run=run+9;
    else if(inputFileName.Contains("CFilterBVeto"))tmp_run=run+18;
    else if(inputFileName.Contains("CVetoBVeto"))tmp_run=run+27;
  }
  return tmp_run;
}

bool MonoJet :: isPassMETTrigger(int rn){ //-Check out https://twiki.cern.ch/twiki/bin/viewauth/Atlas/LowestUnprescaled#Data_Periods_AN1 to keep this updated!
  // 2015:
  if(rn<290000) return m_objTool->IsTrigPassed("HLT_xe70_mht");
  // 2016:
  else if(rn>=296939 && rn<=302872) return m_objTool->IsTrigPassed("HLT_xe90_mht_L1XE50");  //  A-D3
  else if(rn>=302919 && rn<=303892) return m_objTool->IsTrigPassed("HLT_xe100_mht_L1XE50");  //  D4-F1
  else if(rn>=303943 && rn<=311481) return m_objTool->IsTrigPassed("HLT_xe110_mht_L1XE50");  //  F2-(open)
  // 2017:
  else if(rn>=325713 && rn<=328393) return m_objTool->IsTrigPassed("HLT_xe110_pufit_L1XE55");  //  B
  else if(rn>=329385 && rn<=330470) return m_objTool->IsTrigPassed("HLT_xe110_pufit_L1XE55");  //  C
  else if(rn>=330857 && rn<=331975) return m_objTool->IsTrigPassed("HLT_xe110_pufit_L1XE55");  //  D1-D5
  else if(rn>=332303 && rn<=341649) return m_objTool->IsTrigPassed("HLT_xe110_pufit_L1XE50");  //  D6-N (check GRL!)
  // 2018:
  else if(rn>=348197 && rn<=349533) return m_objTool->IsTrigPassed("HLT_xe110_pufit_xe70_L1XE50") || m_objTool->IsTrigPassed("HLT_xe120_pufit_L1XE50");  //  A-B
  else if(rn>=349534 && rn<=350066) return m_objTool->IsTrigPassed("HLT_xe110_pufit_xe70_L1XE50") || m_objTool->IsTrigPassed("HLT_xe120_pufit_L1XE50");  //  C-I
  else if(rn>=350067 && rn<=355273) return m_objTool->IsTrigPassed("HLT_xe110_pufit_xe65_L1XE50") || m_objTool->IsTrigPassed("HLT_xe120_pufit_L1XE50");  //  C-I (splitted)
  else if(rn>=355529 && rn<=364292) return m_objTool->IsTrigPassed("HLT_xe110_pufit_xe65_L1XE50") || m_objTool->IsTrigPassed("HLT_xe110_pufit_xe70_L1XE50");  //  K-current (check twiki!)
  return false;
}


void MonoJet :: GetMuonSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys)
{
  m_objTool->GetTotalMuonSFsys(content.allMuons,CP::SystematicSet(Sys),true,false); // only reco
  for (auto mu : content.baselineMuons){ cand.mu["mu_baseline"].add_SFs(*mu,Sys,false); }
}


void MonoJet :: GetElectronSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys)
{
  m_objTool->GetTotalElectronSFsys(content.allElectrons,CP::SystematicSet(Sys),true,true,false,false); // reco, id
  for (auto el : content.baselineElectrons){ cand.el["el_baseline"].add_SFs(*el,Sys,false,false,false); }
}


void MonoJet :: GetPhotonSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys)
{
  m_objTool->GetTotalPhotonSFsys(content.allPhotons,CP::SystematicSet(Sys),true,false); // only reco
  for (auto ph : content.baselinePhotons){ cand.ph["ph_baseline"].add_SFs(*ph,Sys,false); }
}


void MonoJet :: GetTauSFsys(Analysis::ContentHolder &content, Analysis::Output &cand, std::string Sys)
{
  m_objTool->GetTotalTauSFsys(content.allTaus,CP::SystematicSet(Sys),true,false); // only reco
  for (auto tau : content.baselineTaus){ cand.tau["tau_baseline"].add_SFs(*tau, Sys); }
}


EL::StatusCode MonoJet::fillTree(Analysis::ContentHolder &content, Analysis::Output &cand, MonoJetCuts::CutID &last)
{
   xAOD::TEvent *event = wk()->xaodEvent();
   cand.reset();

   // raw event info
   //   cand.evt.run = (m_isMC) ? content.eventInfo->mcChannelNumber() : content.eventInfo->runNumber();
   cand.evt.run = (m_isMC) ? getRun(content.eventInfo->mcChannelNumber(),m_inputFileName) : content.eventInfo->runNumber(); //F.C remove when mc16e Znunu samples will be fixed
   cand.evt.event = (ULong64_t)content.eventInfo->eventNumber();
   cand.evt.lbn = content.eventInfo->lumiBlock();
   cand.evt.bcid = content.eventInfo->bcid();
   cand.evt.averageIntPerXing = content.eventInfo->averageInteractionsPerCrossing();
   cand.evt.actualIntPerXing = content.eventInfo->actualInteractionsPerCrossing();
   cand.evt.corAverageIntPerXing = m_objTool->GetCorrectedAverageInteractionsPerCrossing();
   cand.evt.corActualIntPerXing = m_objTool->GetCorrectedActualInteractionsPerCrossing();
   cand.evt.allmu_tot_SF = (m_isMC) ?  m_objTool->GetTotalMuonSF(content.goodMuons, true, true, "HLT_mu26_imedium_OR_HLT_mu50") : 1.0;

   // trigger
   for (auto &kv : cand.evt.trigger) {
      kv.second = m_objTool->IsTrigPassed(kv.first.Data());
   }

   // MET trigger:
   int runNumber = content.eventInfo->runNumber(); //Valid for both MC and data
   cand.evt.trigger_pass = isPassMETTrigger(runNumber);
   /*

   
   // MET trigger:
   int runNumber = content.eventInfo->runNumber(); //Valid for both MC and data
   cand.evt.trigger_pass = isPassMETTrigger(runNumber);

   cand.evt.trigger_pass = cand.evt.trigger["HLT_xe70"] || // standard 2015
                           cand.evt.trigger["HLT_xe80_tc_lcw_L1XE50"] || cand.evt.trigger["HLT_xe90_mht_L1XE50"] || cand.evt.trigger["HLT_xe100_mht_L1XE50"] || cand.evt.trigger["HLT_xe110_mht_L1XE50"] || cand.evt.trigger["HLT_xe130_mht_L1XE50"] ||//standard 2016
                           cand.evt.trigger["HLT_e24_lhmedium_L1EM20VH"] || cand.evt.trigger["HLT_e60_lhmedium"] || cand.evt.trigger["HLT_e120_lhloose"] || // for CR2e 2015
                           cand.evt.trigger["HLT_e26_lhtight_nod0_ivarloose"] || cand.evt.trigger["HLT_e60_lhmedium_nod0"] || cand.evt.trigger["HLT_e140_lhloose_nod0"]; // for CR2e
   //if (!doIgnorePhotons) cand.evt.trigger_pass |= cand.evt.trigger["HLT_g140_loose"] || // 2015
   //cand.evt.trigger["HLT_g140_loose"] || cand.evt.trigger["HLT_g160_loose"]; // for CR1ph
*/


   // event-level artifacts from SUSYTools
   cand.evt.year = (m_isMC) ? m_objTool->treatAsYear() : 0;

   // event-level artifacts from system flags
   cand.evt.flag_sct = (content.eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error);
   cand.evt.flag_core = (content.eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000);
   cand.evt.flag_bib = content.eventInfo->eventFlags(xAOD::EventInfo::Background) & (1 << 20);
   cand.evt.flag_bib_raw = content.eventInfo->eventFlags(xAOD::EventInfo::Background);

   // vertex information
   cand.evt.n_vx = content.vertices->size(); // absolute number of PV's (i.e. no track cut)
   cand.evt.last = last;

   // MC-only information
   if (m_isMC) {
      // MC weights
      cand.evt.mconly_weight = content.eventInfo->mcEventWeight();
      cand.evt.mconly_weights = content.eventInfo->mcEventWeights();

      // pile-up * MC weights
      cand.evt.pu_weight = 1.0;
      cand.evt.pu_weight *= content.eventInfo->mcEventWeight();
      cand.evt.pu_weight *= m_objTool->GetPileupWeight();
      cand.evt.pu_hash = m_objTool->GetPileupWeightHash();

      // event-level scale factors
      // cand.evt.btag_weight = m_objTool->BtagSF(&content.goodJets);
      cand.evt.jvt_weight = m_objTool->GetTotalJetSF(content.jets, false, true); // btag, jvt

      // PDF information
      const xAOD::TruthEventContainer *truthE(nullptr);
      if (!event->retrieve(truthE, "TruthEvents").isSuccess()) {
         ANA_MSG_ERROR("Failed to retrieve Truth container");
      } else {
         try {
            xAOD::TruthEventContainer::const_iterator truthE_itr = truthE->begin();
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_id1, xAOD::TruthEvent::PDGID1);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_id2, xAOD::TruthEvent::PDGID2);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_x1, xAOD::TruthEvent::X1);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_x2, xAOD::TruthEvent::X2);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_pdf1, xAOD::TruthEvent::PDF1);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_pdf2, xAOD::TruthEvent::PDF2);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_scale, xAOD::TruthEvent::Q);
            
         } catch (SG::ExcBadAuxVar) {
            // ignore this variable, when unavailable
            // (happens often, see
            // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15aKnownIssues
            // )
         }
      }
      
      // truth jet information
      // leading two jets are saved; this is needed for example for QCD MC, where
      // events with ((pt1+pt2)/2)/pt1 < 1.4 are ignored, in order to avoid counting
      // pileup jets as leading jets when combining different slices of leading jet pt
      const xAOD::JetContainer * truthJets(nullptr);
      static Bool_t failedLookingFor(kFALSE); // trick to avoid infinite RuntimeWarning's for EXOT6
      if (!failedLookingFor) {
         if (!event->retrieve(truthJets, "AntiKt4TruthJets").isSuccess()) {
            ANA_MSG_ERROR("MonoJet::analyzeEvent : Failed to access Truth Jets container; not attempting again, truth_jet* variables will be empty");
            failedLookingFor = kTRUE;
         } else {
            //number of truth jets with pt > 20 GeV and OR
            // loop over all truth jets passing basic cuts
            Int_t nTruthJets(0);
            const xAOD::TruthParticleContainer * truthP(0);
            ANA_CHECK(event->retrieve(truthP, "TruthParticles"));
            for (const auto& truthJ_itr : *truthJets) {
               if (truthJ_itr->pt() > 20000. && fabs(truthJ_itr->eta()) < 2.8) {
                  float minDR2 = 9999999.;
                  // for each jet, loop over all electrons and muons passing basic cuts to check if the jets should be counted or not
                  for (const auto& truthP_itr : *truthP) {
                     if (truthP_itr->pt() > 25000. && fabs(truthP_itr->eta()) < 2.5 && truthP_itr->status() == 1 && (abs(truthP_itr->pdgId()) == 11 || abs(truthP_itr->pdgId()) == 13)) {
                        if (xAOD::P4Helpers::isInDeltaR(*truthJ_itr, *truthP_itr, 0.2, true)) {
                           float dR2 = xAOD::P4Helpers::deltaR2(truthJ_itr, truthP_itr, true);
                           if (dR2 < minDR2) {
                              minDR2 = dR2;
                              // stop if we already know the jet shouldn't be counted
                              if (minDR2 < 0.2 * 0.2) break;
                           }
                        }
                     }
                  }
                  if (minDR2 > 0.2 * 0.2) nTruthJets++;
               }
            }
            cand.evt.n_jet_truth = nTruthJets;
            /*
                 if (truthJets->size() > 0) {
                    cand.evt.truth_jet1_pt = (*truthJets)[0]->p4().Pt();
                    cand.evt.truth_jet1_eta = (*truthJets)[0]->p4().Eta();
                    cand.evt.truth_jet1_phi = (*truthJets)[0]->p4().Phi();
                    cand.evt.truth_jet1_m = (*truthJets)[0]->p4().M();
                    if (truthJets->size() > 1) {
                       cand.evt.truth_jet2_pt = (*truthJets)[1]->p4().Pt();
                       cand.evt.truth_jet2_eta = (*truthJets)[1]->p4().Eta();
                       cand.evt.truth_jet2_phi = (*truthJets)[1]->p4().Phi();
                       cand.evt.truth_jet2_m = (*truthJets)[1]->p4().M();
                    }
                 }
            */
         }
      } // already found AntiKt4TruthJets, or never failed yet


      // save truth information for the W/Z boson, and for truth muons
      //      const int mcid = content.eventInfo->mcChannelNumber();
      const xAOD::TruthParticleContainer *truthParticles(nullptr);
      const xAOD::TruthParticleContainer *truthTaus = nullptr;
      const xAOD::TruthParticleContainer *truthMuons = nullptr;
      const xAOD::TruthParticleContainer *truthElectrons = nullptr;
      //const xAOD::TruthParticleContainer *truthPhotons = nullptr;
      ANA_CHECK(event->retrieve(truthParticles, "TruthParticles"));
      if (!m_determinedDerivation) {
         ANA_MSG_INFO("Determining derivation type");
         m_isEXOT5 = event->retrieve(truthMuons, "EXOT5TruthMuons").isSuccess();
         m_determinedDerivation = kTRUE;
         TString isEXOT5 = (m_isEXOT5) ? "YES" : "NO";
         ANA_MSG_INFO("Is it EXOT5? (will trigger access to dedicated truth electron and muon containers): " << isEXOT5);
      }

      static Bool_t failedLookingForFlag(kFALSE);

      if (!failedLookingForFlag) {
         const TString mu_container = (m_isEXOT5) ? "EXOT5TruthMuons" : "TruthMuons";
         if (!event->retrieve(truthMuons, mu_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
            ANA_MSG_WARNING("Failed to retrieve Muons container");
            failedLookingForFlag = kTRUE;
            // return EL::StatusCode::FAILURE;
         }
         if (!m_determinedDerivation) {
            // ANA_MSG_WARNING("Wrong logic: is/isn't EXOT5 should have been determined in the muon part!");
         }
         const TString el_container = (m_isEXOT5) ? "EXOT5TruthElectrons" : "TruthElectrons";
         if (!event->retrieve(truthElectrons, el_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
            ANA_MSG_WARNING("Failed to retrieve Electrons container");
            failedLookingForFlag = kTRUE;
            // return EL::StatusCode::FAILURE;
         }
         const TString tau_container =  "TruthTaus";
         if (!event->retrieve(truthTaus, tau_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
            ANA_MSG_WARNING("Failed to retrieve Taus container");
            failedLookingForFlag = kTRUE;
            // return EL::StatusCode::FAILURE;
         }
      }
      //const TString ph_container = "TruthPhotons";
      //if (!event->retrieve(truthPhotons, ph_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
      //   Error(APP_NAME, "Failed to retrieve Photons container");
      //   return EL::StatusCode::FAILURE;
      //}

      /*
      // needed to study fakes
      for (const auto& part : *truthMuons) {
         cand.evt.truth_mu_pt.push_back(part->pt());
         cand.evt.truth_mu_eta.push_back(part->eta());
         cand.evt.truth_mu_phi.push_back(part->phi());
         cand.evt.truth_mu_m.push_back(part->m());
         cand.evt.truth_mu_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(*part));
         cand.evt.truth_mu_type.push_back(xAOD::TruthHelpers::getParticleTruthType(*part));
      }
      for (const auto& part : *truthElectrons) {
         cand.evt.truth_el_pt.push_back(part->pt());
         cand.evt.truth_el_eta.push_back(part->eta());
         cand.evt.truth_el_phi.push_back(part->phi());
         cand.evt.truth_el_m.push_back(part->m());
         cand.evt.truth_el_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(*part));
         cand.evt.truth_el_type.push_back(xAOD::TruthHelpers::getParticleTruthType(*part));
      }
      for (const auto& part : *truthTaus) {
         cand.evt.truth_tau_pt.push_back(part->pt());
         cand.evt.truth_tau_eta.push_back(part->eta());
         cand.evt.truth_tau_phi.push_back(part->phi());
         cand.evt.truth_tau_m.push_back(part->m());
         cand.evt.truth_tau_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(*part));
         cand.evt.truth_tau_type.push_back(xAOD::TruthHelpers::getParticleTruthType(*part));
      }
      */

      if (truthMuons) {
         const TLorentzVector truth_V_dressed = MonoJetAnalysis::getTruthBosonP4(truthParticles, truthElectrons, truthMuons, truthParticles); // EXOT5 does not have TruthPhotons container

         cand.evt.truth_V_dressed_pt = truth_V_dressed.Pt();
         cand.evt.truth_V_dressed_eta = truth_V_dressed.Eta();
         cand.evt.truth_V_dressed_phi = truth_V_dressed.Phi();
         cand.evt.truth_V_dressed_m = truth_V_dressed.M();

         const TLorentzVector truth_V_bare = MonoJetAnalysis::getTruthBosonP4(truthParticles, truthElectrons, truthMuons, truthParticles, kFALSE); // EXOT5 does not have TruthPhotons container

         cand.evt.truth_V_bare_pt = truth_V_bare.Pt();
         cand.evt.truth_V_bare_eta = truth_V_bare.Eta();
         cand.evt.truth_V_bare_phi = truth_V_bare.Phi();
         cand.evt.truth_V_bare_m = truth_V_bare.M();

         const TLorentzVector truth_V_simple = MonoJetAnalysis::getTruthBosonP4_simple(truthParticles);
         cand.evt.truth_V_simple_pt = truth_V_simple.Pt();
         cand.evt.truth_V_simple_eta = truth_V_simple.Eta();
         cand.evt.truth_V_simple_phi = truth_V_simple.Phi();
         cand.evt.truth_V_simple_m = truth_V_simple.M();
      }


   } //close MC-only information

   /*
   //////////////////
   // composite variables
   //////////////////
   TVector2 the_met(m_met_nomuon_to_use);

   if (content.goodMuons.size() > 0) {
     const xAOD::Muon *thisMuon = content.goodMuons[0];
     cand.evt.munu_mT = TMath::Sqrt(2.0 * thisMuon->pt() * m_met_wmuon_to_use.Mod() * (1 - TMath::Cos(thisMuon->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use))));
     if (content.goodMuons.size() > 1) {
       TLorentzVector v_2mu = thisMuon->p4() + content.goodMuons[1]->p4();
       cand.evt.mumu_pt = v_2mu.Pt();
       cand.evt.mumu_eta = v_2mu.Eta();
       cand.evt.mumu_phi = v_2mu.Phi();
       cand.evt.mumu_m = v_2mu.M();
     } // at least two vetoed muons
      } // at least a vetoed muon
   if (content.goodJets.size() > 1) {
     TLorentzVector v_2jet = content.goodJets[0]->p4() + content.goodJets[1]->p4();
     cand.evt.jj_m = v_2jet.M();

     std::vector<TLorentzVector> hemi = RazorAnalysis::GetHemi(content.goodJets);
     cand.hem["hem"].add(hemi[0]);
     cand.hem["hem"].add(hemi[1]);

     RazorAnalysis::RazorVariables raz = RazorAnalysis::GetRazorVariables(hemi[0], hemi[1], the_met);
     cand.evt.shatR = raz.shatR;
     cand.evt.gaminvR = raz.gaminvR;
     cand.evt.gaminvRp1 = raz.gaminvRp1;
     cand.evt.dphi_BETA_R = raz.dphi_BETA_R;
     cand.evt.dphi_J1_J2_R = raz.dphi_J1_J2_R;
     cand.evt.gamma_Rp1 = raz.gamma_Rp1;
     cand.evt.costhetaR = raz.costhetaR;
     cand.evt.dphi_R_Rp1 = raz.dphi_R_Rp1;
     cand.evt.mdeltaR = raz.mdeltaR;
     cand.evt.cosptR = raz.cosptR;
     cand.evt.costhetaRp1 = raz.costhetaRp1;
      }
   if (content.goodElectrons.size() > 0) {
     TVector2 the_electron_met(the_met);
     cand.evt.enu_mT = TMath::Sqrt(2.0 * content.goodElectrons[0]->pt() * m_met_wmuon_to_use.Mod() * (1 - TMath::Cos(content.goodElectrons[0]->p4().Vect().XYvector().DeltaPhi(m_met_wmuon_to_use))));
     if (content.goodElectrons.size() > 1) {
       TLorentzVector v_2el = content.goodElectrons[0]->p4() + content.goodElectrons[1]->p4();
       cand.evt.ee_pt = v_2el.Pt();
       cand.evt.ee_eta = v_2el.Eta();
       cand.evt.ee_phi = v_2el.Phi();
       cand.evt.ee_m = v_2el.M();
     } // at least two vetoed electrons
   } // at least a vetoed electron
   */

   /////////////////////////////
   // selected jets
   // + compute b-jet multiplicity
   ////////////////////////////
   cand.evt.n_jet = content.goodJets.size();
   static SG::AuxElement::Accessor<char> acc_bjet("bjet");
   UInt_t tmpNBJet(0);
   for (auto thisJet : content.goodJets) {
      cand.jet["jet"].add(*thisJet);
      if (acc_bjet(*thisJet))  tmpNBJet++;
   }
   cand.evt.n_bjet = tmpNBJet;

   
   /////////////////////////////
   // selected muons
   // + count bad muons
   ////////////////////////////
   cand.evt.n_mu = content.goodMuons.size();
   cand.evt.n_mu_baseline = content.baselineMuons.size();
   static SG::AuxElement::Accessor<char> acc_bad("bad");
   for (auto thisMuon : content.goodMuons) {
     cand.mu["mu"].add(*thisMuon);
   }
   for (auto thisMuon : content.baselineMuons){
     cand.mu["mu_baseline"].add(*thisMuon);
   }
   // SFs (new procedure)
   if (m_isMC && content.isNominal){
     // only reco
     m_objTool->GetTotalMuonSF(content.allMuons,true,false);
     for (auto thisMuon : content.goodMuons) cand.mu["mu"].add_SFs(*thisMuon,"Nom",false);
     for (auto thisMuon : content.baselineMuons) cand.mu["mu_baseline"].add_SFs(*thisMuon,"Nom",false);
     // reco, iso
     m_objTool->GetTotalMuonSF(content.allMuons,true,true);
     for (auto thisMuon : content.goodMuons) cand.mu["mu"].add_SFs(*thisMuon,"Nom",true);
   }
   UInt_t tmpNBadMu(0);
   for (auto thisMuon : * (content.muons)) {
      if (acc_bad(*thisMuon)) tmpNBadMu++;
   }
   cand.evt.n_allmu_bad = tmpNBadMu;


   /////////////////////////////
   // selected electrons
   ////////////////////////////   
   cand.evt.n_el = content.goodElectrons.size();
   cand.evt.n_el_baseline = content.baselineElectrons.size();
   for (auto thisElectron : content.goodElectrons){
     cand.el["el"].add(*thisElectron);
   }
   for (auto thisElectron : content.baselineElectrons){
     cand.el["el_baseline"].add(*thisElectron);
   }
   // SFs (new procedure)
   if (m_isMC && content.isNominal){
     // reco, id
     m_objTool->GetTotalElectronSF(content.allElectrons,true,true,false,false);
     for (auto thisElectron : content.goodElectrons) cand.el["el"].add_SFs(*thisElectron,"Nom",false,false,false);
     for (auto thisElectron : content.baselineElectrons) cand.el["el_baseline"].add_SFs(*thisElectron,"Nom",false,false,false);
     // trigger
     m_objTool->GetTotalElectronSF(content.allElectrons,false,false,true,false,"singleLepton");
     for (auto thisElectron : content.goodElectrons) cand.el["el"].add_SFs(*thisElectron,"Nom",true,false,false);
     // iso
     m_objTool->GetTotalElectronSF(content.allElectrons,false,false,false,true);
     for (auto thisElectron : content.goodElectrons) cand.el["el"].add_SFs(*thisElectron,"Nom",false,true,false);
     // reco, id, trigger, iso (tot)
     m_objTool->GetTotalElectronSF(content.allElectrons,true,true,true,true,"singleLepton");
     for (auto thisElectron : content.goodElectrons) cand.el["el"].add_SFs(*thisElectron,"Nom",false,false,true);
   }

   /////////////////////////////
   // selected photons
   ////////////////////////////
   cand.evt.n_ph = content.goodPhotons.size();
   cand.evt.n_ph_baseline = content.baselinePhotons.size();
   for (auto thisPhoton : content.goodPhotons) {
     cand.ph["ph"].add(*thisPhoton);
   }
   for (auto thisPhoton : content.baselinePhotons){
     cand.ph["ph_baseline"].add(*thisPhoton);
   }
   // SFs (new procedure)
   if (m_isMC && content.isNominal){
     // only reco
     m_objTool->GetTotalPhotonSF(content.allPhotons,true,false);
     for (auto thisPhoton : content.goodPhotons) cand.ph["ph"].add_SFs(*thisPhoton,"Nom",false);
     for (auto thisPhoton : content.baselinePhotons) cand.ph["ph_baseline"].add_SFs(*thisPhoton,"Nom",false);
     // reco, iso
     m_objTool->GetTotalPhotonSF(content.allPhotons,true,true);
     for (auto thisPhoton : content.goodPhotons) cand.ph["ph"].add_SFs(*thisPhoton,"Nom",true);
   }

   /////////////////////////////
   // selected taus
   ////////////////////////////
   if (m_doIncludeTaus) {
      cand.evt.n_tau = content.goodTaus.size();
      cand.evt.n_tau_baseline = content.baselineTaus.size();
      for (auto thisTau : content.goodTaus) {
	cand.tau["tau"].add(*thisTau);
      }
      for (auto thisTau : content.baselineTaus){
	cand.tau["tau_baseline"].add(*thisTau);
      }
      // SFs (new procedure)
      if (m_isMC && content.isNominal){
	// only reco
	m_objTool->GetTotalTauSF(content.allTaus,true,false);
	for (auto thisTau : content.baselineTaus) cand.tau["tau_baseline"].add_SFs(*thisTau,"Nom");
      }
   }

   /////////////////////////////
   // MET
   /////////////////////////////
   cand.met["met_tst"].add(*((*content.met_tst)["Final"]));
   double tmp_metSig(-9999);
   ANA_CHECK(m_objTool->GetMETSig(*content.met_tst,tmp_metSig, true, true));
   cand.evt.met_tst_sig = tmp_metSig;

   cand.met["met_nomuon_tst"].add(*((*content.met_tst_nomuon)["Final"]));
   cand.met["met_noelectron_tst"].add(*((*content.met_tst_noelectron)["Final"]));
   cand.met["met_nophoton_tst"].add(*((*content.met_tst_nophoton)["Final"]));
   cand.met["met_track"].add(*((*content.met_track)["Track"]));
   if (m_isMC) cand.met["met_truth"].add(*((*content.met_truth)["NonInt"]));
   cand.met["met_softerm_tst"].add(*((*content.met_tst_nomuon)["PVSoftTrk"]));
   //cand.met["met_softerm_cst"].add(*((*content.met_cst_nomuon)["SoftClus"]));
   //cand.met["met_muonterm_tst"].add(*((*content.met_tst)["Muons"]));
   cand.met["met_jetterm"].add(*((*content.met_tst)["RefJet"]));
   cand.met["met_muonterm"].add(*((*content.met_tst)["Muons"]));
   cand.met["met_eleterm"].add(*((*content.met_tst)["RefEle"]));
   cand.met["met_phterm"].add(*((*content.met_tst)["RefGamma"]));
   
   
   // overall lepton scale factor calculation for baseline leptons
   if (m_isMC && content.isNominal) {
   //   if (m_isMC && (m_doSystematics || content.isNominal)) {
     if (m_doIncludeTaus) {
       //       for (auto &kv : cand.tau["tau"].tau_SF_Sys) GetTauSFsys(content,cand,kv.first.Data());
       for (auto &kv : cand.tau["tau_baseline"].tau_SF_Sys) GetTauSFsys(content,cand,kv.first.Data());
     }
     //      for (auto &kv : cand.el["el"].el_SF_Sys) GetElectronSFsys(content,cand,kv.first.Data());
     for (auto &kv : cand.el["el_baseline"].el_SF_Sys) GetElectronSFsys(content,cand,kv.first.Data());
     //      for (auto &kv : cand.mu["mu"].mu_SF_Sys) GetMuonSFsys(content,cand,kv.first.Data());
     for (auto &kv : cand.mu["mu_baseline"].mu_SF_Sys) GetMuonSFsys(content,cand,kv.first.Data());
     //      for (auto &kv : cand.ph["ph"].ph_SF_Sys) GetPhotonSFsys(content,cand,kv.first.Data());
     for (auto &kv : cand.ph["ph_baseline"].ph_SF_Sys) GetPhotonSFsys(content,cand,kv.first.Data());
   }

   isRegion(content, cand);

   /////////////////////////////
   // write tree
   /////////////////////////////
   cand.save();

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode MonoJet::getTrackMET(std::shared_ptr<xAOD::MissingETContainer> &met, std::shared_ptr<xAOD::MissingETAuxContainer> &metAux, xAOD::JetContainer * jet, xAOD::ElectronContainer * el, xAOD::MuonContainer * mu)
{
   met = std::make_shared<xAOD::MissingETContainer>();
   metAux = std::make_shared<xAOD::MissingETAuxContainer>();
   met->setStore(metAux.get());

   ANA_CHECK(m_objTool->GetTrackMET(*met, // note that we don't need to pass the Aux container here
                                    jet, // use all objects (before OR and after corrections) for MET utility
                                    el,
                                    mu
                                   ));
   xAOD::MissingETContainer::const_iterator met_it = met->find("Track");

   if (met_it == met->end()) {
      ANA_MSG_ERROR("No Track inside MET container");
      return EL::StatusCode::FAILURE;
   }


   return EL::StatusCode::SUCCESS;
}

EL::StatusCode MonoJet::getLastCutPassed(Analysis::ContentHolder & content, MonoJetCuts::CutID & last)
{

   xAOD::TEvent *event = wk()->xaodEvent();

   //////
   // retrieve event info (used here to check data vs MC, and everywhere for analysis)
   //////
   content.eventInfo = nullptr;

   if (!event->retrieve(content.eventInfo, "EventInfo").isSuccess()) {
      ANA_MSG_ERROR("MonoJet::getLastCutPassed : Failed to retrieve event info collection");
      return EL::StatusCode::FAILURE;
   }

   if (content.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && !m_isMC) {
      throw std::runtime_error("Found MC when set-up for data");
   } else if (!content.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && m_isMC) {
      throw std::runtime_error("Found data when set-up for MC");
   }


   ///////////////////
   // GRL, cleaning
   ///////////////////

   last = MonoJetCuts::processed;

   if (!m_isMC) {
      // GRL
      if (!m_grl->passRunLB(*content.eventInfo)) {
         return EL::StatusCode::SUCCESS;
      } else {
         last = MonoJetCuts::GRL;
      }
   }

   // cleaning
   if ((content.eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
       (content.eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
       //(content.eventInfo->errorState(xAOD::EventInfo::SCT)  == xAOD::EventInfo::Error && !(configFile == "SUSYTools_MonoJetnfigCutflow.config")) || //commented out in R21
       (content.eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000)) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::cleaning;
   }

   ///////////////////
   // trigger
   ///////////////////
   // OBSOLETE (implemented offline)

   last = MonoJetCuts::trigger;


   ///////////////////
   // good /*
   ///////////////////
   Bool_t passesVertex(kFALSE);
   const UInt_t nMinTracks(2);
   content.vertices = nullptr;

   if (!event->retrieve(content.vertices, "PrimaryVertices").isSuccess()) {
      ANA_MSG_ERROR("MonoJet::getLastCutPassed : Failed to retrieve PrimaryVertices container");
      return EL::StatusCode::FAILURE;
   }
   for (auto thisVertex : *content.vertices) {
      if ((UInt_t)(thisVertex->nTrackParticles()) >= nMinTracks) {
         passesVertex = kTRUE;
         break;
      }
   }

   if (!passesVertex) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::vertex;
   }

   ///////////////////
   // MET cleaning
   ///////////////////
   bool dirtyJets = false;
   if (!dirtyJets) {
      static SG::AuxElement::Accessor<char> acc_passOR("passOR"); // passOR implies baseline
      static SG::AuxElement::Accessor<char> acc_bad("bad");
      for (auto thisJet : content.allJets) {
         if (acc_passOR(*thisJet) == 1 && acc_bad(*thisJet) == 1) return EL::StatusCode::SUCCESS;
      }
   }
   last = MonoJetCuts::MET_cleaning;


   ///////////////////
   // BCH cleaning
   ///////////////////
   // TODO: implement when available
   last = MonoJetCuts::BCH_cleaning;


   ///////////////////
   // leading jet pT cut
   ///////////////////
   Bool_t passesJetLeadingPt(kFALSE);

   const Double_t minLeadingJetPt(80000);

   for (auto thisJet : content.goodJets) {
      if (thisJet->pt() >= minLeadingJetPt) {
         passesJetLeadingPt = kTRUE;
         break;
      }
   }

   if (!passesJetLeadingPt) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::Leading_Pt_cut;
   }

   TVector2 met_nomuon_to_use = TVector2((*content.met_tst_nomuon)["Final"]->mpx(), (*content.met_tst_nomuon)["Final"]->mpy());
   //Bool_t passesMETcut = ((*content.met_tst)["Final"]->met() > 100000);
   Bool_t passesMETcut = (met_nomuon_to_use.Mod() > 100000);
   if (!passesMETcut) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::MET_cut;
   }

   ///////////////////
   // leading jet cleaning
   ///////////////////
   // OBSOLETE, as implemented offline
   last = MonoJetCuts::Leading_cleaning;

   ///////////////////
   // lepton veto
   ///////////////////
   Bool_t passesTauVeto = (content.baselineTaus.size()) == 0;
   if (!passesTauVeto) {
     return EL::StatusCode::SUCCESS;
   } else {
     last = MonoJetCuts::tau_veto;
   }

   Bool_t passesElectronVeto = (content.baselineElectrons.size()) == 0;
   if (!passesElectronVeto) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::electron_veto;
   }

   Bool_t passesMuonVeto = (content.baselineMuons.size()) == 0;
   if (!passesMuonVeto) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::muon_veto;
   }

   ///////////////////
   //photon veto
   ///////////////////
   if(!doMonophoton){
   Bool_t passesPhotonVeto = (content.baselinePhotons.size()) == 0;
   //   if (!doIgnorePhotons) passesPhotonVeto = (m_n_ph_baseline == 0);

   if (!passesPhotonVeto) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::photon_veto;
   }
   }

   ///////////////////
   //5th jet veto
   ///////////////////
   if (content.goodJets.size() > 4) return EL::StatusCode::SUCCESS;
   else last = MonoJetCuts::jet_multiplicity;


   ///////////////////
   // jet/MET overlap
   ///////////////////
   Bool_t passesJetMETOverlap(kTRUE);
   //Double_t met_phi =  (*content.met_tst_nomuon)["Final"]->phi();
   const Double_t minJetMETDistance(0.4);

   for (auto thisJet : content.goodJets) {
      if (TMath::Abs(TMath::ACos(TMath::Cos(thisJet->phi() - met_nomuon_to_use.Phi()))) < minJetMETDistance) {
         // note: this cut is tuned on SR, i.e. uses the MET_nomuon instead of MET_noelectron (to be used for e.g. CR2e)
         passesJetMETOverlap = kFALSE;
         break;
      }
   }

   if (!passesJetMETOverlap) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::jet_MET_overlap;
   }

   ///////////////////
   // tighter jet/MET cuts
   ///////////////////
   Bool_t passesMETcut_2 = ((*content.met_tst)["Final"]->met() > 400000);

   if (!passesMETcut_2) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::MET_cut_2;
   }

   Bool_t passesJetLeadingPt_2(kFALSE);

   const Double_t minLeadingJetPt_2(400000);

   for (auto thisJet : content.goodJets) {
      if (thisJet->pt() >= minLeadingJetPt_2) {
         passesJetLeadingPt_2 = kTRUE;
         break;
      }
   }

   if (!passesJetLeadingPt_2) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoJetCuts::Leading_Pt_cut_2;
   }

   return EL::StatusCode::SUCCESS;
}

EL::StatusCode MonoJet::getLastCutPassedMonophoton(Analysis::ContentHolder & content, MonoPhotonCuts::CutID & last) 
{

   xAOD::TEvent *event = wk()->xaodEvent();

   //////
   // retrieve event info (used here to check data vs MC, and everywhere for analysis)
   //////
   content.eventInfo = nullptr;

   if (!event->retrieve(content.eventInfo, "EventInfo").isSuccess()) {
      ANA_MSG_ERROR("MonoJet::getLastCutPassed : Failed to retrieve event info collection");
      return EL::StatusCode::FAILURE;
   }

   if (content.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && !m_isMC) {
      throw std::runtime_error("Found MC when set-up for data");
   } else if (!content.eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION) && m_isMC) {
      throw std::runtime_error("Found data when set-up for MC");
   }


   ///////////////////
   // GRL, cleaning
   ///////////////////

   last = MonoPhotonCuts::processed;

   if (!m_isMC) {
      // GRL
      if (!m_grl->passRunLB(*content.eventInfo)) {
         return EL::StatusCode::SUCCESS;
      } else {
         last = MonoPhotonCuts::GRL;
      }
   }

   // cleaning
   if ((content.eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) ||
       (content.eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) ||
       //(content.eventInfo->errorState(xAOD::EventInfo::SCT)  == xAOD::EventInfo::Error && !(configFile == "SUSYTools_MonoJetnfigCutflow.config")) || //commented out in R21
       (content.eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000)) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoPhotonCuts::cleaning;
   }

   ///////////////////
   // trigger
   ///////////////////
   //if(!m_objTool->IsTrigPassed("HLT_g140_loose")) return EL::StatusCode::SUCCESS;
   /*else */last = MonoPhotonCuts::trigger;


   ///////////////////
   // good /*
   ///////////////////
   Bool_t passesVertex(kFALSE);
   const UInt_t nMinTracks(2);
   content.vertices = nullptr;

   if (!event->retrieve(content.vertices, "PrimaryVertices").isSuccess()) {
      ANA_MSG_ERROR("MonoJet::getLastCutPassed : Failed to retrieve PrimaryVertices container");
      return EL::StatusCode::FAILURE;
   }

   for (auto thisVertex : *content.vertices) {
      if ((UInt_t)(thisVertex->nTrackParticles()) >= nMinTracks) {
         passesVertex = kTRUE;
         break;
     }   
   }

   if (!passesVertex) {
      return EL::StatusCode::SUCCESS;
   } else {
      last = MonoPhotonCuts::vertex;
   }

 
   ///////////////////
   // MET cleaning    
   ///////////////////
   bool dirtyJets = false;
   if (!dirtyJets) {
      static SG::AuxElement::Accessor<char> acc_passOR("passOR"); // passOR implies baseline
      static SG::AuxElement::Accessor<char> acc_bad("bad");
      for (auto thisJet : content.allJets) {
         if (acc_passOR(*thisJet) == 1 && acc_bad(*thisJet) == 1 /*&& thisJet->pt()>20000*/) return EL::StatusCode::SUCCESS;
      }
   }
   last = MonoPhotonCuts::MET_cleaning;


   ///////////////////
   // BCH cleaning
   ///////////////////
   // TODO: implement when available
   last = MonoPhotonCuts::BCH_cleaning;

   return EL::StatusCode::SUCCESS;
}




EL::StatusCode MonoJet::isRegion(Analysis::ContentHolder & content, Analysis::Output & cand)
{

   // This function will set the variables isSR, isCR* and the weights.
   // These variables are especially needed to work with the syst tree output

   //TODO: AntiSFs are ignored here, need to be done at a later stage in the pipeline
   //XSec weights, SumWeight also need to be applied in the MonoJetReader

   	
   cand.evt.isSR = 0;
   cand.evt.isCR1mubveto = 0;
   cand.evt.isCR1mubtag = 0;
   cand.evt.isCR1ebveto = 0;
   cand.evt.isCR1ebtag = 0;
   //cand.evt.isCR1e_metnoel = 0;
   cand.evt.isCR2e = 0;
   cand.evt.isCR2mu = 0;
   cand.evt.isCR1ph = 0;

   Bool_t is2015(kFALSE);
   Bool_t is2016(kFALSE);
   Bool_t is2017(kFALSE);
   Bool_t is2018(kFALSE);

   if ((cand.evt.year == 0 && cand.evt.run >= 276262 && cand.evt.run <= 284484) || cand.evt.year == 2015) is2015 = kTRUE; // data2015
   if ((cand.evt.year == 0 && cand.evt.run > 284484 && cand.evt.run <= 311481) || cand.evt.year == 2016) is2016 = kTRUE; // data2016
   if ((cand.evt.year == 0 && cand.evt.run >= 325713 && cand.evt.run <= 340453) || cand.evt.year == 2017) is2017 = kTRUE; // data2017
   if ((cand.evt.year == 0 && cand.evt.run >= 348885 && cand.evt.run <= 364292) || cand.evt.year == 2018) is2018 = kTRUE; // data2018

   Bool_t met_fire = false;
   Bool_t el_fire = false;
   Bool_t ph_fire = false;

   /*std::vector<std::string>  met_triggers={"HLT_xe70",
   "HLT_xe80_tc_lcw_L1XE50","HLT_xe90_mht_L1XE50","HLT_xe100_mht_L1XE50","HLT_xe110_mht_L1XE50",
   "HLT_xe110_pufit_L1XE55"};
   std::vector<std::string>  el_triggers={"HLT_e24_lhmedium_L1EM20VH","HLT_e60_lhmedium","HLT_e120_lhloose",
   "HLT_e24_lhmedium_nod0_L1EM20VH","HLT_e24_lhtight_nod0_ivarloose","HLT_e26_lhtight_nod0_ivarloose","HLT_e60_medium","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut",
   "HLT_e26_lhtight_nod0_ivarloose","HLT_e60_lhmedium_nod0","HLT_e140_lhloose_nod0","HLT_e300_etcut"};
   std::vector<std::string>  ph_triggers={"HLT_g140_loose","HLT_g160_loose"};

   for(auto triggername: met_triggers){
    if(m_objTool->IsTrigPassed(triggername))
      met_fire=true;
   }

   for(auto triggername: el_triggers){
    if(m_objTool->IsTrigPassed(triggername))
      el_fire=true;
   }

   for(auto triggername: ph_triggers){
    if(m_objTool->IsTrigPassed(triggername))
      ph_fire=true;
   }*/

   if (m_isMC) met_fire = ((is2015 && m_objTool->IsTrigPassed("HLT_xe70")) ||
			   (is2016 && (m_objTool->IsTrigPassed("HLT_xe80_tc_lcw_L1XE50") || m_objTool->IsTrigPassed("HLT_xe90_mht_L1XE50") || m_objTool->IsTrigPassed("HLT_xe100_mht_L1XE50") || m_objTool->IsTrigPassed("HLT_xe110_mht_L1XE50") || m_objTool->IsTrigPassed("HLT_xe130_mht_L1XE50"))) ||
			   (is2017 && (m_objTool->IsTrigPassed("HLT_xe90_pufit_L1XE50") || m_objTool->IsTrigPassed("HLT_xe110_pufit_L1XE55") ||  m_objTool->IsTrigPassed("HLT_xe110_pufit_L1XE50") ||  m_objTool->IsTrigPassed("HLT_xe100_pufit_L1XE55") ||  m_objTool->IsTrigPassed("HLT_xe100_pufit_L1XE50"))) ||
			   (is2018 && ( m_objTool->IsTrigPassed("HLT_xe110_pufit_xe70_L1XE50") ||  m_objTool->IsTrigPassed("HLT_xe110_pufit_xe65_L1XE50") ||  m_objTool->IsTrigPassed("HLT_xe120_pufit_L1XE50")))       
			   );
   else met_fire = isPassMETTrigger(cand.evt.run);


   // logic ORs
   if (cand.evt.n_el >= 1) {
     el_fire = ((is2015 && (m_objTool->IsTrigPassed("HLT_e24_lhmedium_L1EM20VH") || m_objTool->IsTrigPassed("HLT_e60_lhmedium") || m_objTool->IsTrigPassed("HLT_e120_lhloose"))) ||
		(!is2015 && (m_objTool->IsTrigPassed("HLT_e26_lhtight_nod0_ivarloose") || m_objTool->IsTrigPassed("HLT_e60_lhmedium_nod0") || m_objTool->IsTrigPassed("HLT_e140_lhloose_nod0") || m_objTool->IsTrigPassed("HLT_e300_etcut"))));
   }
   
   if (cand.evt.n_ph >= 1) {
     ph_fire = ((is2015 && (m_objTool->IsTrigPassed("HLT_g120_loose") || m_objTool->IsTrigPassed("HLT_g200_etcut"))) ||
		(!is2015 && (m_objTool->IsTrigPassed("HLT_g140_loose") || m_objTool->IsTrigPassed("HLT_g300_etcut"))));
   }

   //////////////////
   // composite variables
   //////////////////

   // MET to be used
   TVector2 met_wmuon_to_use =  TVector2((*content.met_tst)["Final"]->mpx(), (*content.met_tst)["Final"]->mpy());
   TVector2 met_nomuon_to_use = TVector2((*content.met_tst_nomuon)["Final"]->mpx(), (*content.met_tst_nomuon)["Final"]->mpy());
   TVector2 met_noelectron_to_use = TVector2((*content.met_tst_noelectron)["Final"]->mpx(), (*content.met_tst_noelectron)["Final"]->mpy());
   //TVector2 met_nophoton_to_use = TVector2((*content.met_tst_nophoton)["Final"]->mpx(), (*content.met_tst_nophoton)["Final"]->mpy());

   Float_t tmp_met_wmuon= met_wmuon_to_use.Mod();
   Float_t tmp_met_nomuon = met_nomuon_to_use.Mod();
   Float_t tmp_met_noelectron= met_noelectron_to_use.Mod();
   Float_t tmp_met_nophoton= 0.;
   if (cand.evt.n_ph == 1)
      tmp_met_nophoton= cand.ph["ph"].pt.at(0); //met_nophoton_to_use.Mod();

   if (content.goodMuons.size() > 0) {
      const xAOD::Muon *thisMuon = content.goodMuons[0];
      cand.evt.munu_mT = TMath::Sqrt(2.0 * thisMuon->pt() * met_wmuon_to_use.Mod() * (1 - TMath::Cos(thisMuon->p4().Vect().XYvector().DeltaPhi(met_wmuon_to_use))));
      if (content.goodMuons.size() > 1) {
         TLorentzVector v_2mu = thisMuon->p4() + content.goodMuons[1]->p4();
         cand.evt.mumu_m = v_2mu.M();
      } // at least two vetoed muons
   } // at least a vetoed muon
   if (content.goodElectrons.size() > 0) {
      cand.evt.enu_mT = TMath::Sqrt(2.0 * content.goodElectrons[0]->pt() * met_wmuon_to_use.Mod() * (1 - TMath::Cos(content.goodElectrons[0]->p4().Vect().XYvector().DeltaPhi(met_wmuon_to_use))));
      if (content.goodElectrons.size() > 1) {
         TLorentzVector v_2el = content.goodElectrons[0]->p4() + content.goodElectrons[1]->p4();
         cand.evt.ee_m = v_2el.M();
      } // at least two vetoed electrons
   } // at least a vetoed electron

   //determine region
   Bool_t passesJetCleaning = (cand.evt.n_jet >= 1 && abs(cand.jet["jet"].eta.at(0)) <= 2.4 && cand.jet["jet"].fch.at(0) / cand.jet["jet"].fmax.at(0) >= 0.1);

   if (m_last >= 6 && passesJetCleaning == true && cand.evt.n_jet > 0 && cand.jet["jet"].pt.at(0) > ptSkim && cand.evt.n_jet <= 4) { //jets

      //calculate dphi between jets and met
      Bool_t pass_dphi_met_wmuon = true;
      Bool_t pass_dphi_met_nomuon = true;
      Bool_t pass_dphi_met_noelectron = true;
      Bool_t pass_dphi_met_nophoton = true;

      for (Int_t i = 0; i < cand.evt.n_jet; i++) {
         TLorentzVector j;
         j.SetPtEtaPhiM(cand.jet["jet"].pt.at(i), cand.jet["jet"].eta.at(i), cand.jet["jet"].phi.at(i), cand.jet["jet"].m.at(i));
         const Double_t thisAbsDPhi = TMath::Abs(j.Vect().XYvector().DeltaPhi(TVector2(cand.met["met_tst"].etx, cand.met["met_tst"].ety)));
         if (thisAbsDPhi <= 0.4) pass_dphi_met_wmuon = false;
      }

      for (Int_t i = 0; i < cand.evt.n_jet; i++) {
         TLorentzVector j;
         j.SetPtEtaPhiM(cand.jet["jet"].pt.at(i), cand.jet["jet"].eta.at(i), cand.jet["jet"].phi.at(i), cand.jet["jet"].m.at(i));
         const Double_t thisAbsDPhi = TMath::Abs(j.Vect().XYvector().DeltaPhi(TVector2(cand.met["met_nomuon_tst"].etx, cand.met["met_nomuon_tst"].ety)));
         if (thisAbsDPhi <= 0.4) pass_dphi_met_nomuon = false;
      }

      for (Int_t i = 0; i < cand.evt.n_jet; i++) {
         TLorentzVector j;
         j.SetPtEtaPhiM(cand.jet["jet"].pt.at(i), cand.jet["jet"].eta.at(i), cand.jet["jet"].phi.at(i), cand.jet["jet"].m.at(i));
         const Double_t thisAbsDPhi = TMath::Abs(j.Vect().XYvector().DeltaPhi(TVector2(cand.met["met_noelectron_tst"].etx, cand.met["met_noelectron_tst"].ety)));
         if (thisAbsDPhi <= 0.4) pass_dphi_met_noelectron = false;
      }

      if (cand.evt.n_ph >= 1) {
         Double_t phptx = (Double_t)cand.ph["ph"].pt.at(0) * (Double_t)TMath::Cos(cand.ph["ph"].phi.at(0));
         Double_t phpty = (Double_t)cand.ph["ph"].pt.at(0) * (Double_t)TMath::Sin(cand.ph["ph"].phi.at(0));
         for (Int_t i = 0; i < cand.evt.n_jet; i++) {
            TLorentzVector j;
            j.SetPtEtaPhiM(cand.jet["jet"].pt.at(i), cand.jet["jet"].eta.at(i), cand.jet["jet"].phi.at(i), cand.jet["jet"].m.at(i));
            //const Double_t thisAbsDPhi = TMath::Abs(j.Vect().XYvector().DeltaPhi(TVector2(cand.met["met_nophoton_tst"].etx, cand.met["met_nophoton_tst"].ety)));
            const Double_t thisAbsDPhi = TMath::Abs(j.Vect().XYvector().DeltaPhi(TVector2(phptx, phpty)));
            if (thisAbsDPhi <= 0.4) pass_dphi_met_nophoton = false;
         }
      }

      // calculate HT variable for isCR1e_metnoel
      Float_t HT = 0.;  // in GeV
      for (Int_t i = 0; i < cand.evt.n_jet; i++) HT += cand.jet["jet"].pt.at(i) / 1000.;


      if (cand.evt.n_mu_baseline == 0 && cand.evt.n_el_baseline == 0 && cand.evt.n_ph_baseline == 0 && cand.evt.n_el == 0 && cand.evt.n_mu == 0 && cand.evt.n_ph == 0) { //isSR
         if (tmp_met_wmuon> metSkim && pass_dphi_met_wmuon && met_fire) {
            cand.evt.isSR = 1;
         }
      }
      else if (cand.evt.n_mu_baseline == 1 && cand.evt.n_el_baseline == 0 && cand.evt.n_ph_baseline == 0 && cand.evt.n_el == 0 && cand.evt.n_mu == 1 && cand.evt.n_ph == 0) { //isCR1mubveto
	if (tmp_met_nomuon> metSkim && cand.evt.munu_mT / 1000. > 30. && cand.evt.munu_mT / 1000. <= 100. && pass_dphi_met_nomuon && met_fire) {
	  if (cand.evt.n_bjet == 0) {
	    cand.evt.isCR1mubveto = 1;
	  } else if (cand.evt.n_bjet >= 1) {
	    cand.evt.isCR1mubtag = 1;
	  }
	}
      } 
      else if (cand.evt.n_mu_baseline == 0 && cand.evt.n_el_baseline == 1 && cand.evt.n_ph_baseline == 0 && cand.evt.n_el == 1 && cand.evt.n_mu == 0 && cand.evt.n_ph == 0) { //isCR1e_metnoel
	if (tmp_met_noelectron> metSkim && pass_dphi_met_noelectron && cand.el["el"].pt[0] >= 30000 && (fabs(cand.el["el"].eta[0]) >= 1.52 || fabs(cand.el["el"].eta[0]) <= 1.37) && cand.evt.enu_mT / 1000. > 30. && cand.evt.enu_mT / 1000. <= 100. && cand.el["el"].ptvarcone20_TightTTVA_pt1000[0] / cand.el["el"].pt[0] < 0.06 && cand.el["el"].topoetcone20[0] / cand.el["el"].pt[0] < 0.06  && tmp_met_wmuon>= 70000. && tmp_met_wmuon/ 1000. / sqrt(HT) >= 5. && el_fire) {
	  //cand.evt.isCR1e_metnoel = 1; //can be recovered offline
	  //split by bjet multiplicity
	  if (cand.evt.n_bjet == 0) {
	    cand.evt.isCR1ebveto = 1;
	  } else if (cand.evt.n_bjet >= 1) {
	    cand.evt.isCR1ebtag = 1;
	  }
	}
      } 
      else if (cand.evt.n_mu_baseline == 0 && cand.evt.n_el_baseline == 2 && cand.evt.n_ph_baseline == 0 && cand.evt.n_el == 2 && cand.evt.n_mu == 0 && cand.evt.n_ph == 0) { //isCR2e
	if (tmp_met_noelectron> metSkim && cand.evt.ee_m / 1000. > 66. && cand.evt.ee_m / 1000. <= 116. && pass_dphi_met_noelectron && cand.el["el"].pt[0] >= 30000 && cand.el["el"].pt[1] >= 30000 && el_fire) {
	  cand.evt.isCR2e = 1;
	}
      } 
      else if (cand.evt.n_mu_baseline == 2 && cand.evt.n_el_baseline == 0 && cand.evt.n_ph_baseline == 0 && cand.evt.n_el == 0 && cand.evt.n_mu == 2 && cand.evt.n_ph == 0) { //isCR2mu
	if (tmp_met_nomuon> metSkim && cand.evt.mumu_m  / 1000. > 66. && cand.evt.mumu_m / 1000. <= 116. && pass_dphi_met_nomuon && met_fire) {
	  cand.evt.isCR2mu = 1;
	}
      } 
      else if (cand.evt.n_mu_baseline == 0 && cand.evt.n_el_baseline == 0 && cand.evt.n_ph_baseline == 1 && cand.evt.n_el == 0 && cand.evt.n_mu == 0 && cand.evt.n_ph == 1) { //isCR1ph
	if (tmp_met_nophoton> metSkim && pass_dphi_met_nophoton && (cand.ph["ph"].topoetcone40.at(0) / 1000. < 0.022 * cand.ph["ph"].pt.at(0) / 1000. + 2.45) && (cand.ph["ph"].ptcone20.at(0) / cand.ph["ph"].pt.at(0) < 0.05) && ph_fire) {
	  cand.evt.isCR1ph = 1;
	}
      }
   }

   //determine weights:
   Float_t SFs = 1.;
   if (m_isMC) {//HINT: needs to be in sync with what is done in the NTupleMaker
      for (UInt_t i = 0; i < cand.ph["ph"].SF_iso.size(); i++) {//photons
         if (cand.evt.isCR1ph) SFs *= cand.ph["ph"].SF.at(i) * cand.ph["ph"].SF_iso.at(i);
      }
      for (UInt_t i = 0; i < cand.mu["mu"].SF.size(); i++) {//muons
	if (cand.evt.isCR2mu == 1 || cand.evt.isCR1mubveto == 1 || cand.evt.isCR1mubtag == 1) SFs *= cand.mu["mu"].SF.at(i); 
      }
      for (UInt_t i = 0; i < cand.el["el"].SF.size(); i++) {//electrons
	 if (cand.evt.isCR1ebveto == 1 || cand.evt.isCR1ebtag == 1 ) SFs *= cand.el["el"].SF.at(i) * cand.el["el"].SF_iso.at(i) * cand.el["el"].SF_trigger.at(i);
         if (cand.evt.isCR2e == 1) SFs *= cand.el["el"].SF.at(i) * cand.el["el"].SF_trigger.at(i); 
      }
   }

   cand.evt.syst_weight = (m_isMC) ? SFs * cand.evt.pu_weight : 1.0;
   if ((cand.evt.isCR1mubveto == 1 or cand.evt.isCR1mubtag == 1 or cand.evt.isCR1ebveto == 1 or cand.evt.isCR1ebtag == 1) && m_isMC)  cand.evt.syst_weight *= 1; // SGF: This variable was removed, so I'm putting 1 here -> cand.evt.btag_weight;
   if (m_isMC)  cand.evt.syst_weight *= cand.evt.jvt_weight;

   return EL::StatusCode::SUCCESS;
}

/*EL::StatusCode MonoJet::isRegionMonophoton(Analysis::ContentHolder &content, Analysis::Output & cand) //MonoPhoton
{

   // This function will set the variables isSR, isCR* and the weights.
   // These variables are especially needed to work with the syst tree output

   //TODO: AntiSFs are ignored here, need to be done at a later stage in the pipeline
   //XSec weights, SumWeight also need to be applied in the MonoJetReader

   	
   cand.evt.isSR = 0;
   cand.evt.isCR1mubveto = 0;
   cand.evt.isCR1mubtag = 0;
   cand.evt.isCR1e_metnoel = 0;
   cand.evt.isCR2e = 0;
   cand.evt.isCR2mu = 0;
   cand.evt.isCR1ph = 0;

   Bool_t is2015(kFALSE);
   Bool_t is2016(kFALSE);
   Bool_t is2017(kFALSE);

   if ((cand.evt.year == 0 && cand.evt.run >= 276262 && cand.evt.run <= 284484) || cand.evt.year == 2015) is2015 = kTRUE; // data2015
   if ((cand.evt.year == 0 && cand.evt.run > 284484 && cand.evt.run <= 311481) || cand.evt.year == 2016) is2016 = kTRUE; // data2016
   if ((cand.evt.year == 0 && cand.evt.run >= 325713) || cand.evt.year == 2017) is2017 = kTRUE; // data2017

   Bool_t met_fire = false;
   Bool_t el_fire = false;
   Bool_t ph_fire = false;


// .......

   return EL::StatusCode::SUCCESS;
}*/


// MONOPHOTON FILL TREE #################################################

EL::StatusCode MonoJet::fillTreeMonophoton(Analysis::ContentHolder &content, Analysis::Output &cand, MonoPhotonCuts::CutID &last, CP::SystematicSet sys)  //MonoPhoton
{
   xAOD::TEvent *event = wk()->xaodEvent();

   cand.reset();


   // trigger
   for (auto &kv : cand.evt.trigger) {
      kv.second = m_objTool->IsTrigPassed(kv.first.Data());
   }
  
   //cand.evt.trigger_pass = cand.evt.trigger["HLT_g140_loose"]||cand.evt.trigger["HLT_g160_loose"]||cand.evt.trigger["HLT_xe90_tc_lcw_L1XE50"]||cand.evt.trigger["HLT_xe90_mht"]||cand.evt.trigger["HLT_xe100_mht_L1XE50"]||cand.evt.trigger["HLT_xe100"]||cand.evt.trigger["HLT_xe90_mht_L1XE50"]||cand.evt.trigger["HLT_xe110_mht_L1XE50"];  
   

   // raw event info
   
 
   cand.evt.run = (m_isMC) ? content.eventInfo->mcChannelNumber() : content.eventInfo->runNumber();
   cand.evt.event = (ULong64_t)content.eventInfo->eventNumber();
   cand.evt.lbn = content.eventInfo->lumiBlock();
   cand.evt.bcid = content.eventInfo->bcid();
   cand.evt.averageIntPerXing = content.eventInfo->averageInteractionsPerCrossing();
   cand.evt.actualIntPerXing = content.eventInfo->actualInteractionsPerCrossing();
   cand.evt.corAverageIntPerXing = m_objTool->GetCorrectedAverageInteractionsPerCrossing();
   cand.evt.corActualIntPerXing = m_objTool->GetCorrectedActualInteractionsPerCrossing();
   cand.evt.allmu_tot_SF = (m_isMC) ?  m_objTool->GetTotalMuonSF(content.goodMuons, true, true, "HLT_mu26_imedium_OR_HLT_mu50") : 1.0;
   //monophoton
   if(m_isMC){
      cand.evt.xSec_SUSY = m_xSec_SUSY->rawxsect(cand.evt.run); 
      cand.evt.k_factor = m_xSec_SUSY->kfactor(cand.evt.run);
      cand.evt.filter_eff = m_xSec_SUSY->efficiency(cand.evt.run);
   }

   // event-level artifacts from SUSYTools
   cand.evt.year = (m_isMC) ? m_objTool->treatAsYear() : 0;

   // event-level artifacts from system flags
   cand.evt.flag_sct = (content.eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error);
   cand.evt.flag_core = (content.eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000);
   cand.evt.flag_bib = content.eventInfo->eventFlags(xAOD::EventInfo::Background) & (1 << 20);
   cand.evt.flag_bib_raw = content.eventInfo->eventFlags(xAOD::EventInfo::Background);

   // vertex information
   cand.evt.n_vx = content.vertices->size(); // absolute number of PV's (i.e. no track cut)


   Int_t n_pv_tmp = 0;
   for (auto vx : *content.vertices){
      if(vx->vertexType()==1 || vx->vertexType()==3) n_pv_tmp ++;
   }
   cand.evt.n_pv = n_pv_tmp;


   cand.evt.last = last;

   cand.evt.pass_PFlowCVC=m_objTool->IsPFlowCrackVetoCleaning(content.electrons,content.photons); //crack veto cleaning for PFlow

   // MC-only information
   if (m_isMC) {
      // MC weights
      cand.evt.mconly_weight = content.eventInfo->mcEventWeight();
		if(m_doTheoSystematics&&TString(sys.name()) == ""){ 
		   for (auto weight : m_weightTool->getWeightNames()) {
		     cand.evt.mconly_weights.push_back(m_weightTool->getWeight(weight));
		   }
		}

      //cand.evt.mconly_weights = content.eventInfo->mcEventWeights();

      // pile-up
      
      cand.evt.pu_weight = m_objTool->GetPileupWeight();
      cand.evt.pu_hash = m_objTool->GetPileupWeightHash();

      // event-level scale factors
      //cand.evt.btag_weight = m_objTool->BtagSF(&content.goodJets);
      cand.evt.jvt_weight = m_objTool->GetTotalJetSF(content.jets, false, true); // btag, jvt

      // PDF information
      const xAOD::TruthEventContainer *truthE(nullptr);
      if (!event->retrieve(truthE, "TruthEvents").isSuccess()) {
         ANA_MSG_ERROR("Failed to retrieve Truth container");
      } else {
         try {
            xAOD::TruthEventContainer::const_iterator truthE_itr = truthE->begin();
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_id1, xAOD::TruthEvent::PDGID1);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_id2, xAOD::TruthEvent::PDGID2);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_x1, xAOD::TruthEvent::X1);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_x2, xAOD::TruthEvent::X2);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_pdf1, xAOD::TruthEvent::PDF1);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_pdf2, xAOD::TruthEvent::PDF2);
            (*truthE_itr)->pdfInfoParameter(cand.evt.pdf_scale, xAOD::TruthEvent::Q);
         } catch (SG::ExcBadAuxVar) {
            // ignore this variable, when unavailable
            // (happens often, see
            // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MC15aKnownIssues
            // )
         }
      }


      // truth jet information
      // leading two jets are saved; this is needed for example for QCD MC, where
      // events with ((pt1+pt2)/2)/pt1 < 1.4 are ignored, in order to avoid counting
      // pileup jets as leading jets when combining different slices of leading jet pt
      const xAOD::JetContainer * truthJets(nullptr);
      static Bool_t failedLookingFor(kFALSE); // trick to avoid infinite RuntimeWarning's for EXOT6
      if (!failedLookingFor) {
         if (!event->retrieve(truthJets, "AntiKt4TruthJets").isSuccess()) {
            ANA_MSG_ERROR("MonoJet::analyzeEvent : Failed to access Truth Jets container; not attempting again, truth_jet* variables will be empty");
            failedLookingFor = kTRUE;
         } else {
            //number of truth jets with pt > 20 GeV and OR
            // loop over all truth jets passing basic cuts
            Int_t nTruthJets(0);
            const xAOD::TruthParticleContainer * truthP(0);
            ANA_CHECK(event->retrieve(truthP, "TruthParticles"));
            for (const auto& truthJ_itr : *truthJets) {
               if (truthJ_itr->pt() > 20000. && fabs(truthJ_itr->eta()) < 2.8) {
                  float minDR2 = 9999999.;
                  // for each jet, loop over all electrons and muons passing basic cuts to check if the jets should be counted or not
                  for (const auto& truthP_itr : *truthP) {
                     if (truthP_itr->pt() > 25000. && fabs(truthP_itr->eta()) < 2.5 && truthP_itr->status() == 1 && (abs(truthP_itr->pdgId()) == 11 || abs(truthP_itr->pdgId()) == 13)) {
                        if (xAOD::P4Helpers::isInDeltaR(*truthJ_itr, *truthP_itr, 0.2, true)) {
                           float dR2 = xAOD::P4Helpers::deltaR2(truthJ_itr, truthP_itr, true);
                           if (dR2 < minDR2) {
                              minDR2 = dR2;
                              // stop if we already know the jet shouldn't be counted
                              if (minDR2 < 0.2 * 0.2) break;
                           }
                        }
                     }
                  }
                  if (minDR2 > 0.2 * 0.2) nTruthJets++;
               }
            }
            cand.evt.n_jet_truth = nTruthJets;
         }
      } // already found AntiKt4TruthJets, or never failed yet


      // save truth information for the W/Z boson, and for truth muons
      //      const int mcid = content.eventInfo->mcChannelNumber();
      /*const xAOD::TruthParticleContainer *truthParticles(nullptr);
      const xAOD::TruthParticleContainer *truthTaus = nullptr;
      const xAOD::TruthParticleContainer *truthMuons = nullptr;
      const xAOD::TruthParticleContainer *truthElectrons = nullptr;
      //const xAOD::TruthParticleContainer *truthPhotons = nullptr;
      ANA_CHECK(event->retrieve(truthParticles, "TruthParticles"));
      if (!m_determinedDerivation) {
         ANA_MSG_INFO("Determining derivation type");
         
	 //SIL
	 //m_isEXOT6 = event->retrieve(truthMuons, "EXOT6TruthMuons").isSuccess();
         m_determinedDerivation = kTRUE;
         TString isEXOT6 = (m_isEXOT6) ? "YES" : "NO";
         ANA_MSG_INFO("Is it EXOT6? (will trigger access to dedicated truth electron and muon containers): " << isEXOT6);
      }
 
      if (!m_determinedDerivation) {
         ANA_MSG_ERROR("Wrong logic: is/isn't EXOT6 should have been determined in the muon part!");
      }
 
      const TString tau_container =  "TruthTaus";
      if (!event->retrieve(truthTaus, tau_container.Data()).isSuccess()) {    // retrieve arguments: container type, container key
         ANA_MSG_ERROR("Failed to retrieve Taus container");
         return EL::StatusCode::FAILURE;
      }*/

   } //close MC-only information

   
   //////////////////
   // composite variables
   //////////////////


   if (content.goodMuons.size() > 1) {
     const xAOD::Muon *thisMuon = content.goodMuons[0];
     TLorentzVector v_2mu = thisMuon->p4() + content.goodMuons[1]->p4();
     cand.evt.mumu_pt = v_2mu.Pt();
     cand.evt.mumu_eta = v_2mu.Eta();
     cand.evt.mumu_phi = v_2mu.Phi();
     cand.evt.mumu_m = v_2mu.M();
   } // at least two vetoed muons


   if (content.goodElectrons.size() > 1) {
     TLorentzVector v_2el = content.goodElectrons[0]->p4() + content.goodElectrons[1]->p4();
     cand.evt.ee_pt = v_2el.Pt();
     cand.evt.ee_eta = v_2el.Eta();
     cand.evt.ee_phi = v_2el.Phi();
     cand.evt.ee_m = v_2el.M();
   } // at least two vetoed electrons

   static SG::AuxElement::Decorator<unsigned int> dec_isLeading("isLeading");   
   static SG::AuxElement::Decorator<bool> dec_isSelected("isSelected");   
   //static SG::AuxElement::Decorator<unsigned int> dec_nSelected("nSelected");
   static SG::AuxElement::Decorator<bool> dec_isGood("isGood");    
     //static SG::AuxElement::Decorator<unsigned int> dec_jet_isLeading("jet_isLeading");   
   // Initializing dphi decorator for MonoPhoton analysis
   static SG::AuxElement::Decorator<float> dec_new_met_nomuon_dphi("new_met_nomuon_dphi");
   //static SG::AuxElement::Decorator<float> dec_new_met_dphi("new_met_dphi");
   static SG::AuxElement::Decorator<float> dec_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::Decorator<float> dec_new_met_noelectron_dphi("new_met_noelectron_dphi");  
   static SG::AuxElement::Decorator<float> dec_new_met_noelectron_probe_dphi("new_met_noelectron_probe_dphi"); 
   static SG::AuxElement::Decorator<float> dec_new_jet_ph_dphi("new_jet_ph_dphi");  


   /////////////////////////////
   // selected jets
   // + compute b-jet multiplicity
   ////////////////////////////
   cand.evt.n_jet = content.goodJets.size();
   /* JVT test for (auto thisJet : content.noJVTJets) { 
      dec_isLeading(*thisJet)=0;
      dec_isSelected(*thisJet)=false;
      dec_isGood(*thisJet)=false;
      //cand.jet["jet"].add_monophoton(*thisJet);
      if(content.baselinePhotons.size()>0) dec_new_jet_ph_dphi(*thisJet) = float_t(TVector2::Phi_mpi_pi( thisJet->phi() - content.baselinePhotons[0]->phi())); 
      cand.jet["noJVT_jet"].add_monophoton(*thisJet);
   }*/


   /////////////////////////////
   // selected jets with the monophoton good definition
   ////////////////////////////
   cand.evt.n_jet_tot = content.jets->size();
   UInt_t n_jet_good_tmp = 0;

   Int_t i_jet = 0;
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");

   for(auto thisJet : *content.jets){
      dec_isLeading(*thisJet)=0;
      dec_isSelected(*thisJet)=false;
      dec_isGood(*thisJet)=false;
      cand.jet["jet"].add_monophoton(*thisJet);
   }

   for (auto thisJet : content.goodJets) {
      dec_isSelected(*thisJet)=true;
      dec_isLeading(*thisJet)=0;
      dec_isGood(*thisJet)=false;
      dec_new_met_wmuon_dphi(*thisJet) = float_t(TVector2::Phi_mpi_pi( thisJet->phi() - (*content.met_tst)["Final"]->phi()));
      dec_new_met_nomuon_dphi(*thisJet) = float_t(TVector2::Phi_mpi_pi( thisJet->phi() - (*content.met_tst_nomuon)["Final"]->phi()));
      if(content.baselinePhotons.size()>0) dec_new_jet_ph_dphi(*thisJet) = float_t(TVector2::Phi_mpi_pi( thisJet->phi() - content.baselinePhotons[0]->phi())); 
   
      if(fabs(acc_new_met_nomuon_dphi(*thisJet))>0.4) {
         dec_isGood(*thisJet)=true;
         i_jet++;
         n_jet_good_tmp++;
         dec_isLeading(*thisJet)=i_jet;
      }
      //else if(fabs(acc_new_met_nomuon_dphi(*thisJet))<=0.4) dec_isSelected(*thisJet)=false;
      cand.jet["jet"].add_monophoton(*thisJet);
   }
   cand.evt.n_jet_good = n_jet_good_tmp;




   /////////////////////////////
   // selected muons
   // + count bad muons
   ////////////////////////////
   cand.evt.n_mu_tot = content.muons->size();
   cand.evt.n_mu_baseline = content.baselineMuons.size();
   cand.evt.n_mu = content.goodMuons.size();

   UInt_t i_mu = 0;
   static SG::AuxElement::Accessor<char> acc_bad("bad");
   for (auto thisMuon : content.goodMuons) {
      i_mu++;
      dec_isLeading(*thisMuon)=i_mu;
      cand.mu["mu"].add_monophoton(*thisMuon);
   }
   UInt_t tmpNBadMu(0);
   for (auto thisMuon : * (content.muons)) {
      if (acc_bad(*thisMuon)) tmpNBadMu++;
   }
   cand.evt.n_allmu_bad = tmpNBadMu;


   /////////////////////////////
   // selected electrons
   ////////////////////////////
   cand.evt.n_el_tot = content.electrons->size();
   cand.evt.n_el_baseline = content.baselineElectrons.size();
   cand.evt.n_el_probe = content.probeElectrons.size();
   cand.evt.n_el = content.goodElectrons.size();

   UInt_t i_el = 0;
   for (auto thisElectron : content.goodElectrons) {
      i_el++;
      dec_isLeading(*thisElectron)=i_el;
      dec_new_met_wmuon_dphi(*thisElectron) = float_t(TVector2::Phi_mpi_pi( thisElectron->phi() - (*content.met_tst)["Final"]->phi()));
      dec_new_met_nomuon_dphi(*thisElectron) = float_t(TVector2::Phi_mpi_pi( thisElectron->phi() - (*content.met_tst_nomuon)["Final"]->phi()));
      dec_new_met_noelectron_dphi(*thisElectron) = float_t(TVector2::Phi_mpi_pi( thisElectron->phi() - (*content.met_tst_noelectron)["Final"]->phi())); 
      dec_new_met_noelectron_probe_dphi(*thisElectron) = float_t(TVector2::Phi_mpi_pi( thisElectron->phi() - (*content.met_tst_noelectron_probe)["Final"]->phi())); 
      cand.el["el"].add_monophoton(*thisElectron);
   }

   /////////////////////////////
   // selected photons
   // + count tight photons
   ////////////////////////////
   cand.evt.n_ph_tot = content.photons->size();
   cand.evt.n_ph_baseline = content.baselinePhotons.size(); 

   static SG::AuxElement::Decorator<float> dec_ph_zvx("ph_zvx"); 
   static SG::AuxElement::Decorator<float> dec_ph_zvx_err("ph_zvx_err"); 
   static SG::AuxElement::Decorator<char> dec_ph_isTight("ph_isTight");
   static SG::AuxElement::Decorator<unsigned int> dec_ph_isEM("ph_isEM");
 
   UInt_t tmpNtightPh=0;  
   static SG::AuxElement::Accessor<char> acc_signal("signal");
   static SG::AuxElement::ConstAccessor<char> acc_isTight("DFCommonPhotonsIsEMTight");
   static SG::AuxElement::ConstAccessor< unsigned int > acc_isEM(  "DFCommonPhotonsIsEMTightIsEMValue" );   
   for (auto ph : *content.photons) {   
		//if(acc_isTight(*ph)==1) tmpNtightPh++;  
		try {
			if(acc_isTight(*ph)==1) tmpNtightPh++;
			dec_ph_isTight(*ph) = acc_isTight(*ph);
			dec_ph_isEM(*ph) = acc_isEM(*ph);
		} catch (SG::ExcBadAuxVar) {
			dec_ph_isTight(*ph) = m_photonTightIsEMSelector->accept(ph);
			dec_ph_isEM(*ph) = m_photonTightIsEMSelector->IsemValue(); 
			/*if(acc_signal(*ph)) dec_ph_isTight(*ph) = 1;
			else dec_ph_isTight(*ph) = 0;*/
			if(dec_ph_isTight(*ph)==1) tmpNtightPh++;
		}
   }
   cand.evt.n_ph_tight = tmpNtightPh; 

   UInt_t i_ph=0;
   UInt_t n_baseline_tight_tmp = 0;
   for (auto thisPhoton : content.baselinePhotons) { 
      i_ph++;
      if(dec_ph_isTight(*thisPhoton)==1) n_baseline_tight_tmp++;
      //if(acc_ph_isTight(*thisPhoton)==1) n_baseline_tight_tmp++;
      dec_isLeading(*thisPhoton)=i_ph;
      std::pair<float, float> pointedZ = m_photonPointingTool->getCaloPointing( thisPhoton ); 
      dec_ph_zvx(*thisPhoton) = pointedZ.first;  
      dec_ph_zvx_err(*thisPhoton) = pointedZ.second;  
      dec_new_met_wmuon_dphi(*thisPhoton) = float_t(TVector2::Phi_mpi_pi( thisPhoton->phi() - (*content.met_tst)["Final"]->phi()));
      dec_new_met_nomuon_dphi(*thisPhoton) = float_t(TVector2::Phi_mpi_pi( thisPhoton->phi() - (*content.met_tst_nomuon)["Final"]->phi()));
      dec_new_met_noelectron_dphi(*thisPhoton) = float_t(TVector2::Phi_mpi_pi( thisPhoton->phi() - (*content.met_tst_noelectron)["Final"]->phi()));  
      cand.ph["ph"].add_monophoton(*thisPhoton); 
   }  
   cand.evt.n_ph_baseline_tight = n_baseline_tight_tmp++; 


   /////////////////////////////
   // selected taus
   ////////////////////////////
   if (m_doIncludeTaus) {
      cand.evt.n_tau = content.goodTaus.size();
      cand.evt.n_tau_baseline = content.baselineTaus.size();
      for (auto thisTau : content.goodTaus) {
	      cand.tau["tau"].add(*thisTau);
      }
      for (auto thisTau : content.baselineTaus){
	      cand.tau["tau_baseline"].add(*thisTau);
      }

   }

   /////////////////////////////
   // MET
   /////////////////////////////
   cand.met["met_tst"].add(*((*content.met_tst)["Final"]));

   double tmp_metSig(-9999);
   m_objTool->GetMETSig(*content.met_tst,tmp_metSig, true, true);
   cand.evt.met_tst_sig = tmp_metSig;

   

   cand.met["met_nomuon_tst"].add(*((*content.met_tst_nomuon)["Final"]));
   cand.met["met_noelectron_tst"].add(*((*content.met_tst_noelectron)["Final"]));
   cand.met["met_noelectron_tst_probe"].add(*((*content.met_tst_noelectron_probe)["Final"]));
   cand.met["met_cst"].add(*((*content.met_cst)["Final"]));
   cand.met["met_track"].add(*((*content.met_track)["Track"]));
   if (m_isMC) cand.met["met_truth"].add(*((*content.met_truth)["NonInt"]));
   cand.met["met_softerm_tst"].add(*((*content.met_tst)["PVSoftTrk"]));
   cand.met["met_softerm_cst"].add(*((*content.met_cst)["SoftClus"]));
   //cand.met["met_muonterm_tst"].add(*((*content.met_tst)["Muons"]));
   cand.met["met_jetterm"].add(*((*content.met_tst)["RefJet"]));
   cand.met["met_muonterm"].add(*((*content.met_tst)["Muons"]));
   cand.met["met_eleterm"].add(*((*content.met_tst)["RefEle"]));
   cand.met["met_phterm"].add(*((*content.met_tst)["RefGamma"]));
   if(m_doIncludeTaus) cand.met["met_tauterm"].add(*((*content.met_tst)["RefTau"]));
   
   cand.evt.met_tst_sig_old = (cand.met["met_tst"].et/1000.)/sqrt(cand.met["met_tst"].sumet/1000.);
   for(auto thisPhoton : content.goodPhotons){     
      cand.evt.met_ph_MT = std::sqrt(2*thisPhoton->pt()*cand.met["met_tst"].et*(1-std::cos(thisPhoton->phi()-cand.met["met_tst"].phi)));
      break;
   }
   // overall lepton scale factor calculation for baseline leptons

   if (m_isMC /*&& content.isNominal*/) {
      if (m_doIncludeTaus) {
         cand.evt.evsf_baseline_nominal_TAU = m_objTool->GetTotalTauSFsys(content.baselineTaus,sys, kTRUE, kFALSE, ""); // id, trig, trigName
      }

     	
      cand.evt.evsf_baseline_nominal_EL = m_objTool->GetTotalElectronSFsys(content.baselineElectrons, sys, kTRUE, kTRUE, kFALSE, kFALSE, ""); // reco, id, trig, iso, trigName
      cand.evt.evsf_baseline_nominal_MU = m_objTool->GetTotalMuonSFsys(content.baselineMuons, sys, kTRUE, kFALSE, ""); // reco+id, iso, trigName
      cand.evt.evsf_baseline_nominal_PH = m_objTool->GetTotalPhotonSFsys(content.baselinePhotons, sys, kTRUE, kFALSE);  // eff, iso

      cand.evt.evsf_good_nominal_EL = m_objTool->GetTotalElectronSFsys(content.goodElectrons, sys, kTRUE, kTRUE, kFALSE, kTRUE, ""); // reco, id, trig, iso, trigName
      cand.evt.evsf_good_nominal_MU = m_objTool->GetTotalMuonSFsys(content.goodMuons, sys, kTRUE, kTRUE, ""); // reco+id, iso, trigName

      if(content.baselinePhotons.size()>0) {
	 double tmp_SF_iso=-9999;
	 double tmp_SF_id=-9999;
         //cand.evt.evsf_baseline_nominal_PH = m_objTool->GetSignalPhotonSFsys(*content.baselinePhotons[0], sys, kTRUE, kTRUE);  // eff, iso
	 m_photonIsoSF->applySystematicVariation(sys);
	 m_photonIDSF->applySystematicVariation(sys);
	 m_photonIsoSF->getEfficiencyScaleFactor(*content.baselinePhotons[0],tmp_SF_iso);
	 m_photonIDSF->getEfficiencyScaleFactor(*content.baselinePhotons[0],tmp_SF_id);
	 cand.evt.evsf_leading_nominal_PH=tmp_SF_iso*tmp_SF_id;
      }
   }

	if (!cutSystematics && m_isMC) {antiSF(content, cand, sys);}

   if (cutSystematics && m_isMC && content.isNominal) {  //Systematics reorganization
		
		double tmp_SF(-9999);

		antiSF(content, cand, sys);  //save histograms for nominal sys

		for (auto &kv : cand.evt.el_SF_Sys){			
  			try { tmp_SF = m_objTool->GetTotalElectronSFsys(content.goodElectrons, CP::SystematicSet(kv.first.Data()), kTRUE, kTRUE, kFALSE, kTRUE, "");  } catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
			kv.second.push_back(tmp_SF);
			antiSF(content, cand, CP::SystematicSet(kv.first.Data()));  //save histograms for EL_EFF systematics
  		}	

		for (auto &kv : cand.evt.mu_SF_Sys){		
  			try { tmp_SF = m_objTool->GetTotalMuonSFsys(content.goodMuons, CP::SystematicSet(kv.first.Data()), kTRUE, kTRUE, "");  } catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
			kv.second.push_back(tmp_SF);
			antiSF(content, cand, CP::SystematicSet(kv.first.Data()));  //save histograms for MU_EFF systematics
  		}

		/*for (auto &kv : cand.evt.baseline_ph_SF_Sys){
			if(content.baselinePhotons.size()>0){
	  			try { tmp_SF = m_objTool->GetSignalPhotonSFsys(*content.baselinePhotons[0], CP::SystematicSet(kv.first.Data()), kTRUE, kTRUE);  } catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
				kv.second.push_back(tmp_SF);
			}
  		}*/

		for (auto &kv : cand.evt.ph_SF_Sys){
			double tmp_SF_iso=-9999;
			double tmp_SF_id=-9999;
			if(content.baselinePhotons.size()>0){
	  			try { 
					m_photonIDSF->applySystematicVariation(CP::SystematicSet(kv.first.Data()));
					m_photonIDSF->getEfficiencyScaleFactor(*content.baselinePhotons[0],tmp_SF_iso);
					m_photonIsoSF->applySystematicVariation(CP::SystematicSet(kv.first.Data()));
					m_photonIsoSF->getEfficiencyScaleFactor(*content.baselinePhotons[0],tmp_SF_id);
					tmp_SF=tmp_SF_iso*tmp_SF_id;
			 	} catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
				kv.second.push_back(tmp_SF);
			}
  		}

      if (m_doIncludeTaus) {
			for (auto &kv : cand.evt.tau_SF_Sys){
				/*try { tmp_SF = m_objTool->GetTotalTauSFsys(content.baselineTaus,  CP::SystematicSet(kv.first.Data()),kTRUE, kFALSE, "");  } catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
				kv.second.push_back(tmp_SF);*/
				antiSF(content, cand, CP::SystematicSet(kv.first.Data()));  //save histograms for TAU_EFF systematics
	  		}
		}

		for (auto &kv : cand.evt.jet_SF_Sys){
	  		try { tmp_SF = m_objTool->GetTotalJetSFsys(content.jets, CP::SystematicSet(kv.first.Data()),false, true);  } catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
			kv.second.push_back(tmp_SF);
  		}

		for (auto &kv : cand.evt.pu_weight_Sys){
			m_objTool->applySystematicVariation(CP::SystematicSet(kv.first.Data()));
	  		try { tmp_SF = m_objTool->GetPileupWeight();  } catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
			kv.second.push_back(tmp_SF);
			antiSF(content, cand, CP::SystematicSet(kv.first.Data()));  //save histograms for PRW_DATASF systematic
  		}

		/*for (auto &kv : cand.evt.mconly_weight_Sys){
			m_objTool->applySystematicVariation(CP::SystematicSet(kv.first.Data()));
	  		try { tmp_SF = content.eventInfo->mcEventWeight();  } catch (SG::ExcBadAuxVar) { tmp_SF = 1.0; }
			kv.second.push_back(tmp_SF);
			//antiSF(content, cand, CP::SystematicSet(kv.first.Data()));  //save histograms for PRW_DATASF systematic
  		}*/
     
	
	}
 

   //if (m_isMC /*&& content.isNominal*/) {
   /*   if (doIncludeTaus) {
         cand.evt.evsf_baseline_nominal_TAU = m_objTool->GetTotalTauSF(content.baselineTaus, kTRUE, kFALSE, ""); // id, trig, trigName
      }

     	
      cand.evt.evsf_baseline_nominal_EL = m_objTool->GetTotalElectronSF(content.baselineElectrons, kTRUE, kTRUE, kFALSE, kFALSE, ""); // reco, id, trig, iso, trigName
      cand.evt.evsf_baseline_nominal_MU = m_objTool->GetTotalMuonSF(content.baselineMuons, kTRUE, kFALSE, ""); // reco+id, iso, trigName
      cand.evt.evsf_baseline_nominal_PH = m_objTool->GetTotalPhotonSF(content.baselinePhotons, kTRUE, kFALSE);  // eff, iso

      cand.evt.evsf_good_nominal_EL = m_objTool->GetTotalElectronSF(content.goodElectrons, kTRUE, kTRUE, kFALSE, kTRUE, ""); // reco, id, trig, iso, trigName
      cand.evt.evsf_good_nominal_MU = m_objTool->GetTotalMuonSF(content.goodMuons, kTRUE, kTRUE, ""); // reco+id, iso, trigName
      if(content.baselinePhotons.size()>0) {
         cand.evt.evsf_leading_nominal_PH = m_objTool->GetSignalPhotonSF(*content.baselinePhotons[0], kTRUE, kFALSE);  // eff, iso
      }
   }*/

  //isRegionMonophoton(content, cand); //MonoPhoton
  
   /////////////////////////////
   // write tree
   /////////////////////////////


   double pass_ph_sel = cand.evt.n_ph_baseline>0 ? (cand.ph["ph"].pt.at(0)>150000 && fabs(cand.ph["ph"].eta2.at(0))<2.37 && (fabs(cand.ph["ph"].eta2.at(0))<1.37 || fabs(cand.ph["ph"].eta2.at(0))>1.52) && cand.ph["ph"].topoetcone40.at(0)/1000. < (0.022*(cand.ph["ph"].pt.at(0)/1000.)+2.45) && cand.ph["ph"].ptcone20.at(0)/cand.ph["ph"].pt.at(0)<0.05 && cand.ph["ph"].isTight.at(0) == 1 ) : false;
   double pass_el_probe = cand.evt.n_el_probe>0 ?  (cand.el["el"].pt.at(0)>150000 && fabs(cand.el["el"].eta.at(0))<2.37 && (fabs(cand.el["el"].eta.at(0))<1.37 || fabs(cand.el["el"].eta.at(0))>1.52)) : false;


   if(!m_doSkim) {cand.save();}
   else if (m_doSkim && cand.evt.trigger["HLT_g140_loose"]==1 && (cand.evt.n_jet==0 || (cand.evt.n_jet==1 && cand.evt.n_jet_good==cand.evt.n_jet))){
	if (pass_ph_sel && (cand.met["met_tst"].et>85000 || cand.met["met_nomuon_tst"].et >150000 || cand.met["met_noelectron_tst"].et > 150000)) {cand.save();}
	if (!pass_ph_sel && !m_isMC && pass_el_probe && (cand.met["met_tst_tst"].et>85000 || cand.met["met_nomuon_tst"].et >150000 || cand.met["met_noelectron_tst_probe"].et > 150000)) {cand.save();}
   }

   return EL::StatusCode::SUCCESS;
}


//FINE MONOPHOTON FILLT REE ###################################################
 

EL::StatusCode MonoJet::antiSF(Analysis::ContentHolder & content, Analysis::Output & cand,   CP::SystematicSet sys ) //CRs
{

   	

   const Double_t ptSkimToUse = (content.isNominal) ? ptSkim : ptSkimForSyst;
	const Double_t metSkimToUse = (content.isNominal) ? metSkim : metSkimForSyst;
	TVector2 met_nomuon_to_use = TVector2((*content.met_tst_nomuon)["Final"]->mpx(), (*content.met_tst_nomuon)["Final"]->mpy());
	TVector2 met_noelectron_to_use = TVector2((*content.met_tst_noelectron)["Final"]->mpx(), (*content.met_tst_noelectron)["Final"]->mpy());
	
	Double_t pu_weight=cand.evt.pu_weight;
	if(TString(sys.name()).Contains("PRW_DATASF_")){
		m_objTool->applySystematicVariation(CP::SystematicSet(sys));
		pu_weight = m_objTool->GetPileupWeight();
	}
	
   
   const MonoPhotonCuts::CutID lastSkimToUse = (content.isNominal) ? MonoPhotonCuts::BCH_cleaning : MonoPhotonCuts::BCH_cleaning;
    

   if (m_last_monophoton >= lastSkimToUse
                      && content.baselinePhotons.size()>0
                      && content.baselinePhotons[0]->pt() > ptSkimToUse
                      && (met_nomuon_to_use.Mod() > metSkimToUse || met_noelectron_to_use.Mod() >  metSkimToUse )) { //jets


		if(cand.evt.trigger["HLT_g140_loose"]==1&& (cand.ph["ph"].pt.at(0)>150000 && fabs(cand.ph["ph"].eta2.at(0))<2.37 && (fabs(cand.ph["ph"].eta2.at(0))<1.37 || fabs(cand.ph["ph"].eta2.at(0))>1.52)) &&  (cand.ph["ph"].topoetcone40.at(0)/1000. < (0.022*(cand.ph["ph"].pt.at(0)/1000)+2.45) && cand.ph["ph"].ptcone20.at(0)/cand.ph["ph"].pt.at(0)<0.05) && (cand.evt.n_jet==0 || (cand.evt.n_jet==1&& cand.evt.n_jet_good==cand.evt.n_jet)) && cand.ph["ph"].isTight.at(0) == 1) {  //Common region, used for anti-SF in CRs

			if(cand.evt.n_el_baseline==0) {
				m_mapCountSF[sys.name()]->Fill("CR_noEle",cand.evt.mconly_weight*pu_weight);
			}
			if(cand.evt.n_mu_baseline==0) {
				m_mapCountSF[sys.name()]->Fill("CR_noMu",cand.evt.mconly_weight*pu_weight);
			}
			if(cand.evt.n_mu_baseline>0 ) {
				m_mapCountSF[sys.name()]->Fill("CR_withMu",cand.evt.mconly_weight*pu_weight);
			} 
			if(cand.evt.n_el_baseline>0 ) {
				m_mapCountSF[sys.name()]->Fill("CR_withEle",cand.evt.mconly_weight*pu_weight);
			}			
	
			if (m_doIncludeTaus) {
				if(cand.evt.n_tau_baseline==0) {
					m_mapCountSF[sys.name()]->Fill("CR_noTau",cand.evt.mconly_weight*pu_weight);
				}
				if(cand.evt.n_tau_baseline>0) {
					m_mapCountSF[sys.name()]->Fill("CR_withTau",cand.evt.mconly_weight*pu_weight);
				}
			}	
			double ele_baseline_mean_SF = 0;
			double mu_baseline_mean_SF = 0;
			double tau_baseline_mean_SF = 0;

			for (auto thisEle : content.baselineElectrons){
				m_objTool->applySystematicVariation(CP::SystematicSet(sys));
				ele_baseline_mean_SF = ele_baseline_mean_SF+m_objTool->GetSignalElecSF(*thisEle, kTRUE, kTRUE, kFALSE, kFALSE, "");
			}
			ele_baseline_mean_SF = (content.baselineElectrons.size()>0) ? ele_baseline_mean_SF/double(content.baselineElectrons.size()) : 1.; 
			if(content.baselineElectrons.size()>0){m_mapEleSF[sys.name()]->Fill(ele_baseline_mean_SF, "CR", cand.evt.mconly_weight*pu_weight);} 

			for (auto thisMu : content.baselineMuons){
				m_objTool->applySystematicVariation(CP::SystematicSet(sys));
				mu_baseline_mean_SF = mu_baseline_mean_SF+m_objTool->GetSignalMuonSF(*thisMu, kTRUE, kFALSE, "");
			}
			mu_baseline_mean_SF = (content.baselineMuons.size()>0) ? mu_baseline_mean_SF/double(content.baselineMuons.size()) : 1.; 
			if(content.baselineMuons.size()>0){m_mapMuSF[sys.name()]->Fill(mu_baseline_mean_SF, "CR", cand.evt.mconly_weight*pu_weight);} 

			if (m_doIncludeTaus) {
				for (auto thisTau : content.baselineTaus){
					m_objTool->applySystematicVariation(CP::SystematicSet(sys));
					tau_baseline_mean_SF = tau_baseline_mean_SF+m_objTool->GetSignalTauSF(*thisTau, kTRUE, kFALSE, "");
				}
				tau_baseline_mean_SF = (content.baselineTaus.size()>0) ? tau_baseline_mean_SF/double(content.baselineTaus.size()) : 1.; 
				if(content.baselineTaus.size()>0){m_mapTauSF[sys.name()]->Fill(tau_baseline_mean_SF, "CR", cand.evt.mconly_weight*pu_weight);} 
			}


			// temporary check

			if(fabs(cand.ph["ph"].met_nomuon_dphi.at(0)) >0.4){

				if(cand.evt.n_mu_baseline>0){
					if(cand.evt.n_el_baseline==0) {
						m_mapCountSF[sys.name()]->Fill("1muCR_noEle",cand.evt.mconly_weight*pu_weight);
					}
					if(cand.evt.n_mu_baseline==1) {
						m_mapCountSF[sys.name()]->Fill("1muCR_noMu",cand.evt.mconly_weight*pu_weight);
					}
					if(cand.evt.n_mu_baseline>1) {
						m_mapCountSF[sys.name()]->Fill("1muCR_withMu",cand.evt.mconly_weight*pu_weight);
					} 
					if(cand.evt.n_el_baseline>0 ) {
						m_mapCountSF[sys.name()]->Fill("1muCR_withEle",cand.evt.mconly_weight*pu_weight);
					}				
					if(cand.evt.n_tau_baseline==0) {
						m_mapCountSF[sys.name()]->Fill("1muCR_noTau",cand.evt.mconly_weight*pu_weight);
					}	
					if(cand.evt.n_tau_baseline>0 ) {
						m_mapCountSF[sys.name()]->Fill("1muCR_withTau",cand.evt.mconly_weight*pu_weight);
					}		
		
					ele_baseline_mean_SF = 0;
					mu_baseline_mean_SF = 0;
					tau_baseline_mean_SF = 0;

					for (auto thisEle : content.baselineElectrons){
						m_objTool->applySystematicVariation(CP::SystematicSet(sys));
						ele_baseline_mean_SF = ele_baseline_mean_SF+m_objTool->GetSignalElecSF(*thisEle, kTRUE, kTRUE, kFALSE, kFALSE, "");
					}
					ele_baseline_mean_SF = (content.baselineElectrons.size()>0) ? ele_baseline_mean_SF/double(content.baselineElectrons.size()) : 1.; 
					if(content.baselineElectrons.size()>0){m_mapEleSF[sys.name()]->Fill(ele_baseline_mean_SF, "1muCR", cand.evt.mconly_weight*pu_weight); }

					for (auto thisMu : content.baselineMuons){
						m_objTool->applySystematicVariation(CP::SystematicSet(sys));
						mu_baseline_mean_SF = mu_baseline_mean_SF+m_objTool->GetSignalMuonSF(*thisMu, kTRUE, kFALSE, "");
					}
					mu_baseline_mean_SF = (content.baselineMuons.size()>0) ? mu_baseline_mean_SF/double(content.baselineMuons.size()) : 1.; 
					if(content.baselineMuons.size()>0){m_mapMuSF[sys.name()]->Fill(mu_baseline_mean_SF, "1muCR", cand.evt.mconly_weight*pu_weight); }
				
					if (m_doIncludeTaus) {
						for (auto thisTau : content.baselineTaus){
							m_objTool->applySystematicVariation(CP::SystematicSet(sys));
							tau_baseline_mean_SF = tau_baseline_mean_SF+m_objTool->GetSignalTauSF(*thisTau, kTRUE, kFALSE, "");
						}
						tau_baseline_mean_SF = (content.baselineTaus.size()>0) ? tau_baseline_mean_SF/double(content.baselineTaus.size()) : 1.; 
						if(content.baselineTaus.size()>0){m_mapTauSF[sys.name()]->Fill(tau_baseline_mean_SF, "1muCR", cand.evt.mconly_weight*pu_weight);} 
					}			
				}
			}
			

			if(cand.evt.met_tst_sig>8.5 && fabs(cand.ph["ph"].ZVtx.at(0)) < 250) {
				if(cand.evt.n_el_baseline==0) {
					m_mapCountSF[sys.name()]->Fill("SR_noEle",cand.evt.mconly_weight*pu_weight);
				}
				if(cand.evt.n_mu_baseline==0) {
					m_mapCountSF[sys.name()]->Fill("SR_noMu",cand.evt.mconly_weight*pu_weight);
				}
				if(cand.evt.n_mu_baseline>0 ) {
					m_mapCountSF[sys.name()]->Fill("SR_withMu",cand.evt.mconly_weight*pu_weight);
				} 
				if(cand.evt.n_el_baseline>0 ) {
					m_mapCountSF[sys.name()]->Fill("SR_withEle",cand.evt.mconly_weight*pu_weight);
				}
			if (m_doIncludeTaus) {
				if(cand.evt.n_tau_baseline==0) {
					m_mapCountSF[sys.name()]->Fill("SR_noTau",cand.evt.mconly_weight*pu_weight);
				}
				if(cand.evt.n_tau_baseline>0) {
					m_mapCountSF[sys.name()]->Fill("SR_withTau",cand.evt.mconly_weight*pu_weight);
				}
			}	
				ele_baseline_mean_SF = 0;
				mu_baseline_mean_SF = 0;
				tau_baseline_mean_SF = 0;

				for (auto thisEle : content.baselineElectrons){
					m_objTool->applySystematicVariation(CP::SystematicSet(sys));
					ele_baseline_mean_SF = ele_baseline_mean_SF+m_objTool->GetSignalElecSF(*thisEle, kTRUE, kTRUE, kFALSE, kFALSE, "");
				}
				ele_baseline_mean_SF = (content.baselineElectrons.size()>0) ? ele_baseline_mean_SF/double(content.baselineElectrons.size()) : 1.; 
				if(content.baselineElectrons.size()>0){m_mapEleSF[sys.name()]->Fill(ele_baseline_mean_SF, "SR", cand.evt.mconly_weight*pu_weight);} 

				for (auto thisMu : content.baselineMuons){
					m_objTool->applySystematicVariation(CP::SystematicSet(sys));
					mu_baseline_mean_SF = mu_baseline_mean_SF+m_objTool->GetSignalMuonSF(*thisMu, kTRUE, kFALSE, "");
				}
				mu_baseline_mean_SF = (content.baselineMuons.size()>0) ? mu_baseline_mean_SF/double(content.baselineMuons.size()) : 1.; 
				if(content.baselineMuons.size()>0){m_mapMuSF[sys.name()]->Fill(mu_baseline_mean_SF, "SR", cand.evt.mconly_weight*pu_weight);} 

				if (m_doIncludeTaus) {
					for (auto thisTau : content.baselineTaus){
						m_objTool->applySystematicVariation(CP::SystematicSet(sys));
						tau_baseline_mean_SF = tau_baseline_mean_SF+m_objTool->GetSignalTauSF(*thisTau, kTRUE, kFALSE, "");
					}
					tau_baseline_mean_SF = (content.baselineTaus.size()>0) ? tau_baseline_mean_SF/double(content.baselineTaus.size()) : 1.; 
					if(content.baselineTaus.size()>0){m_mapTauSF[sys.name()]->Fill(tau_baseline_mean_SF, "SR", cand.evt.mconly_weight*pu_weight);} 
				}				
				
			}

			
		}

		

	}

   return EL::StatusCode::SUCCESS;
} 


