#include "MonoJetAnalysis/OutputMET.h"

#include <TTree.h>

//FEDE argument added

Analysis::OutputMET::OutputMET(TString name, Bool_t doTrim/*, Bool_t doMonophoton*/) : Analysis::OutputObject::OutputObject(name, doTrim/*, doMonophoton*/)
{
   reset();
}

Analysis::OutputMET::~OutputMET()
{
}

void Analysis::OutputMET::reset()
{
   et = -9999;
   etx = -9999;
   ety = -9999;
   sumet = -9999;
   phi = -9999;

   return;
}

void Analysis::OutputMET::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (!doTrim()) {
      tree->Branch(prefix + "et", &et);
      tree->Branch(prefix + "phi", &phi);
      //tree->Branch(prefix + "etx", &etx);
      //tree->Branch(prefix + "ety", &ety);
      tree->Branch(prefix + "sumet", &sumet);

   } else if (doTrim() && (prefix == "met_tst_"||/*prefix == "met_cst_"||*/prefix == "met_nomuon_tst_"||prefix == "met_noelectron_tst_" || (isData() && prefix=="met_noelectron_tst_probe_"))) {
      tree->Branch(prefix + "et", &et);
      //tree->Branch(prefix + "phi", &phi);
   }

   return;
}

void Analysis::OutputMET::add(const xAOD::MissingET &input)
{
   et = input.met();
   etx = input.mpx();
   ety = input.mpy();
   sumet = input.sumet();
   phi = input.phi();
}
