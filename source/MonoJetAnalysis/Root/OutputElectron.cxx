#include "MonoJetAnalysis/OutputElectron.h"
#include <xAODEgamma/EgammaxAODHelpers.h>
#include "xAODTruth/xAODTruthHelpers.h"

#include <TTree.h>

Analysis::OutputElectron::OutputElectron(TString name, Bool_t doTrim/*, Bool_t doMonophoton*/) : Analysis::OutputObject::OutputObject(name, doTrim/*, doMonophoton*/)
{
   reset();
}

Analysis::OutputElectron::~OutputElectron()
{
}

void Analysis::OutputElectron::reset()
{

   SF.clear();
   SF_iso.clear();
   SF_trigger.clear();
   SF_tot.clear();
   for (auto &kv : el_SF_Sys) {
     kv.second.clear();
   }

   charge.clear();
   pt.clear();
   eta.clear();
   phi.clear();
   m.clear();
   id_pt.clear();
   id_eta.clear();
   id_phi.clear();
   id_m.clear();
   cl_pt.clear();
   cl_eta.clear();
   cl_etaBE2.clear();
   cl_phi.clear();
   cl_m.clear();
   ptcone20.clear();
   ptvarcone20.clear();
   ptvarcone20_TightTTVA_pt1000.clear();
   etcone20.clear();
   topoetcone20.clear();
   ptcone30.clear();
   ptvarcone30.clear();
   etcone30.clear();
   topoetcone30.clear();
   ptcone40.clear();
   ptvarcone40.clear();
   etcone40.clear();
   topoetcone40.clear();
   d0.clear();
   d0sig.clear();
   z0.clear();
   z0sig.clear();
   author.clear();
   isConv.clear();
   /*
   demaxs1.clear();
   fside.clear();
   weta2.clear();
   ws3.clear();
   eratio.clear();
   reta.clear();
   rphi.clear();
   time_cl.clear();
   time_maxEcell.clear();
   truth_pt.clear();
   truth_eta.clear();
   truth_phi.clear();
   truth_E.clear();
   truth_matched.clear();
   truth_mothertype.clear();
   truth_status.clear();
   truth_type.clear();
   truth_typebkg.clear();
   truth_origin.clear();
   truth_originbkg.clear();
   */
   isotool_pass_loosetrackonly.clear();
   met_nomuon_dphi.clear();
   met_wmuon_dphi.clear();
   met_noelectron_dphi.clear();
   met_noelectron_probe_dphi.clear();

   truth_pt.clear();
   truth_eta.clear();
   truth_phi.clear();
   truth_type.clear();
   truth_origin.clear();
   truth_status.clear();


   //for monophoton
   pt_1.clear();
   eta_1.clear();
   phi_1.clear();
   pt_2.clear();
   eta_2.clear();
   phi_2.clear();
   /*pt_3.clear();
   eta_3.clear();
   phi_3.clear();*/
   _1_met_nomuon_dphi.clear();
   _2_met_nomuon_dphi.clear();
   //_3_met_nomuon_dphi.clear();
   _1_met_wmuon_dphi.clear();
   _2_met_wmuon_dphi.clear();
   //_3_met_wmuon_dphi.clear();

   return;
}

void Analysis::OutputElectron::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {

		if(!doMonophoton()){
			tree->Branch(prefix + "pt", &pt);

			if (!prefix.Contains("baseline")){
				tree->Branch(prefix + "eta", &eta);
				tree->Branch(prefix + "phi", &phi);
			}

			if (!doTrim()) {

				tree->Branch(prefix + "SF", &SF);

				if (prefix.Contains("baseline")){
				  	tree->Branch(prefix + "eta", &eta);
				  	tree->Branch(prefix + "phi", &phi);
				  	for (auto &kv : el_SF_Sys) tree->Branch(prefix + "SF_syst_" + kv.first.Data(), &kv.second);
				}

				else {
					tree->Branch(prefix + "SF_iso", &SF_iso);
					tree->Branch(prefix + "SF_trigger", &SF_trigger);
					tree->Branch(prefix + "SF_tot", &SF_tot);

					tree->Branch(prefix + "isotool_pass_loosetrackonly", &isotool_pass_loosetrackonly);

					tree->Branch(prefix + "m", &m);
					tree->Branch(prefix + "charge", &charge);
					tree->Branch(prefix + "id_pt", &id_pt);
					tree->Branch(prefix + "id_eta", &id_eta);
					tree->Branch(prefix + "id_phi", &id_phi);
					tree->Branch(prefix + "id_m", &id_m);
					tree->Branch(prefix + "cl_pt", &cl_pt);
					tree->Branch(prefix + "cl_eta", &cl_eta);
					tree->Branch(prefix + "cl_etaBE2", &cl_etaBE2);
					tree->Branch(prefix + "cl_phi", &cl_phi);
					tree->Branch(prefix + "cl_m", &cl_m);
					tree->Branch(prefix + "ptcone20", &ptcone20);
					tree->Branch(prefix + "ptvarcone20", &ptvarcone20);
					tree->Branch(prefix + "ptvarcone20_TightTTVA_pt1000", &ptvarcone20_TightTTVA_pt1000);
					tree->Branch(prefix + "etcone20", &etcone20);
					tree->Branch(prefix + "topoetcone20", &topoetcone20);
					tree->Branch(prefix + "ptcone30", &ptcone30);
					tree->Branch(prefix + "ptvarcone30", &ptvarcone30);
					tree->Branch(prefix + "etcone30", &etcone30);
					tree->Branch(prefix + "topoetcone30", &topoetcone30);
					tree->Branch(prefix + "ptcone40", &ptcone40);
					tree->Branch(prefix + "ptvarcone40", &ptvarcone40);
					tree->Branch(prefix + "etcone40", &etcone40);
					tree->Branch(prefix + "topoetcone40", &topoetcone40);
					tree->Branch(prefix + "d0", &d0);
					tree->Branch(prefix + "d0sig", &d0sig);
					tree->Branch(prefix + "z0", &z0);
					tree->Branch(prefix + "z0sig", &z0sig);
					tree->Branch(prefix + "author", &author);
					tree->Branch(prefix + "isConv", &isConv);
					/*
						tree->Branch(prefix + "demaxs1", &demaxs1);
						tree->Branch(prefix + "fside", &fside);
						tree->Branch(prefix + "weta2", &weta2);
						tree->Branch(prefix + "ws3", &ws3);
						tree->Branch(prefix + "eratio", &eratio);
						tree->Branch(prefix + "reta", &reta);
						tree->Branch(prefix + "rphi", &rphi);
						tree->Branch(prefix + "time_cl", &time_cl);
						tree->Branch(prefix + "time_maxEcell", &time_maxEcell);
						tree->Branch(prefix + "truth_matched", &truth_matched);
						tree->Branch(prefix + "truth_mothertype", &truth_mothertype);
						tree->Branch(prefix + "truth_typebkg", &truth_typebkg);
						tree->Branch(prefix + "truth_originbkg", &truth_originbkg);
					*/
					tree->Branch(prefix + "truth_pt", &truth_pt);
					tree->Branch(prefix + "truth_eta", &truth_eta);
					tree->Branch(prefix + "truth_phi", &truth_phi);
					tree->Branch(prefix + "truth_status", &truth_status);
					tree->Branch(prefix + "truth_type", &truth_type);
					tree->Branch(prefix + "truth_origin", &truth_origin);

					tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
					tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
					tree->Branch(prefix + "met_noelectron_dphi", &met_noelectron_dphi);
				}
			}
		}

      if(doMonophoton()){

			//tree->Branch(prefix + "ptcone20", &ptcone20);
			//tree->Branch(prefix + "topoetcone40", &topoetcone40);
			if(!doTrim() || (doTrim() && isData())){
				tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
				tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);
				tree->Branch(prefix + "met_noelectron_probe_dphi", &met_noelectron_probe_dphi);
		  		tree->Branch(prefix + "pt", &pt);
		   		tree->Branch(prefix + "eta", &eta);   
			}    
		 

			if(!doTrim()){
				//tree->Branch(prefix + "m", &m);
				tree->Branch(prefix + "met_noelectron_dphi", &met_noelectron_dphi);

		   		tree->Branch(prefix + "phi", &phi);
				tree->Branch(prefix + "charge", &charge);

				/*tree->Branch(prefix + "d0", &d0);
				tree->Branch(prefix + "d0sig", &d0sig);
				tree->Branch(prefix + "z0", &z0);
				tree->Branch(prefix + "z0sig", &z0sig);*/
				tree->Branch(prefix + "author", &author);
				tree->Branch(prefix + "isConv", &isConv);

				tree->Branch(prefix + "truth_pt", &truth_pt);
				tree->Branch(prefix + "truth_eta", &truth_eta);
				tree->Branch(prefix + "truth_phi", &truth_phi);
				tree->Branch(prefix + "truth_status", &truth_status);
				tree->Branch(prefix + "truth_type", &truth_type);
				tree->Branch(prefix + "truth_origin", &truth_origin);				
			}
      }
   }

   return;
}

void Analysis::OutputElectron::add(const xAOD::Electron &input)
{

	author.push_back(input.author());
   isConv.push_back(xAOD::EgammaHelpers::isConvertedPhoton(&input));
   charge.push_back(input.charge());
   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());

   const xAOD::TrackParticle *thisTrack = input.trackParticle();
   id_pt.push_back(thisTrack->pt());
   id_eta.push_back(thisTrack->eta());
   id_phi.push_back(thisTrack->phi());
   id_m.push_back(thisTrack->m());
   cl_pt.push_back(input.caloCluster()->pt());
   cl_eta.push_back(input.caloCluster()->eta());
   cl_etaBE2.push_back(input.caloCluster()->etaBE(2)); // to be used for detector-related cuts, e.g. track
   cl_phi.push_back(input.caloCluster()->phi());
   cl_m.push_back(input.caloCluster()->m());

   static SG::AuxElement::ConstAccessor<float> acc_new_d0("new_d0");
   static SG::AuxElement::ConstAccessor<float> acc_new_d0sig("new_d0sig");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0("new_z0");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0sig("new_z0sig");
   try {
      d0.push_back(acc_new_d0(input));
      d0sig.push_back(acc_new_d0sig(input));
      z0.push_back(acc_new_z0(input));
      z0sig.push_back(acc_new_z0sig(input));
   } catch (SG::ExcBadAuxVar) {
      d0.push_back(-9999);
      d0sig.push_back(-9999);
      z0.push_back(-9999);
      z0sig.push_back(-9999);
   }
   /*
   demaxs1.push_back(-9999); // TODO: implement or remove
   fside.push_back(-9999); // TODO: implement or remove
   weta2.push_back(-9999); // TODO: implement or remove
   ws3.push_back(-9999); // TODO: implement or remove
   eratio.push_back(-9999);  // TODO: implement or remove
   reta.push_back(-9999);  // TODO: implement or remove
   rphi.push_back(-9999);  // TODO: implement or remove
   time_cl.push_back(-9999); // TODO: implement or remove
   time_maxEcell.push_back(-9999);  // TODO: implement or remove
   */

   // isolation variables
   Float_t tmp_ptcone20(-9999);
   Float_t tmp_ptvarcone20(-9999);
   Float_t tmp_ptvarcone20_TightTTVA_pt1000(-9999);
   Float_t tmp_etcone20(-9999);
   Float_t tmp_topoetcone20(-9999);
   Float_t tmp_ptcone30(-9999);
   Float_t tmp_ptvarcone30(-9999);
   Float_t tmp_etcone30(-9999);
   Float_t tmp_topoetcone30(-9999);
   Float_t tmp_ptcone40(-9999);
   Float_t tmp_ptvarcone40(-9999);
   Float_t tmp_etcone40(-9999);
   Float_t tmp_topoetcone40(-9999);
   input.isolationValue(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
   input.isolationValue(tmp_ptvarcone20, xAOD::Iso::IsolationType::ptvarcone20);
   input.isolationValue(tmp_ptvarcone20_TightTTVA_pt1000, xAOD::Iso::IsolationType::ptvarcone20_TightTTVA_pt1000);
   input.isolationValue(tmp_etcone20, xAOD::Iso::IsolationType::etcone20);
   input.isolationValue(tmp_topoetcone20, xAOD::Iso::IsolationType::topoetcone20);
   input.isolationValue(tmp_ptcone30, xAOD::Iso::IsolationType::ptcone30);
   input.isolationValue(tmp_ptvarcone30, xAOD::Iso::IsolationType::ptvarcone30);
   input.isolationValue(tmp_etcone30, xAOD::Iso::IsolationType::etcone30);
   input.isolationValue(tmp_topoetcone30, xAOD::Iso::IsolationType::topoetcone30);
   input.isolationValue(tmp_ptcone40, xAOD::Iso::IsolationType::ptcone40);
   input.isolationValue(tmp_ptvarcone40, xAOD::Iso::IsolationType::ptvarcone40);
   input.isolationValue(tmp_etcone40, xAOD::Iso::IsolationType::etcone40);
   input.isolationValue(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
   ptcone20.push_back(tmp_ptcone20);
   ptvarcone20.push_back(tmp_ptvarcone20);
   ptvarcone20_TightTTVA_pt1000.push_back(tmp_ptvarcone20_TightTTVA_pt1000);
   etcone20.push_back(tmp_etcone20);
   topoetcone20.push_back(tmp_topoetcone20);
   ptcone30.push_back(tmp_ptcone30);
   ptvarcone30.push_back(tmp_ptvarcone30);
   etcone30.push_back(tmp_etcone30);
   topoetcone30.push_back(tmp_topoetcone30);
   ptcone40.push_back(tmp_ptcone40);
   ptvarcone40.push_back(tmp_ptvarcone40);
   etcone40.push_back(tmp_etcone40);
   topoetcone40.push_back(tmp_topoetcone40);

   // decorations we define in our analysis code
   static SG::AuxElement::ConstAccessor<char> acc_pass_loosetrackonly("pass_loosetrackonly");
   try {
      isotool_pass_loosetrackonly.push_back(acc_pass_loosetrackonly(input));
   } catch (SG::ExcBadAuxVar) {
      isotool_pass_loosetrackonly.push_back(-9999);
   }

   // dphi's
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_noelectron_dphi("new_met_noelectron_dphi");
   try {
     met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
     met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
     met_noelectron_dphi.push_back(acc_new_met_noelectron_dphi(input));
   } catch (SG::ExcBadAuxVar) {
     met_nomuon_dphi.push_back(-9999);
     met_wmuon_dphi.push_back(-9999);
     met_noelectron_dphi.push_back(-9999);
   }


   const xAOD::TruthParticle *truth_particle = xAOD::TruthHelpers::getTruthParticle(input);
   if (truth_particle) {
      truth_pt.push_back(truth_particle->pt());
      truth_eta.push_back(truth_particle->eta());
      truth_phi.push_back(truth_particle->phi());
      truth_status.push_back(truth_particle->status());
      //     truth_type.push_back(xAOD::TruthHelpers::getParticleTruthType(input));
      //     truth_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(input));
   }

   // MCTruthClassifier
   Int_t tp_type(-9999), tp_origin(-9999);

   static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
   static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

   try {
      tp_type = acc_truthType(input);
      tp_origin = acc_truthOrigin(input);
   } catch (SG::ExcBadAuxVar) {
      tp_type = -9999;
      tp_origin = -9999;
   }

   truth_type.push_back(tp_type);
   truth_origin.push_back(tp_origin);

   return;

}



void Analysis::OutputElectron::add_SFs(const xAOD::Electron &input, std::string Sys, bool trigger, bool iso, bool tot)
{
  static SG::AuxElement::ConstAccessor<double> acc_lep_SF("effscalefact");
  double tmp_lep_SF(-9999);
  try { tmp_lep_SF = acc_lep_SF(input); } catch (SG::ExcBadAuxVar) { tmp_lep_SF = 1.0; }
  if (Sys=="Nom"){
    if (iso) SF_iso.push_back(tmp_lep_SF);
    else if (trigger) SF_trigger.push_back(tmp_lep_SF);
    else if (tot) SF_tot.push_back(tmp_lep_SF);
    else SF.push_back(tmp_lep_SF);
  }
  for (auto &kv : el_SF_Sys) {
    if (Sys==kv.first.Data()) kv.second.push_back(tmp_lep_SF);
  }
  return;
}


void Analysis::OutputElectron::add_monophoton(const xAOD::Electron &input)
{
   static SG::AuxElement::ConstAccessor<unsigned int> acc_isLeading("isLeading");
   UInt_t ele_isLeading = acc_isLeading(input);

	if((!doTrim()&&ele_isLeading<4) or (doTrim() && ele_isLeading==1)){  
		pt.push_back(input.pt());
		eta.push_back(input.eta());
		phi.push_back(input.phi());
	}



	if(ele_isLeading==1){

		static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
		static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
		static SG::AuxElement::ConstAccessor<float> acc_new_met_noelectron_dphi("new_met_noelectron_dphi");
		static SG::AuxElement::ConstAccessor<float> acc_new_met_noelectron_probe_dphi("new_met_noelectron_probe_dphi");
		try {
		  met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
		  met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
		  met_noelectron_dphi.push_back(acc_new_met_noelectron_dphi(input));
		  met_noelectron_probe_dphi.push_back(acc_new_met_noelectron_probe_dphi(input));
		} catch (SG::ExcBadAuxVar) {
		  met_nomuon_dphi.push_back(-9999);
		  met_wmuon_dphi.push_back(-9999);
		  met_noelectron_dphi.push_back(-9999);
		  met_noelectron_probe_dphi.push_back(-9999);
		}

	
   	if(!doTrim()){
			author.push_back(input.author());
			isConv.push_back(xAOD::EgammaHelpers::isConvertedPhoton(&input));
			charge.push_back(input.charge());
			//m.push_back(input.m());

			/*static SG::AuxElement::ConstAccessor<float> acc_new_d0("new_d0");
			static SG::AuxElement::ConstAccessor<float> acc_new_d0sig("new_d0sig");
			static SG::AuxElement::ConstAccessor<float> acc_new_z0("new_z0");
			static SG::AuxElement::ConstAccessor<float> acc_new_z0sig("new_z0sig");
			try {
				d0.push_back(acc_new_d0(input));
				d0sig.push_back(acc_new_d0sig(input));
				z0.push_back(acc_new_z0(input));
				z0sig.push_back(acc_new_z0sig(input));
			} catch (SG::ExcBadAuxVar) {
				d0.push_back(-9999);
				d0sig.push_back(-9999);
				z0.push_back(-9999);
				z0sig.push_back(-9999);
			}*/


			// isolation variables 
			/*Float_t tmp_ptcone20(-9999);
			Float_t tmp_topoetcone40(-9999);
			input.isolationValue(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
			input.isolationValue(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
			ptcone20.push_back(tmp_ptcone20);
			topoetcone40.push_back(tmp_topoetcone40);*/

			const xAOD::TruthParticle *truth_particle = xAOD::TruthHelpers::getTruthParticle(input);
			if (truth_particle) {
				truth_pt.push_back(truth_particle->pt());
				truth_eta.push_back(truth_particle->eta());
				truth_phi.push_back(truth_particle->phi());
				truth_status.push_back(truth_particle->status());
				//     truth_type.push_back(xAOD::TruthHelpers::getParticleTruthType(input));
				//     truth_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(input));
			}

			// MCTruthClassifier
			Int_t tp_type(-9999), tp_origin(-9999);

			static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
			static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

			try {
				tp_type = acc_truthType(input);
				tp_origin = acc_truthOrigin(input);
			} catch (SG::ExcBadAuxVar) {
				tp_type = -9999;
				tp_origin = -9999;
			}

			truth_type.push_back(tp_type);
			truth_origin.push_back(tp_origin);
		}
	}

   return;
}



