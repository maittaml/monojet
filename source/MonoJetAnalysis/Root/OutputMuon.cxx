#include "MonoJetAnalysis/OutputMuon.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"

#include <TTree.h>

Analysis::OutputMuon::OutputMuon(TString name, Bool_t doTrim/*, Bool_t doMonophoton*/) : Analysis::OutputObject::OutputObject(name, doTrim/*, doMonophoton*/)
{
   reset();
}

Analysis::OutputMuon::~OutputMuon()
{
}

void Analysis::OutputMuon::reset()
{

   charge.clear();

   SF.clear();
   SF_iso.clear();
   for (auto &kv : mu_SF_Sys) {
     kv.second.clear();
   }

	pt.clear();
   eta.clear();
   phi.clear();
   m.clear();
   id_pt.clear();
   id_eta.clear();
   id_phi.clear();
   id_m.clear();
   me_pt.clear();
   me_eta.clear();
   me_phi.clear();
   me_m.clear();
   ptcone20.clear();
   ptvarcone20.clear();
   etcone20.clear();
   topoetcone20.clear();
   ptcone30.clear();
   ptvarcone30.clear();
   ptvarcone30_TightTTVA_pt1000.clear();
   etcone30.clear();
   topoetcone30.clear();
   ptcone40.clear();
   ptvarcone40.clear();
   etcone40.clear();
   topoetcone40.clear();
   d0.clear();
   d0sig.clear();
   z0.clear();
   z0sig.clear();
   author.clear();
   quality.clear();
   isSA.clear();
   isotool_pass_loosetrackonly.clear();
   met_nomuon_dphi.clear();
   met_wmuon_dphi.clear();
   //   truth_pt.clear();
   //   truth_eta.clear();
   //   truth_phi.clear();
   //   truth_status.clear();

   //for monophoton
   pt_1.clear();
   eta_1.clear();
   phi_1.clear();
   pt_2.clear();
   eta_2.clear();
   phi_2.clear();
   /*pt_3.clear();
   eta_3.clear();
   phi_3.clear();*/

   return;
}

void Analysis::OutputMuon::attachToTree(TTree *tree)
{
   const TString prefix = name() + "_";

   if (write()) {
		if(!doMonophoton()){
			tree->Branch(prefix + "pt", &pt);

		   if (!prefix.Contains("baseline")){
				tree->Branch(prefix + "eta", &eta);
				tree->Branch(prefix + "phi", &phi);
		   }

		   if (!doTrim()) {

		      tree->Branch(prefix + "SF", &SF);

				if (prefix.Contains("baseline")){
					tree->Branch(prefix + "eta", &eta);
				  	tree->Branch(prefix + "phi", &phi);
				  	for (auto &kv : mu_SF_Sys) tree->Branch(prefix + "SF_syst_" + kv.first, &kv.second);
				}
				else {

			  		tree->Branch(prefix + "SF_iso", &SF_iso);
					tree->Branch(prefix + "isotool_pass_loosetrackonly", &isotool_pass_loosetrackonly);

					tree->Branch(prefix + "m", &m);
					tree->Branch(prefix + "d0", &d0);
					tree->Branch(prefix + "d0sig", &d0sig);
					tree->Branch(prefix + "z0", &z0);
					tree->Branch(prefix + "z0sig", &z0sig);

					tree->Branch(prefix + "charge", &charge);
					tree->Branch(prefix + "id_pt", &id_pt);
					tree->Branch(prefix + "id_eta", &id_eta);
					tree->Branch(prefix + "id_phi", &id_phi);
					tree->Branch(prefix + "id_m", &id_m);
					tree->Branch(prefix + "me_pt", &me_pt);
					tree->Branch(prefix + "me_eta", &me_eta);
					tree->Branch(prefix + "me_phi", &me_phi);
					tree->Branch(prefix + "me_m", &me_m);
					tree->Branch(prefix + "ptcone20", &ptcone20);
					tree->Branch(prefix + "ptvarcone20", &ptvarcone20);
					tree->Branch(prefix + "etcone20", &etcone20);
					tree->Branch(prefix + "topoetcone20", &topoetcone20);
					tree->Branch(prefix + "ptcone30", &ptcone30);
					tree->Branch(prefix + "ptvarcone30", &ptvarcone30);
					tree->Branch(prefix + "ptvarcone30_TightTTVA_pt1000", &ptvarcone30_TightTTVA_pt1000);
					tree->Branch(prefix + "etcone30", &etcone30);
					tree->Branch(prefix + "topoetcone30", &topoetcone30);
					tree->Branch(prefix + "ptcone40", &ptcone40);
					tree->Branch(prefix + "ptvarcone40", &ptvarcone40);
					tree->Branch(prefix + "etcone40", &etcone40);
					tree->Branch(prefix + "topoetcone40", &topoetcone40);
					tree->Branch(prefix + "author", &author);
					tree->Branch(prefix + "quality", &quality);
					tree->Branch(prefix + "isSA", &isSA);

					tree->Branch(prefix + "met_nomuon_dphi", &met_nomuon_dphi);
					tree->Branch(prefix + "met_wmuon_dphi", &met_wmuon_dphi);

					//    tree->Branch(prefix + "truth_pt", &truth_pt);
					//    tree->Branch(prefix + "truth_eta", &truth_eta);
					//    tree->Branch(prefix + "truth_phi", &truth_phi);
					//    tree->Branch(prefix + "truth_status", &truth_status);
					tree->Branch(prefix + "truth_type", &truth_type);
					tree->Branch(prefix + "truth_origin", &truth_origin);
				}

			}
		}


      if(doMonophoton()){
			if(!doTrim()){
				tree->Branch(prefix + "pt", &pt);
				tree->Branch(prefix + "eta", &eta);
				tree->Branch(prefix + "phi", &phi);
				//tree->Branch(prefix + "charge", &charge);
				/*tree->Branch(prefix + "d0", &d0);
				tree->Branch(prefix + "d0sig", &d0sig);
				tree->Branch(prefix + "z0", &z0);
				tree->Branch(prefix + "z0sig", &z0sig);*/

			}
      }
   }

   return;
}

void Analysis::OutputMuon::add(const xAOD::Muon &input)
{

	author.push_back((Int_t)input.author());
   // quality.push_back(input.quality());
   quality.push_back(-9999);
   isSA.push_back((input.muonType() == xAOD::Muon::MuonType::MuonStandAlone));
   charge.push_back(input.charge());
   pt.push_back(input.pt());
   eta.push_back(input.eta());
   phi.push_back(input.phi());
   m.push_back(input.m());
   const xAOD::TrackParticle *thisTrack = input.trackParticle(xAOD::Muon::InnerDetectorTrackParticle);
   if (thisTrack) {
      id_pt.push_back(thisTrack->pt());
      id_eta.push_back(thisTrack->eta());
      id_phi.push_back(thisTrack->phi());
      id_m.push_back(thisTrack->m());
   }
   thisTrack = input.trackParticle(xAOD::Muon::MuonSpectrometerTrackParticle);
   if (thisTrack) {
      me_pt.push_back(thisTrack->pt());
      me_eta.push_back(thisTrack->eta());
      me_phi.push_back(thisTrack->phi());
      me_m.push_back(thisTrack->m());
   }

   Float_t tmp_ptcone20(-9999);
   Float_t tmp_ptcone30(-9999);
   Float_t tmp_ptcone40(-9999);
   Float_t tmp_etcone20(-9999);
   Float_t tmp_etcone30(-9999);
   Float_t tmp_etcone40(-9999);
   Float_t tmp_topoetcone20(-9999);
   Float_t tmp_topoetcone30(-9999);
   Float_t tmp_topoetcone40(-9999);
   input.isolation(tmp_ptcone20, xAOD::Iso::IsolationType::ptcone20);
   input.isolation(tmp_ptcone30, xAOD::Iso::IsolationType::ptcone30);
   input.isolation(tmp_ptcone40, xAOD::Iso::IsolationType::ptcone40);
   input.isolation(tmp_etcone20, xAOD::Iso::IsolationType::etcone20);
   input.isolation(tmp_etcone30, xAOD::Iso::IsolationType::etcone30);
   input.isolation(tmp_etcone40, xAOD::Iso::IsolationType::etcone40);
   input.isolation(tmp_topoetcone20, xAOD::Iso::IsolationType::topoetcone20);
   input.isolation(tmp_topoetcone30, xAOD::Iso::IsolationType::topoetcone30);
   input.isolation(tmp_topoetcone40, xAOD::Iso::IsolationType::topoetcone40);
   ptcone20.push_back(tmp_ptcone20);
   ptcone30.push_back(tmp_ptcone30);
   ptcone40.push_back(tmp_ptcone40);
   etcone20.push_back(tmp_etcone20);
   etcone30.push_back(tmp_etcone30);
   etcone40.push_back(tmp_etcone40);
   topoetcone20.push_back(tmp_topoetcone20);
   topoetcone30.push_back(tmp_topoetcone30);
   topoetcone40.push_back(tmp_topoetcone40);


   static SG::AuxElement::ConstAccessor<float> acc_ptvarcone20("ptvarcone20");
   static SG::AuxElement::ConstAccessor<float> acc_ptvarcone30("ptvarcone30");
   static SG::AuxElement::ConstAccessor<float> acc_ptvarcone40("ptvarcone40");
   static SG::AuxElement::ConstAccessor<float> acc_ptvarcone30_TightTTVA_pt1000("ptvarcone30_TightTTVA_pt1000");
   if (acc_ptvarcone20.isAvailable(input)) {
      ptvarcone20.push_back(acc_ptvarcone20(input));
   }
   if (acc_ptvarcone30.isAvailable(input)) {
      ptvarcone30.push_back(acc_ptvarcone30(input));
   }
   if (acc_ptvarcone40.isAvailable(input)) {
      ptvarcone40.push_back(acc_ptvarcone40(input));
   }
   if (acc_ptvarcone30_TightTTVA_pt1000.isAvailable(input)) {
      ptvarcone30_TightTTVA_pt1000.push_back(acc_ptvarcone30_TightTTVA_pt1000(input));
   }

   thisTrack = input.primaryTrackParticle();

   static SG::AuxElement::ConstAccessor<float> acc_new_d0("new_d0");
   static SG::AuxElement::ConstAccessor<float> acc_new_d0sig("new_d0sig");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0("new_z0");
   static SG::AuxElement::ConstAccessor<float> acc_new_z0sig("new_z0sig");
   try {
      d0.push_back(acc_new_d0(input));
      d0sig.push_back(acc_new_d0sig(input));
      z0.push_back(acc_new_z0(input));
      z0sig.push_back(acc_new_z0sig(input));
   } catch (SG::ExcBadAuxVar) {
      d0.push_back(-9999);
      d0sig.push_back(-9999);
      z0.push_back(-9999);
      z0sig.push_back(-9999);
   }


   // decorations we define in our analysis code
   static SG::AuxElement::ConstAccessor<char> acc_pass_loosetrackonly("pass_loosetrackonly");
   try {
      isotool_pass_loosetrackonly.push_back(acc_pass_loosetrackonly(input));
   } catch (SG::ExcBadAuxVar) {
      isotool_pass_loosetrackonly.push_back(-9999);
   }

   // dphi's
   static SG::AuxElement::ConstAccessor<float> acc_new_met_nomuon_dphi("new_met_nomuon_dphi");
   static SG::AuxElement::ConstAccessor<float> acc_new_met_wmuon_dphi("new_met_wmuon_dphi");
   try {
      met_nomuon_dphi.push_back(acc_new_met_nomuon_dphi(input));
      met_wmuon_dphi.push_back(acc_new_met_wmuon_dphi(input));
   } catch (SG::ExcBadAuxVar) {
      met_nomuon_dphi.push_back(-9999);
      met_wmuon_dphi.push_back(-9999);
   }

    //   const xAOD::TruthParticle *truth_particle = xAOD::TruthHelpers::getTruthParticle(input);
   //   if (truth_particle) {
   //     truth_pt.push_back(truth_particle->pt());
   //     truth_eta.push_back(truth_particle->eta());
   //     truth_phi.push_back(truth_particle->phi());
   //     truth_status.push_back(truth_particle->status());
   //     truth_type.push_back(xAOD::TruthHelpers::getParticleTruthType(input));
   //     truth_origin.push_back(xAOD::TruthHelpers::getParticleTruthOrigin(input));
   //   }

   // MCTruthClassifier
   // from https://twiki.cern.ch/twiki/bin/view/Atlas/XAODMuon#How_to_retrieve_truth_type_and_o

   Int_t tp_type(-9999), tp_origin(-9999);

   static SG::AuxElement::ConstAccessor<int> acc_truthType("truthType");
   static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

   try {
      tp_type = acc_truthType(input);
      tp_origin = acc_truthOrigin(input);
   } catch (SG::ExcBadAuxVar) {
      tp_type = -9999;
      tp_origin = -9999;
   }

   truth_type.push_back(tp_type);
   truth_origin.push_back(tp_origin);


   return;
}


void Analysis::OutputMuon::add_SFs(const xAOD::Muon &input, std::string Sys, bool iso)
{
	static SG::AuxElement::ConstAccessor<double> acc_lep_SF("effscalefact");
	double tmp_lep_SF(-9999);
	try { tmp_lep_SF = acc_lep_SF(input); } catch (SG::ExcBadAuxVar) { tmp_lep_SF = 1.0; }
	if (Sys=="Nom"){
		if (iso) SF_iso.push_back(tmp_lep_SF);
		else SF.push_back(tmp_lep_SF);
	}
	for (auto &kv : mu_SF_Sys) {
		if (Sys==kv.first) kv.second.push_back(tmp_lep_SF);
	}
	return;

}

void Analysis::OutputMuon::add_monophoton(const xAOD::Muon &input)
{
   static SG::AuxElement::ConstAccessor<unsigned int> acc_isLeading("isLeading");
   UInt_t mu_isLeading = acc_isLeading(input);

	if(!doTrim() && mu_isLeading<4){   // We don't need any muon info in Systematics ...
		pt.push_back(input.pt());
		eta.push_back(input.eta());
		phi.push_back(input.phi());
	}


	/*if(mu_isLeading==1){
   	if(!doTrim()){
			static SG::AuxElement::ConstAccessor<float> acc_new_d0("new_d0");
			static SG::AuxElement::ConstAccessor<float> acc_new_d0sig("new_d0sig");
			static SG::AuxElement::ConstAccessor<float> acc_new_z0("new_z0");
			static SG::AuxElement::ConstAccessor<float> acc_new_z0sig("new_z0sig");
			try {
				d0.push_back(acc_new_d0(input));
				d0sig.push_back(acc_new_d0sig(input));
				z0.push_back(acc_new_z0(input));
				z0sig.push_back(acc_new_z0sig(input));
			} catch (SG::ExcBadAuxVar) {
				d0.push_back(-9999);
				d0sig.push_back(-9999);
				z0.push_back(-9999);
				z0sig.push_back(-9999);
			}

		}
	}*/

   return;
}




