#Francesco: data15,data16.data17,data18
#Andreas: mc16e
#Guglielmo: mc16d
#Jose Luis: mc16a
#
#set your user name
#CERN_USER=fcirotto
#python ../source/MonoJetAnalysis/python/monojetSubmit.py -w prun -u $CERN_USER -l  ../source/MonoJetAnalysis/data/samples/EXOT5/submission2019-03-14/mc16a_bkg_EXOT5.txt -n bkg.mc16a.MJ265.15Mar --doIncludeTaus --doSyst
#python ../source/MonoJetAnalysis/python/monojetSubmit.py -w prun -u $CERN_USER -l  ../source/MonoJetAnalysis/data/samples/EXOT5/submission2019-03-14/mc16d_bkg_EXOT5.txt -n bkg.mc16d.MJ265.15Mar --doIncludeTaus --doSyst
#python ../source/MonoJetAnalysis/python/monojetSubmit.py -w prun -u $CERN_USER -l  ../source/MonoJetAnalysis/data/samples/EXOT5/submission2019-03-14/mc16e_bkg_EXOT5.txt -n bkg.mc16e.MJ265.15Mar --doIncludeTaus --doSyst
#python ../source/MonoJetAnalysis/python/monojetSubmit.py -w prun -u $CERN_USER -l  ../source/MonoJetAnalysis/data/samples/EXOT5/submission2019-03-14/data15_EXOT5.txt -n data15.MJ265.15Mar --doIncludeTaus
#python ../source/MonoJetAnalysis/python/monojetSubmit.py -w prun -u $CERN_USER -l  ../source/MonoJetAnalysis/data/samples/EXOT5/submission2019-03-14/data16_EXOT5.txt -n data16.MJ265.15Mar --doIncludeTaus
#python ../source/MonoJetAnalysis/python/monojetSubmit.py -w prun -u $CERN_USER -l  ../source/MonoJetAnalysis/data/samples/EXOT5/submission2019-03-14/data17_EXOT5.txt -n data17.MJ265.15Mar --doIncludeTaus
#python ../source/MonoJetAnalysis/python/monojetSubmit.py -w prun -u $CERN_USER -l  ../source/MonoJetAnalysis/data/samples/EXOT5/submission2019-03-14/data18_EXOT5.txt -n data18.MJ265.15Mar --doIncludeTaus

python ../source/MonoJetAnalysis/python/monojetSubmit.py -w local \
       -d /afs/cern.ch/work/g/gusta/public/samples/mc16/EXOT5/Wtaunu/ \
       -m 100 \
       -n test \
       --doIncludeTaus \
       --doSyst
