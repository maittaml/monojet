#!/bin/bash

echo "START1"
ls .
echo "START2"
ls ..

echo ${CI_PROJECT_DIR}

echo "CHECKING BUILD DIR ABOVE"
pwd
ls ../*

##############################
# Setup                      #
##############################
echo "SETUPRelease"
source ~/release_setup.sh
echo "SETUPExecutables"
source ../build/${AnalysisBase_PLATFORM}/setup.sh

# DAOD file types to loop over
TEST_TYPES=(Signal Data)

# Remote DAOD filenames
TESTFILE_ORIGINS=("root://eoshome.cern.ch//eos/user/m/monojet/ci/TestFiles/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_EXOT5.e6337_s3126_r10201_p3480/DAOD_EXOT5.13463460._000575.pool.root.1" "root://eoshome.cern.ch//eos/user/m/monojet/ci/TestFiles/data18_13TeV.00351359.physics_Main.deriv.DAOD_EXOT5.f937_m1972_p3713/DAOD_EXOT5.16264816._000003.pool.root.1")

# Local names for the DAODs
TESTFILE_LOCALS=(test_signal.root test_data.root)

# Directory names to loop over
TEST_DIRS=(signal_test data_test)                    # Directory for results
TESTFILE_DIRS=(TestFiles_signal TestFiles_data)      # Directory to contain test files
TESTFILE_SUBDIRS=(mc16_13TeV.ttbar mc16_13TeV.data)   # Sub-directory to contain test files (need two directory levels due to the way that monojetSubmit.py looks for the files)
OUTPUT_DIRS=(testOutput_ttbar testOutput_data)      # Directory to contain the output minitrees

i_type=0

while [  $i_type -lt 2 ]; do

  echo "RUNNING TEST OF NTUPLING : " "${TEST_TYPES[$i_type]}"

  ##############################
  # Process test sample        #
  ##############################

  # create directory for results
  pwd
  mkdir -p "${TEST_DIRS[$i_type]}"
  cd "${TEST_DIRS[$i_type]}"
  pwd
  # copy file with xrdcp
  if [ ! -f "${TESTFILE_LOCALS[$i_type]}" ]; then
      echo "File not found! Copying it from EOS"
      # get kerberos token with service account monojet to access central test samples on EOS
      if [ -z ${SERVICE_PASS} ]
      then
        CERN_USER=monojet
        echo "Please enter the password for the service account: user ${CERN_USER} (can be obtained in the Gitlab/Settings/CICD/Variables menu)"
        echo "If you belong to the analysis team and already have your CERN USER kerberos token by entering kinit ${USER}@CERN.CH you can skip by entering ctrl + c"
        kinit ${CERN_USER}@CERN.CH
      else
        echo "Setting up kerberos"
        echo "CERN_USER - "${CERN_USER}
        echo "SERVICE_PASS - "${SERVICE_PASS}
        echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
      fi
      echo xrdcp "${TESTFILE_ORIGINS[$i_type]}" "${TESTFILE_LOCALS[$i_type]}"
      xrdcp "${TESTFILE_ORIGINS[$i_type]}" "${TESTFILE_LOCALS[$i_type]}"
  fi

  # put the test input file in a proper place
  echo "Moving test file to appropriate place"
  mkdir -p "${TESTFILE_DIRS[$i_type]}/${TESTFILE_SUBDIRS[$i_type]}"
  mv "${TESTFILE_LOCALS[$i_type]}" "${TESTFILE_DIRS[$i_type]}/${TESTFILE_SUBDIRS[$i_type]}/."

  # run a test job
  echo "Running a test job"
  echo "Current Dir : "
  pwd
  echo "Place where executable lives : "
  ls ../source/*
  echo "Running job now : "
  python ../source/MonoJetAnalysis/python/monojetSubmit.py \
         -w local \
         -m 100 \
         -n "${OUTPUT_DIRS[$i_type]}" \
         -d "${TESTFILE_DIRS[$i_type]}"

  # Increment counter
  i_type=$(( $i_type + 1))

  # cd back to the /jdm/monojet directory
  cd ..

done

echo "DONE"
